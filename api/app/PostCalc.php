<?php
class PostCalc
{

    private $_V;
    private $_nc;
    private $_na;
    private $_Rt;
    private $_Kpre;
    private $_RC;
    private $_RC1;
    private $_RC2;
    private $_optK3;
    private $_SF;

    private $preRfrctn;
    private $preSph;
    private $preVertex;
    private $preK1;
    private $preK2;

    private $postSph;
    private $postVertex;
    private $postEyeSysEffRP;
    private $postAtlasZone;
    private $postAtlas_ring_fst;
    private $postAtlas_ring_scd;
    private $postAtlas_ring_thd;
    private $postAtlas_ring_fhd;
    private $postTomey_ACCP;
    private $postGalilei_TCP;
    private $postPentacam_zone;
    private $postNet_Power;
    private $postPosterior_Power;
    private $postCentral_Power;

    private $optK1;
    private $optK2;
    private $optAL;
    private $optACD;
    private $optA_const;//AConstant
    private $optSF;//SF
    private $optHaigis_a_f;//a0
    private $optHaigis_a_s;//a1
    private $optHaigis_a_t;//a2

    private $calcData;//a2


    function __construct($data){
        $this->setV(12);
        $this->setNc(4/3);
        $this->setNa(1.336);
        $this->setRt(0.2);

        $this->setCalcData($data);
        $this->setPreSph($data->preSph);
        $this->setPreVertex($data->preVertex);
        $this->setPreK1($data->preK1);
        $this->setPreK2($data->preK2);
        $this->setKpre(($this->getPreK1() +$this->getPreK2())/2);

        $this->setPostSph($data->postSph);
        $this->setPostVertex($data->postVertex);
        $this->setPostEyeSysEffRP($data->postEyeSysEffRP);
        $this->setPostAtlasZone($data->postAtlasZone);
        $this->setPostAtlasRingFst($data->postAtlas_ring_fst);
        $this->setPostAtlasRingScd($data->postAtlas_ring_scd);
        $this->setPostAtlasRingThd($data->postAtlas_ring_thd);
        $this->setPostAtlasRingFhd($data->postAtlas_ring_fhd);
        $this->setPostTomeyACCP($data->postTomey_ACCP);
        $this->setPostGalileiTCP($data->postGalilei_TCP);
        $this->setPostPentacamZone($data->postPentacam_zone);
        $this->setPostNetPower($data->postNet_Power);
        $this->setPostPosteriorPower($data->postPosterior_Power);
        $this->setPostCentralPower($data->postCentral_Power);

        $this->setOptK1($data->optK1);
        $this->setOptK2($data->optK2);
        $this->setOptK3(($this->getOptK1()+$this->getOptK2())/2);
        $this->setOptAL($data->optAL);
        $this->setOptACD($data->optACD);
        $this->setOptACD($data->optACD);

        $this->setPreRfrctn($data->preRfrctn);
        $this->setOptAConst($data->optA_const);
        $this->setOptSF($data->optSF);
        $this->setOptHaigisAS($data->optHaigis_a_s);
        $this->setOptHaigisAT($data->optHaigis_a_t);
        $this->setOptHaigisAF($data->optHaigis_a_f);


    }

    public function getData($algoritm){
        try{
            switch($algoritm){
                case "Myopic":{
                    return array("data"=>$this->getMyopicData());
                }
                case "Hyperopic":{
                    return array("data"=>$this->getHyperopicData());
                }
                case "RK":{
                    return array("data"=>$this->getRKData());
                }
                default: return array("error"=>"Algoritm is not selected");
            }
        }catch(Exception $e){
            return (array("error"=>"Some Bad calculate ".$e));
        }

    }

    private function getMyopicData(){ //using double-K Holladay

        $res = array();
        $this->setRC1(0 - $this->getPreSph()/(1-($this->getPreVertex()/1000.*($this->getPreSph()))));
        $this->setRC2(0-( $this->getPostSph())/(1-( $this->getPostVertex()/1000.*( $this->getPostSph()))));
        $this->setRC( $this->getRC1() - $this->getRC2());

        $AL = $this->getOptAL();
        $RT = $this->getRt();
        $SF = $this->getOptSF();
        $na = $this->getNa();
        $nc = $this->getNc();
        $V= $this->getV();
        $Ref= $this->getPreRfrctn();
        $AConstant= $this->getOptAConst();
        $A9000= $this->getPostAtlasZone();
        $Atl0= $this->getPostAtlasRingFst();
        $Atl1= $this->getPostAtlasRingScd();
        $Atl2= $this->getPostAtlasRingThd();
        $Atl3= $this->getPostAtlasRingFhd();
        $RC= $this->getRC();
        $Kpre= $this->getKpre();
        $OptK3 = $this->getOptK3();
        $ACCP = $this->getPostTomeyACCP();
        $ACD = $this->getOptACD();
        $a0 = $this->getOptHaigisAF();
        $a1 = $this->getOptHaigisAS();
        $a2 = $this->getOptHaigisAT();
        $GalileiTCP = $this->getPostGalileiTCP();
        $PentacamK = $this->getPostPentacamZone();
        $NCP = $this->getPostNetPower();
        $CCT = $this->getPostCentralPower();
        $Pp = $this->getPostPosteriorPower();

        /*====================== USING ?MR ==========================*/
        $AdjEffRP = $this->getPostEyeSysEffRP()-($this->getRC()*0.15)-0.05;
        $Kpost = $AdjEffRP;
        $Rpre = 337.5/$Kpre;
        $Rpost = 337.5/$Kpost;
        $Rag = (7.*($Rpre<7) + $Rpre*($Rpre>=7));
        $AG = (13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5));
        $Alm = $AL + $RT;
        $HolACD = (0.56+$Rag-sqrt(abs($Rag*$Rag-($AG*$AG)/4)));
        $OACD = $HolACD+$SF;
        $AdjEffRPDKHOL = (1000.*$na*($na*$Rpost-($nc-1)*$Alm-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$Alm)+($Alm*$Rpost))))/(($Alm-$OACD)*(($na*$Rpost)-($nc-1)*$OACD-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$OACD)+$OACD*$Rpost)));//Adjusted EffRP output
        $res['AdjEffRPDKHOL'] = $AdjEffRPDKHOL;

        //Adjusted Atlas 9000 (4mm zone)	(Using Shammas)
        $pACD = (0.5835*$AConstant)-64.40;
        $R = ($Ref)/(1-($V/1000.*($Ref)));
        $Kc = $A9000-($RC*0.2);
        $AdjAtl9000 = ((1336./($AL-0.1*($AL-23)-($pACD+0.05)))-(1./((1.0125/($Kc+$R))-(($pACD+0.05)/1336)))); //Adjusted Atlas 9000 (4mm zone) output
        $res['AdjAtl9000'] = $AdjAtl9000;

        //Adjusted Atlas Ring Values (using double-K Holladay)
        $ARV = ($Atl0+$Atl1+$Atl2+$Atl3)/4;
        $AARV = $ARV-($RC*0.2);
        $Kpost = $AARV;
        $Rpre = 337.5/$Kpre;
        $Rpost = 337.5/$Kpost;
        $Rag = (7*($Rpre<7) + $Rpre*($Rpre>=7));
        $AG = (13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5));
        $Alm = $AL + $RT;
        $HolACD = (0.56+$Rag-sqrt(abs($Rag*$Rag-($AG*$AG)/4)));
        $OACD = $HolACD+$SF;
        $AARVDKHOL = (1000.*$na*($na*$Rpost-($nc-1)*$Alm-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$Alm)+($Alm*$Rpost))))/(($Alm-$OACD)*(($na*$Rpost)-($nc-1)*$OACD-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$OACD)+$OACD*$Rpost))); //Adjusted Atlas Ring Values output
        $res['AARVDKHOL'] = $AARVDKHOL;

        //Masket (w/Holladay)
        $LSE = -$RC;
        $R = 337.5/$OptK3;
        $Rag = (7*($R<7) + $R*($R>=7));
        $AG = (13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5));
        $HolACD = (0.56+$Rag-sqrt(($Rag*$Rag-($AG*$AG)/4)));
        $HOLLADAY = ((1000*$na*($na*$R-($nc-1)*($AL+$RT)-0.001*$Ref*($V*($na*$R-($nc-1)*($AL+$RT))+($AL+$RT)*$R)))/((($AL+$RT)-$HolACD-$SF)*($na*$R-($nc-1)*($HolACD+$SF)-0.001*$Ref*($V*($na*$R-($nc-1)*($HolACD+$SF))+($HolACD+$SF)*$R))));
        $MASKET = $HOLLADAY+($LSE*-0.326)+0.101;//Masket output
        $res['MASKET'] = $MASKET;


        //Modified-Masket (w/Holladay)
        $LSE = -$RC;
        $RT = 0.2;
        $R = 337.5/$OptK3;
        $Rag = (7.*($R<7) + $R*($R>=7));
        $AG = (13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5));
        $HolACD = (0.56+$Rag-sqrt(($Rag*$Rag-($AG*$AG)/4)));
        $HOLLADAY = ((1000*$na*($na*$R-($nc-1)*($AL+$RT)-0.001*$Ref*($V*($na*$R-($nc-1)*($AL+$RT))+($AL+$RT)*$R)))/((($AL+$RT)-$HolACD-$SF)*($na*$R-($nc-1)*($HolACD+$SF)-0.001*$Ref*($V*($na*$R-($nc-1)*($HolACD+$SF))+($HolACD+$SF)*$R))));
        $MODMASKET = $HOLLADAY +($LSE*-0.4385)+0.0295; //Modified-Masket output
        $res['MODMASKET'] = $MODMASKET;


        //Adjusted ACCP/ACP/APP (using double-K Holladay)
        $AACCP = $ACCP-($RC*0.16);
        $Kpost = $AACCP;
        $Rpre = 337.5/$Kpre;
        $Rpost = 337.5/$Kpost;
        $Rag = (7*($Rpre<7) + $Rpre*($Rpre>=7));
        $AG = (13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5));
        $Alm = $AL + $RT;
        $HolACD = (0.56+$Rag-sqrt(abs($Rag*$Rag-($AG*$AG)/4)));
        $OACD = $HolACD+$SF;
        $AACCPDKHOL = (1000*$na*($na*$Rpost-($nc-1)*$Alm-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$Alm)+($Alm*$Rpost))))/(($Alm-$OACD)*(($na*$Rpost)-($nc-1)*$OACD-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$OACD)+$OACD*$Rpost)));//Adjusted ACCP/ACP/APP output
        $res['AACCPDKHOL'] = $AACCPDKHOL;


        /*====================== NO PRIOR DATA =======================*/

        //Wang-Koch-Maloney
        $pACD = (0.5835*$AConstant)-64.40;
        $R = ($Ref)/(1-($V/1000.*($Ref)));
        $Kc = $A9000*1.114-5.59;
        $WKM = ((1336./($AL-0.1*($AL-23)-($pACD+0.05)))-(1./((1.0125/($Kc+$R))-(($pACD+0.05)/1336)))); //Wang-Koch-Maloney output
        $res['WKM'] = $WKM;


        //Shammas
        $pACD = (0.5835*$AConstant)-64.40;
        $R = ($Ref)/(1-($V/1000.*($Ref)));
        $Kc = 1.14*$OptK3-6.8;
        $SHAMMAS = ((1336./($AL-0.1*($AL-23)-($pACD+0.05)))-(1./((1.0125/($Kc+$R))-(($pACD+0.05)/1336)))); //Shammas output
        $res['SHAMMAS'] = $SHAMMAS;


        //Haigis-L
        $MWVKpr = 3.37;
        $MWALpr = 23.39;
        $VKpr = $ACD;
        $ALpr = $AL;
        $ACDKonstant = ($AConstant*0.62467)-68.747;
        $d = ($a0 + $a1*$VKpr/1000 + $a2*$ALpr/1000);
        $nCC = 1.3315;
        $n = $na;
        $dBC = 12;
        $RC = (331.5/(-5.1625*(337.5/$OptK3)+82.2603-0.35));
        $DC = ($nCC-1)/($RC/1000);
        $Zee = ($DC+($Ref/(1-$Ref*$dBC/1000)));
        $HAIGISL = ($n/($AL/1000-$d))-($n/($n/$Zee-$d)); //Haigis-L output
        $res['HAIGISL'] = $HAIGISL;

//        return "val =  ".$HAIGISL." a0 ={$a0} a1 ={$a1} a2 ={$a2} n = {$n}"." AL = {$AL}"." d = {$d}";
        //Galilei (Double-K Holladay)
        $InputRefIndex = 1.3277;
        $InputK3 = $GalileiTCP;
        $AdjustedInputK3 = ($InputK3*(1.3375-1))/($InputRefIndex-1);
        $TCP = $AdjustedInputK3; //If Version 5.2.1 or later radio button is selected.
        $TCP = $GalileiTCP; //%If Version 5.2 or earlier radio button is selected.
        $Kpost = 1.057*$TCP-1.8348;
        $Rpre = 337.5/$Kpre;
        $Rpost = 337.5/$Kpost;
        $Rag = (7.*($Rpre<7) + $Rpre*($Rpre>=7));
        $AG = (13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5));
        $Alm = $AL + $RT;
        $HolACD = (0.56+$Rag-sqrt(abs($Rag*$Rag-($AG*$AG)/4)));
        $OACD = $HolACD+$SF;
        $GALILEI = (1000*$na*($na*$Rpost-($nc-1)*$Alm-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$Alm)+($Alm*$Rpost))))/(($Alm-$OACD)*(($na*$Rpost)-($nc-1)*$OACD-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$OACD)+$OACD*$Rpost))); //Galilei output
        $res['GALILEI'] = $GALILEI;


        //Potvin/Shammas/Hill formula (aka Potvin-Hill pentacam method)
        $pACD = (0.5835*$AConstant)-64.40;
        $R = ($Ref)/(1-($V/1000.*($Ref)));
        $Kc = 12.08+0.9*$PentacamK-0.282*$AL;// %Use this if no ACD provided.
        $Kc = 11.19+0.951*$PentacamK-0.247*$AL-0.588*$ACD; //%Use this if ACD provided.
        $PSH = ((1336./($AL-0.1*($AL-23)-($pACD+0.05)))-(1./((1.0125/($Kc+$R))-(($pACD+0.05)/1336)))); //%Potvin-Hill Pentacam output
        $res['PSH'] = $PSH;


        //OCT-based IOL Formula (Myopic)
        $n1 = 1.376; //%Corneal refractive index
        $n2 = $na;// %Aqueous/vitreous index
        $pACD = (($AConstant*0.5663)-65.6+3.595)/0.9704;
        $adjustedAL = (($AL>24.4)*(sqrt($AL+0.8*($AL-24.4))) + ($AL<=24.4)*(sqrt($AL)));
        $ECP = $NCP+0.0302*$AL-1.739;
        $TargetMRSECornealPlane = 1./(1./($Ref+0.0000000001)-$V/1000);
        $IOLdepth = (0.711*($ACD-$CCT/1000)+0.623*$adjustedAL-0.25*$Pp)+$pACD-8.11;
        $V1 = $n2/($AL-$IOLdepth-$CCT/1000)*1000;
        $V2a = $TargetMRSECornealPlane;
        $V2 = $ECP+$V2a;
        $OCTIOL = $V1-$n2/($n2/$V2-($IOLdepth+$CCT/1000)/1000); //%OCT result
        $res['OCTIOL'] = $OCTIOL;

        return $res;
    }

    private function getHyperopicData(){
        $res = array();
        $PreSph = $this->getPreSph();
        $PostSph = $this->getPostSph();
        $VertexPost =$this->getPreVertex();
        $this->setRC1($PreSph/(1-($VertexPost/1000.*($PreSph))));
        $this->setRC2(( $PostSph)/(1-( $VertexPost/1000.*( $PostSph))));
        $this->setRC( $this->getRC1() - $this->getRC2());

        $AL = $this->getOptAL();
        $RT = $this->getRt();
        $SF = $this->getOptSF();
        $na = $this->getNa();
        $nc = $this->getNc();
        $V= $this->getV();
        $Ref= $this->getPreRfrctn();
        $AConstant= $this->getOptAConst();
        $A9000= $this->getPostAtlasZone();
        $Atl0= $this->getPostAtlasRingFst();
        $Atl1= $this->getPostAtlasRingScd();
        $Atl2= $this->getPostAtlasRingThd();
        $Atl3= $this->getPostAtlasRingFhd();
        $RC= $this->getRC();
        $Kpre= $this->getKpre();
        $OptK3 = $this->getOptK3();
        $ACCP = $this->getPostTomeyACCP();
        $ACD = $this->getOptACD();
        $a0 = $this->getOptHaigisAF();
        $a1 = $this->getOptHaigisAS();
        $a2 = $this->getOptHaigisAT();
        $GalileiTCP = $this->getPostGalileiTCP();
        $PentacamK = $this->getPostPentacamZone();
        $NCP = $this->getPostNetPower();
        $CCT = $this->getPostCentralPower();
        $Pp = $this->getPostPosteriorPower();
        $EffRP = $this->getPostEyeSysEffRP();

        /*=================== USING PRIOR DATA =======================*/
        //Adjusted EffRP (using double-K Holladay)
        $AdjEffRP = $EffRP+($RC*0.162)-0.279;
        $Kpost = $AdjEffRP;
        $Rpre = 337.5/$Kpre;
        $Rpost = 337.5/$Kpost;
        $Rag = 7*($Rpre<7) + $Rpre*($Rpre>=7);
        $AG = 13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5);
        $Alm = $AL + $RT;
        $HolACD = (0.56+$Rag-sqrt(abs($Rag*$Rag-($AG*$AG)/4)));
        $OACD = $HolACD+$SF;
        $AdjEffRPDKHOL = (1000*$na*($na*$Rpost-($nc-1)*$Alm-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$Alm)+($Alm*$Rpost))))/(($Alm-$OACD)*(($na*$Rpost)-($nc-1)*$OACD-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$OACD)+$OACD*$Rpost))); //%Adjusted EffRP output
        $res['AdjEffRPDKHOL']= $AdjEffRPDKHOL;


        //%Adjusted Atlas 0-3 (using double-K Holladay)
        $ARV = ($Atl0+$Atl1+$Atl2+$Atl3)/4;
        $AARV = $ARV+($RC*0.19)-0.396;// %Steinert method
        $Kpost = $AARV;
        $Rpre = 337.5/$Kpre;
        $Rpost = 337.5/$Kpost;
        $Rag = 7*($Rpre<7) + $Rpre*($Rpre>=7);
        $AG = 13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5);
        $Alm = $AL + $RT;
        $HolACD = (0.56+$Rag-sqrt(abs($Rag*$Rag-($AG*$AG)/4)));
        $OACD = $HolACD+$SF;
        $AARVDKHOL = (1000*$na*($na*$Rpost-($nc-1)*$Alm-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$Alm)+($Alm*$Rpost))))/(($Alm-$OACD)*(($na*$Rpost)-($nc-1)*$OACD-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$OACD)+$OACD*$Rpost))); //%Adjusted Atlas 0-3 output
        $res['AARVDKHOL'] = $AARVDKHOL;

//                %Masket (w/Holladay)
        $LSE = $RC;
        $R = 337.5/$OptK3;
        $Rag = 7.*($R<7) + $R*($R>=7);
        $AG =  13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5);
        $HolACD = (0.56+$Rag-sqrt(($Rag*$Rag-($AG*$AG)/4)));
        $HOLLADAY = ((1000*$na*($na*$R-($nc-1)*($AL+$RT)-0.001*$Ref*($V*($na*$R-($nc-1)*($AL+$RT))+($AL+$RT)*$R)))/((($AL+$RT)-$HolACD-$SF)*($na*$R-($nc-1)*($HolACD+$SF)-0.001*$Ref*($V*($na*$R-($nc-1)*($HolACD+$SF))+($HolACD+$SF)*$R))));
        $MASKET = $HOLLADAY+($LSE*-0.326)+0.101; //%Masket output
        $res['MASKET'] = $MASKET;

//                %Modified-Masket (w/Holladay)
        $LSE = $RC;
        $R = 337.5/$OptK3;
        $Rag = 7.*($R<7) + $R*($R>=7);
        $AG = 13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5);
        $HolACD = (0.56+$Rag-sqrt(($Rag*$Rag-($AG*$AG)/4)));
        $HOLLADAY = ((1000*$na*($na*$R-($nc-1)*($AL+$RT)-0.001*$Ref*($V*($na*$R-($nc-1)*($AL+$RT))+($AL+$RT)*$R)))/((($AL+$RT)-$HolACD-$SF)*($na*$R-($nc-1)*($HolACD+$SF)-0.001*$Ref*($V*($na*$R-($nc-1)*($HolACD+$SF))+($HolACD+$SF)*$R))));
        $MODMASKET = $HOLLADAY+($LSE*-0.4385)+0.0295; //%Modified Masket output
        $res['MODMASKET'] = $MODMASKET;



        /*====================== NO PRIOR DATA =======================*/


        //Shammas
        $pACD = (0.5835*$AConstant)-64.40;
        $R = ($Ref)/(1-($V/1000*($Ref)));
        $Kc = 1.0457*$OptK3-1.9538;
        $SHAMMAS = ((1336./($AL-0.1*($AL-23)-($pACD+0.05)))-(1/((1.0125/($Kc+$R))-(($pACD+0.05)/1336))));// %Shammas output
        $res['SHAMMAS'] = $SHAMMAS;


//                %Haigis-L
        $MWVKpr = 3.37;
        $MWALpr = 23.39;
        $ACDKonstant = ($AConstant*0.62467)-68.747;
        $VKpr = $ACD;
        $ALpr = $AL;
        $d = ($a0 + $a1*$VKpr/1000 + $a2*$ALpr/1000);
        $nCC = 1.3315;
        $n = $na;
        $dBC = 12;
        $RC = (331.5/(-5.1625*(337.5/$OptK3)+82.2603-0.35));
        $DC = ($nCC-1)/($RC/1000);
        $Zee = ($DC+($Ref/(1-$Ref*$dBC/1000)));
        $HAIGISL = ($n/($AL/1000-$d))-($n/($n/$Zee-$d)); //%Haigis-L output
        $res['HAIGISL'] = $HAIGISL;


//                %OCT-based IOL Formula (Hyperopic)
        $n1 = 1.376; //Corneal refractive index
        $n2 = $na; //Aqueous/vitreous index
        $pACD = (($AConstant*0.5663)-65.6+3.595)/0.9704;
        $adjustedAL = ($AL>24.4)*(sqrt($AL+0.8*($AL-24.4))) + ($AL<=24.4)*(sqrt($AL));
        $ECP = $NCP-0.0143*$AL-0.672;
        $TargetMRSECornealPlane = 1./(1./($Ref+0.0000000001)-$V/1000);
        $IOLdepth = (0.711*($ACD-$CCT/1000)+0.623*$adjustedAL-0.25*$Pp)+$pACD-8.11;
        $V1 = $n2/($AL-$IOLdepth-$CCT/1000)*1000;
        $V2a = $TargetMRSECornealPlane;
        $V2 = $ECP+$V2a;
        $OCTIOL = $V1-$n2/($n2/$V2-($IOLdepth+$CCT/1000)/1000); //OCT output
        $res['OCTIOL'] = is_nan($OCTIOL)?"--":$OCTIOL;

        return $res;
    }

    private function getRKData(){
        $res = array();
        $c = $this->getCalcData();
        $Kpre = 43.86;
        $Rpre = 337.5/$Kpre;

        $EffRP = $c->octEffrp;
        $ACP = $c->octACP;
        $Atl1= $c->octAtlf;
        $Atl2= $c->octAtls;
        $Atl3= $c->octAtlt;
        $Atl4= $c->octAtlfth;
        $ARV = ($Atl1+$Atl2+$Atl3+$Atl4)/4;

        $Km = $c->octKm;// %Take input from PWR_SF_Pupil_4.0 mm Zone textbox; mean (Km) Sagittal Front keratometry value at a 4.0-mm zone from the Power Distribution display of the Oculus Pentacam, centered over the pupil (i.e. "Zone" and "Pupil" are selected in the options on that display)
        $CT_MIN = $c->octCT_MIN;// %Take input from CT_MIN textbox; Thinnest Local pachymetry measure, obtained from Topometric display of the Oculus Pentacam (in microns)
        $NCP = $c->octNCP;//%Take input from Net Corneal Power textbox (no default value)
        $Pp =  $c->octPp;// %Take input from Posterior Corneal Power textbox (no default value)
        $CCT =  $c->octCCT;// %Take input from Central Corneal Thickness textbox (no default value)

        $RT = $this->getRt();
        $na = $this->getNa();
        $nc = $this->getNc();
        $V = $this->getV();
        $OptK3 = $this->getOptK3();
        $AL = $this->getOptAL();
        $ACD = $this->getOptACD();
        $ACD = $this->getOptACD();
        $Ref = $c->octRef;//Take input from Target Ref (D) textbox (no default value)
        $AConstant = $this->getOptAConst(); //%Take input from A-const(SRK/T) textbox (no default value)
        $SF = $this->getOptSF();// %If no AConstant entered, make this equal to value entered in SF textbox.*/


        //%Adjusted EffRP (using double-K Holladay)
        $AdjEffRP = $EffRP;
        $Kpost = $AdjEffRP;
        $Rpost = 337.5/$Kpost;
        $Rag = 7.*($Rpre<7) + $Rpre*($Rpre>=7);
        $AG = 13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5);
        $Alm = $AL + $RT;
        $HolACD = (0.56+$Rag-sqrt(abs($Rag*$Rag-($AG*$AG)/4)));
        $OACD = $HolACD+$SF;
        $AdjEffRPDKHOL = (1000*$na*($na*$Rpost-($nc-1)*$Alm-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$Alm)+($Alm*$Rpost))))/(($Alm-$OACD)*(($na*$Rpost)-($nc-1)*$OACD-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$OACD)+$OACD*$Rpost)));
        $res['AdjEffRPDKHOL'] = $AdjEffRPDKHOL;

        //%Adjusted ACCP/ACP/APP (using double-K Holladay)
        $AACP = $ACP;
        $Kpost = $AACP;
        $Rpost = 337.5/$Kpost;
        $Rag = 7.*($Rpre<7) + $Rpre*($Rpre>=7);
        $AG = 13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5);
        $Alm = $AL + $RT;
        $HolACD = (0.56+$Rag-sqrt(abs($Rag*$Rag-($AG*$AG)/4)));
        $OACD = $HolACD+$SF;
        $AACCPDKHOL = (1000*$na*($na*$Rpost-($nc-1)*$Alm-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$Alm)+($Alm*$Rpost))))/(($Alm-$OACD)*(($na*$Rpost)-($nc-1)*$OACD-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$OACD)+$OACD*$Rpost)));
        $res['AACCPDKHOL'] = $AACCPDKHOL;

        //%Adjusted Atlas Ring Values (using double-K Holladay)
        $AARV = $ARV;
        $Kpost = $AARV;
        $Rpost = 337.5/$Kpost;
        $Rag =  7*($Rpre<7) + $Rpre*($Rpre>=7);
        $AG = 13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5);
        $Alm = $AL + $RT;
        $HolACD = (0.56+$Rag-sqrt(abs($Rag*$Rag-($AG*$AG)/4)));
        $OACD = $HolACD+$SF;
        $AARVDKHOL = (1000*$na*($na*$Rpost-($nc-1)*$Alm-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$Alm)+($Alm*$Rpost))))/(($Alm-$OACD)*(($na*$Rpost)-($nc-1)*$OACD-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$OACD)+$OACD*$Rpost)));
        $res['AARVDKHOL'] = $AARVDKHOL;

        //%Pentacam (using double-K Holladay)
        $Kpost = $Km;
        $Rpost = 337.5/$Kpost;
        $Rag = 7*($Rpre<7) + $Rpre*($Rpre>=7);
        $AG =  13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5) ;
        $Alm = $AL + $RT;
        $HolACD = (0.56+$Rag-sqrt(abs($Rag*$Rag-($AG*$AG)/4)));
        $OACD = $HolACD+$SF;
        $DKHOL = (1000*$na*($na*$Rpost-($nc-1)*$Alm-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$Alm)+($Alm*$Rpost))))/(($Alm-$OACD)*(($na*$Rpost)-($nc-1)*$OACD-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$OACD)+$OACD*$Rpost)));
        $PENTACAMIOL = $DKHOL-4.4554+0.0084*$CT_MIN;
        $res['PENTACAMIOL'] = $PENTACAMIOL;

        //%IOLMaster/Lenstar (using double-K Holladay)
        $Kpost = $OptK3;
        $Rpost = 337.5/$Kpost;
        $Rag = 7.*($Rpre<7) + $Rpre*($Rpre>=7);
        $AG = 13.5*((12.5*$AL/23.45)>13.5)+(12.5*$AL/23.45)*((12.5*$AL/23.45)<=13.5);
        $Alm = $AL + $RT;
        $HolACD = (0.56+$Rag-sqrt(abs($Rag*$Rag-($AG*$AG)/4)));
        $OACD = $HolACD+$SF;
        $IOLMLENSTAR = (1000*$na*($na*$Rpost-($nc-1)*$Alm-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$Alm)+($Alm*$Rpost))))/(($Alm-$OACD)*(($na*$Rpost)-($nc-1)*$OACD-0.001*$Ref*($V*($na*$Rpost-($nc-1)*$OACD)+$OACD*$Rpost)));
        $res['IOLMLENSTAR'] = $IOLMLENSTAR;

        //%OCT-based IOL Formula (Prior RK)
        $n1 = 1.376;// %Corneal refractive index
        $n2 = 1.336; //%Aqueous/vitreous index
        $pACD = (($AConstant*0.5663)-65.6+3.595)/0.9704;
        $adjustedAL = ($AL>24.4)*(sqrt($AL+0.8*($AL-24.4))) + ($AL<=24.4)*(sqrt($AL));
        $ECP = $NCP+0.0302*$AL-2.069; //%For prior PK
        $TargetMRSECornealPlane = 1/(1/($Ref+0.0000000001)-$V/1000);
        $IOLdepth = (0.711*($ACD-$CCT/1000)+0.623*$adjustedAL-0.25*(-5.65))+$pACD-8.11; //%For prior RK
        $V1 = $n2/($AL-$IOLdepth-$CCT/1000)*1000;
        $V2a = $TargetMRSECornealPlane;
        $V2 = $ECP+$V2a;
        $OCTIOL = $V1-$n2/($n2/$V2-($IOLdepth+$CCT/1000)/1000);
        $res['OCTIOL'] = $OCTIOL;
        return $res;

    }

    /**
     * @return mixed
     */
    public function getCalcData()
    {
        return $this->calcData;
    }

    /**
     * @param mixed $calcData
     */
    public function setCalcData($calcData)
    {
        $this->calcData = $calcData;
    }


    /**
     * @return mixed
     */
    public function getOptK3()
    {
        return $this->_optK3;
    }

    /**
     * @param mixed $optK3
     */
    public function setOptK3($optK3)
    {
        $this->_optK3 = $optK3;
    }

    /**
     * @return mixed
     */
    public function getV()
    {
        return $this->_V;
    }

    /**
     * @param mixed $V
     */
    public function setV($V)
    {
        $this->_V = $V;
    }

    /**
     * @return mixed
     */
    public function getNc()
    {
        return $this->_nc;
    }

    /**
     * @param mixed $nc
     */
    public function setNc($nc)
    {
        $this->_nc = $nc;
    }

    /**
     * @return mixed
     */
    public function getNa()
    {
        return $this->_na;
    }

    /**
     * @param mixed $na
     */
    public function setNa($na)
    {
        $this->_na = $na;
    }

    /**
     * @return mixed
     */
    public function getRt()
    {
        return $this->_Rt;
    }

    /**
     * @param mixed $Rt
     */
    public function setRt($Rt)
    {
        $this->_Rt = $Rt;
    }

    /**
     * @return mixed
     */
    public function getKpre()
    {
        return $this->_Kpre;
    }

    /**
     * @param mixed $Kpre
     */
    public function setKpre($Kpre)
    {
        $this->_Kpre = $Kpre;
    }

    /**
     * @return mixed
     */
    public function getRC()
    {
        return $this->_RC;
    }

    /**
     * @param mixed $RC
     */
    public function setRC($RC)
    {
        $this->_RC = $RC;
    }

    /**
     * @return mixed
     */
    public function getRC1()
    {
        return $this->_RC1;
    }

    /**
     * @param mixed $RC1
     */
    public function setRC1($RC1)
    {
        $this->_RC1 = $RC1;
    }

    /**
     * @return mixed
     */
    public function getRC2()
    {
        return $this->_RC2;
    }

    /**
     * @param mixed $RC2
     */
    public function setRC2($RC2)
    {
        $this->_RC2 = $RC2;
    }


    /**
     * @return mixed
     */
    public function getSF()
    {
        return $this->_SF;
    }

    /**
     * @param mixed $SF
     */
    public function setSF($SF)
    {
        $this->_SF = $SF;
    }

    /**
     * @return mixed
     */
    public function getPreRfrctn()
    {
        return $this->preRfrctn;
    }

    /**
     * @param mixed $preRfrctn
     */
    public function setPreRfrctn($preRfrctn)
    {
        $this->preRfrctn = $preRfrctn;
    }

    /**
     * @return mixed
     */
    public function getPreSph()
    {
        return $this->preSph;
    }

    /**
     * @param mixed $preSph
     */
    public function setPreSph($preSph)
    {
        $this->preSph = $preSph;
    }

    /**
     * @return mixed
     */
    public function getPreVertex()
    {
        return $this->preVertex;
    }

    /**
     * @param mixed $preVertex
     */
    public function setPreVertex($preVertex)
    {
        $this->preVertex = $preVertex;
    }

    /**
     * @return mixed
     */
    public function getPreK1()
    {
        return $this->preK1;
    }

    /**
     * @param mixed $preK1
     */
    public function setPreK1($preK1)
    {
        $this->preK1 = $preK1;
    }

    /**
     * @return mixed
     */
    public function getPreK2()
    {
        return $this->preK2;
    }

    /**
     * @param mixed $preK2
     */
    public function setPreK2($preK2)
    {
        $this->preK2 = $preK2;
    }

    /**
     * @return mixed
     */
    public function getPostSph()
    {
        return $this->postSph;
    }

    /**
     * @param mixed $postSph
     */
    public function setPostSph($postSph)
    {
        $this->postSph = $postSph;
    }

    /**
     * @return mixed
     */
    public function getPostVertex()
    {
        return $this->postVertex;
    }

    /**
     * @param mixed $postVertex
     */
    public function setPostVertex($postVertex)
    {
        $this->postVertex = $postVertex;
    }

    /**
     * @return mixed
     */
    public function getPostEyeSysEffRP()
    {
        return $this->postEyeSysEffRP;
    }

    /**
     * @param mixed $postEyeSysEffRP
     */
    public function setPostEyeSysEffRP($postEyeSysEffRP)
    {
        $this->postEyeSysEffRP = $postEyeSysEffRP;
    }

    /**
     * @return mixed
     */
    public function getPostAtlasZone()
    {
        return $this->postAtlasZone;
    }

    /**
     * @param mixed $postAtlasZone
     */
    public function setPostAtlasZone($postAtlasZone)
    {
        $this->postAtlasZone = $postAtlasZone;
    }

    /**
     * @return mixed
     */
    public function getPostAtlasRingFst()
    {
        return $this->postAtlas_ring_fst;
    }

    /**
     * @param mixed $postAtlas_ring_fst
     */
    public function setPostAtlasRingFst($postAtlas_ring_fst)
    {
        $this->postAtlas_ring_fst = $postAtlas_ring_fst;
    }

    /**
     * @return mixed
     */
    public function getPostAtlasRingScd()
    {
        return $this->postAtlas_ring_scd;
    }

    /**
     * @param mixed $postAtlas_ring_scd
     */
    public function setPostAtlasRingScd($postAtlas_ring_scd)
    {
        $this->postAtlas_ring_scd = $postAtlas_ring_scd;
    }

    /**
     * @return mixed
     */
    public function getPostAtlasRingThd()
    {
        return $this->postAtlas_ring_thd;
    }

    /**
     * @param mixed $postAtlas_ring_thd
     */
    public function setPostAtlasRingThd($postAtlas_ring_thd)
    {
        $this->postAtlas_ring_thd = $postAtlas_ring_thd;
    }

    /**
     * @return mixed
     */
    public function getPostAtlasRingFhd()
    {
        return $this->postAtlas_ring_fhd;
    }

    /**
     * @param mixed $postAtlas_ring_fhd
     */
    public function setPostAtlasRingFhd($postAtlas_ring_fhd)
    {
        $this->postAtlas_ring_fhd = $postAtlas_ring_fhd;
    }

    /**
     * @return mixed
     */
    public function getPostTomeyACCP()
    {
        return $this->postTomey_ACCP;
    }

    /**
     * @param mixed $postTomey_ACCP
     */
    public function setPostTomeyACCP($postTomey_ACCP)
    {
        $this->postTomey_ACCP = $postTomey_ACCP;
    }

    /**
     * @return mixed
     */
    public function getPostGalileiTCP()
    {
        return $this->postGalilei_TCP;
    }

    /**
     * @param mixed $postGalilei_TCP
     */
    public function setPostGalileiTCP($postGalilei_TCP)
    {
        $this->postGalilei_TCP = $postGalilei_TCP;
    }

    /**
     * @return mixed
     */
    public function getPostPentacamZone()
    {
        return $this->postPentacam_zone;
    }

    /**
     * @param mixed $postPentacam_zone
     */
    public function setPostPentacamZone($postPentacam_zone)
    {
        $this->postPentacam_zone = $postPentacam_zone;
    }

    /**
     * @return mixed
     */
    public function getPostNetPower()
    {
        return $this->postNet_Power;
    }

    /**
     * @param mixed $postNet_Power
     */
    public function setPostNetPower($postNet_Power)
    {
        $this->postNet_Power = $postNet_Power;
    }

    /**
     * @return mixed
     */
    public function getPostPosteriorPower()
    {
        return $this->postPosterior_Power;
    }

    /**
     * @param mixed $postPosterior_Power
     */
    public function setPostPosteriorPower($postPosterior_Power)
    {
        $this->postPosterior_Power = $postPosterior_Power;
    }

    /**
     * @return mixed
     */
    public function getPostCentralPower()
    {
        return $this->postCentral_Power;
    }

    /**
     * @param mixed $postCentral_Power
     */
    public function setPostCentralPower($postCentral_Power)
    {
        $this->postCentral_Power = $postCentral_Power;
    }

    /**
     * @return mixed
     */
    public function getOptK1()
    {
        return $this->optK1;
    }

    /**
     * @param mixed $optK1
     */
    public function setOptK1($optK1)
    {
        $this->optK1 = $optK1;
    }

    /**
     * @return mixed
     */
    public function getOptK2()
    {
        return $this->optK2;
    }

    /**
     * @param mixed $optK2
     */
    public function setOptK2($optK2)
    {
        $this->optK2 = $optK2;
    }

    /**
     * @return mixed
     */
    public function getOptAL()
    {
        return $this->optAL;
    }

    /**
     * @param mixed $optAL
     */
    public function setOptAL($optAL)
    {
        $this->optAL = $optAL;
    }

    /**
     * @return mixed
     */
    public function getOptACD()
    {
        return $this->optACD;
    }

    /**
     * @param mixed $optACD
     */
    public function setOptACD($optACD)
    {
        $this->optACD = $optACD;
    }

    /**
     * @return mixed
     */
    public function getOptAConst()
    {
        return $this->optA_const;
    }

    /**
     * @param mixed $optA_const
     */
    public function setOptAConst($optA_const)
    {
        $this->optA_const = $optA_const;
    }

    /**
     * @return mixed
     */
    public function getOptSF()
    {
        return $this->optSF;
    }

    /**
     * @param mixed $optSF
     */
    public function setOptSF($optSF)
    {
        $this->optSF = $optSF;
    }

    /**
     * @return mixed
     */
    public function getOptHaigisAF()
    {
        return $this->optHaigis_a_f;
    }

    /**
     * @param mixed $optHaigis_a_f
     */
    public function setOptHaigisAF($optHaigis_a_f)
    {
        $this->optHaigis_a_f = $optHaigis_a_f;
    }

    /**
     * @return mixed
     */
    public function getOptHaigisAS()
    {
        return $this->optHaigis_a_s;
    }

    /**
     * @param mixed $optHaigis_a_s
     */
    public function setOptHaigisAS($optHaigis_a_s)
    {
        $this->optHaigis_a_s = $optHaigis_a_s;
    }

    /**
     * @return mixed
     */
    public function getOptHaigisAT()
    {
        return $this->optHaigis_a_t;
    }

    /**
     * @param mixed $optHaigis_a_t
     */
    public function setOptHaigisAT($optHaigis_a_t)
    {
        $this->optHaigis_a_t = $optHaigis_a_t;
    }

}