<?php

/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 07.05.2016
 * Time: 11:58
 */
class SuperFormula
{
    public $_AConstant;
    public $_SF;
    public $_ACD;
    public $_Ref;
    public $_nc;
    public $_pACD;
    public $_RT;
    public $_MWVKpr;
    public $_MWALpr;
    public $_VKpr;
    public $_ACDKonstant;
    public $_a1;
    public $_a2;
    public $_a0;
    public $_nCC;
    public $_n;
    public $_dBC;
    public $_V;
    public $_na;
    public $_NC;
    public $_ncml;
    public $_ModACD;
    public $_Offset;
    public $_REFTGT;

    function __construct()
    {

        $this->setNc(4/3);
        $this->setRT(0.2);
        $this->setMWVKpr(3.37);
        $this->setMWALpr(23.39);
        //$this->setVKpr($this->getMWVKpr());
        $this->setA1(0.4);
        $this->setA2(0.1);
        $this->setNCC(1.3315);
        $this->setN(1.336);
        $this->setDBC(12);
        $this->setV(12);
        $this->setNa(1.336);
        $this->setNcml(0.333);
        $this->resetFormulaVal();
    }

    public function resetFormulaVal($AC = 119, $Rfrctn = 0, $needInputACD = null, $AD = null, $CCT = null, $ACD = 3.5){
        $this->setAConstant($AC);
        $this->setSF($this->getAConstant() * 0.5663 - 65.6);
        //$this->setACD($ACD);
        //$this->setVKpr($this->getACD());
        $this->setACD(($this->getSF() + 3.595) / 0.9704);
        $this->setVKpr($ACD);

        $this->setACDKonstant(($this->getAConstant() * 0.62467) - 68.747);
        $this->setA0($this->getACDKonstant() / 1000 - $this->getA1() * ($this->getMWVKpr() / 1000) - $this->getA2() *
            ($this->getMWALpr() / 1000));
        //$this->setA0((($this->getACDKonstant() / 1000) - ($this->getA1() * ($this->getMWVKpr() / 1000))) - ($this->getA2() * ($this->getMWALpr() / 1000)));
        $this->setModACD(0.62467 * $this->getAConstant() - 68.747);
        $this->setOffset($this->getModACD() - 3.336);
        $this->setRef($Rfrctn);
        $this->setPACD(0.58357 * $this->getAConstant() - 63.896);
    }

    /**
     * @return mixed
     */
    public function getMWVKpr()
    {
        return $this->_MWVKpr;
    }

    /**
     * @param mixed $MWVKpr
     */
    public function setMWVKpr($MWVKpr)
    {
        $this->_MWVKpr = $MWVKpr;
    }

    public function ALpr($xx, $yy){
        return $yy;
    }

    /**
     * @return mixed
     */
    public function getMWALpr()
    {
        return $this->_MWALpr;
    }

    /**
     * @param mixed $MWALpr
     */
    public function setMWALpr($MWALpr)
    {
        $this->_MWALpr = $MWALpr;
    }

    /**
     * @return mixed
     */
    public function getVKpr()
    {
        return $this->_VKpr;
    }

    /**
     * @param mixed $VKpr
     */
    public function setVKpr($VKpr)
    {
        $this->_VKpr = $VKpr;
    }

    /**
     * @return mixed
     */
    public function getACDKonstant()
    {
        return $this->_ACDKonstant;
    }

    /**
     * @param mixed $ACDKonstant
     */
    public function setACDKonstant($ACDKonstant)
    {
        $this->_ACDKonstant = $ACDKonstant;
    }

    /**
     * @return mixed
     */
    public function getA1()
    {
        return $this->_a1;
    }

    /**
     * @param mixed $a1
     */
    public function setA1($a1)
    {
        $this->_a1 = $a1;
    }

    /**
     * @return mixed
     */
    public function getA2()
    {
        return $this->_a2;
    }

    /**
     * @param mixed $a2
     */
    public function setA2($a2)
    {
        $this->_a2 = $a2;
    }

    /**
     * @return mixed
     */
    public function getA0()
    {
        return $this->_a0;
    }

    /**
     * @param mixed $a0
     */
    public function setA0($a0)
    {
        $this->_a0 = $a0;
    }

    /**
     * @return mixed
     */
    public function getNCC()
    {
        return $this->_nCC;
    }

    /**
     * @param mixed $nCC
     */
    public function setNCC($nCC)
    {
        $this->_nCC = $nCC;
    }

    /**
     * @return mixed
     */
    public function getN()
    {
        return $this->_n;
    }

    /**
     * @param mixed $n
     */
    public function setN($n)
    {
        $this->_n = $n;
    }

    /**
     * @return mixed
     */
    public function getDBC()
    {
        return $this->_dBC;
    }

    /**
     * @param mixed $dBC
     */
    public function setDBC($dBC)
    {
        $this->_dBC = $dBC;
    }

    /**
     * @return mixed
     */
    public function getV()
    {
        return $this->_V;
    }

    /**
     * @param mixed $V
     */
    public function setV($V)
    {
        $this->_V = $V;
    }

    /**
     * @return mixed
     */
    public function getNa()
    {
        return $this->_na;
    }

    /**
     * @param mixed $na
     */
    public function setNa($na)
    {
        $this->_na = $na;
    }

    /**
     * @return mixed
     */
    public function getNcml()
    {
        return $this->_ncml;
    }

    /**
     * @param mixed $ncml
     */
    public function setNcml($ncml)
    {
        $this->_ncml = $ncml;
    }

    /**
     * @return mixed
     */
    public function getModACD()
    {
        return $this->_ModACD;
    }

    /**
     * @param mixed $ModACD
     */
    public function setModACD($ModACD)
    {
        $this->_ModACD =  $this->toPrecis($ModACD);
    }

    /**
     * @return mixed
     */
    public function getOffset()
    {
        return $this->_Offset;
    }

    /**
     * @param mixed $Offset
     */
    public function setOffset($Offset)
    {
        $this->_Offset = $this->toPrecis($Offset);
    }

    /**
     * @return mixed
     */
    public function getREFTGT()
    {
        return $this->_REFTGT;
    }

    /**
     * @param mixed $REFTGT
     */
    public function setREFTGT($REFTGT)
    {
        $this->_REFTGT = $REFTGT;
    }

    /**
     * @return mixed
     */
    public function getRT()
    {
        return $this->_RT;
    }

    /**
     * @param mixed $RT
     */
    public function setRT($RT)
    {
        $this->_RT = $RT;
    }

    /**
     * @return mixed
     */
    public function getAConstant()
    {
        return $this->_AConstant;
    }

    /**
     * @param mixed $AConstant
     */
    public function setAConstant($AConstant)
    {
        $this->_AConstant = $AConstant;
    }

    /**
     * @return mixed
     */
    public function getSF()
    {
        return $this->_SF;
    }

    /**
     * @param mixed $SF
     */
    public function setSF($SF)
    {
        $this->_SF = $SF;
    }

    /**
     * @return mixed
     */
    public function getACD()
    {
        return $this->_ACD;
    }

    /**
     * @param mixed $ACD
     */
    public function setACD($ACD)
    {
        $this->_ACD = $ACD;
    }

    /**
     * @return mixed
     */
    public function getRef()
    {
        return $this->_Ref;
    }

    /**
     * @param mixed $Ref
     */
    public function setRef($Ref)
    {
        $this->_Ref = $Ref;
        $this->setREFTGT($Ref);
    }

    /**
     * @return mixed
     */
    public function getNc()
    {
        return $this->_nc;
    }

    /**
     * @param mixed $nc
     */
    public function setNc($nc)
    {
        $this->_nc =($nc);
    }

    /**
     * @return mixed
     */
    public function getPACD()
    {
        return $this->_pACD;
    }

    /**
     * @param mixed $pACD
     */
    public function setPACD($pACD)
    {
        $this->_pACD = $pACD;
    }

    /**
     * possible double _R
     */
    public function r($xx, $yy){
        return (
            $this->_R($xx, $yy)
        );
    }

    private function _R($xx, $yy)
    {
        return (337.5 / $xx);
    }

    private function _HofRef()
    {
        return ($this->getRef() / (1 - 0.012 * $this->getRef()));
    }

    private function _HofAL($xx, $yy)
    {
        return ((18.5 * ($yy < 18.5) + 31 * ($yy > 31)) + ($yy * (($yy >= 18.5) && ($yy <= 31))));
    }

    private function _M($xx, $yy)
    {
        return (1 * ($yy <= 23) + -1 * ($yy > 23));
    }

    private function _G($xx, $yy)
    {
        return (28 * ($yy <= 23) + 23.5 * ($yy > 23));
    }

    private function _HofACDPlaceholder($xx, $yy)
    {

        return (
            $this->getPACD() + 0.3 * ($this->_HofAL($xx, $yy) - 23.5) + (pow(tan($this->radians($xx)), 2)) +
            (0.1 * $this->_M($xx, $yy) * (pow((23.5 - $this->_HofAL($xx, $yy)), 2)) *
                $this->radians((0.1 * (pow(($this->_G($xx, $yy) - $this->_HofAL($xx, $yy)), 2))))) - 0.99166
        );

//        return (((
//                ($this->getPACD() + (0.3 * ($this->_HofAL($xx, $yy) - 23.5))) +
//                (pow(tan($this->radians($xx)), 2))) +
//            ((
//                    (0.1 * $this->_M($xx, $yy)) *
//                    (pow((23.5 - $this->_HofAL($xx, $yy)), 2))) *
//                $this->radians((0.1 * (pow(($this->_G($xx, $yy) - $this->_HofAL($xx, $yy)), 2))))))) - 0.99166;

    }

    private function _HofACD($xx, $yy)
    {
        return ((2.5 * ($this->_HofACDPlaceholder($xx, $yy) < 2.5) + 6.5 * ($this->_HofACDPlaceholder($xx, $yy) > 6.5)) +
            $this->_HofACDPlaceholder($xx, $yy) *
            (($this->_HofACDPlaceholder($xx, $yy) >= 2.5) && ($this->_HofACDPlaceholder($xx, $yy) <= 6.5)));
    }

    private function _Rag($xx, $yy)
    {
        return (7 * ($this->_R($xx, $yy) < 7) + $this->_R($xx, $yy) * ($this->_R($xx, $yy) >= 7));
    }

    private function _AG($xx, $yy)
    {
        return (
            13.5 * ((12.5 * $yy / 23.45) > 13.5) + (12.5 * $yy / 23.45) * ((12.5 * $yy / 23.45) <= 13.5)
        );
    }

    private function _HolACD($xx, $yy)
    {
        return (
            (0.56 + $this->_Rag($xx, $yy)) - sqrt((($this->_Rag($xx, $yy) * $this->_Rag($xx, $yy)) -
                (($this->_AG($xx, $yy) * $this->_AG($xx, $yy)) / 4)))
        );
    }

    private function _KochAL($xx, $yy)
    {
        return ((0.8814 * $yy) + 2.8701);
    }

    private function _ALpr($xx, $yy)
    {
        return $yy;
    }

    private function _d($xx, $yy)
    {
        return (
            $this->getA0() + $this->getA1() * ($this->getVKpr() / 1000) + $this->getA2() * ($this->_ALpr($xx, $yy) / 1000)
//            ($this->getA0() +
//                (($this->getA1() * $this->getVKpr()) / 1000)) +
//            (($this->getA2() * $this->_ALpr($xx, $yy)) / 1000)
        );
    }

    private function _RC($xx, $yy)
    {
        return (337.5 / $xx);
    }

    private function _DC($xx, $yy)
    {
        return (
            ($this->getNCC() - 1) / ($this->_RC($xx, $yy) / 1000)
        );
    }

    private function _Zee($xx, $yy)
    {
        return (
            $this->_DC($xx, $yy) + ($this->getRef() / (1 - $this->getRef() * ($this->getDBC() / 1000)))
            //$this->_DC($xx, $yy) + ($Ref / (1 - (($Ref * $this->getDBC()) / 1000)))
        );
    }

    private function __r($xx, $yy)
    {
        return $this->_R($xx, $yy);
    }

    private function _LCOR($x, $y)
    {
        return (
            $y * ($y <= 24.2) + (-3.446 + 1.715 * $y - 0.0237 * pow($y, 2)) *($y > 24.2)
            //$y * ($y <= 24.2) + (-3.446 + 1.715 * $y - 0.0237 * pow($y, 2) * ($y > 24.2))
        );
    }

    private function _Cw($x, $y)
    {
        return (
            -5.41 + 0.58412 * $this->_LCOR($x, $y) + 0.098 * $x
        );
    }

    private function _H($x, $y)
    {
        return (
            -10.326 + 0.32630 * $y + 0.13533 * $x
        );
//        return (
//            (($this->__r($x, $y) - sqrt(abs($this->__r($x, $y) * $this->__r($x, $y) -
//                    (($this->_Cw($x, $y) * $this->_Cw($x, $y)) / 4))))) * (($this->__r($x, $y) *
//                    $this->__r($x, $y) - (($this->_Cw($x, $y) * $this->_Cw($x, $y)) / 4)) >= 0)
//            + (($this->__r($x, $y) - sqrt(0))) * (($this->__r($x, $y) * $this->__r($x, $y) - (
//                        ($this->_Cw($x, $y) * $this->_Cw($x, $y)) / 4)) < 0));
    }

    private function _NegTest($x, $y)
    {
        return (
            $this->r($x, $y) * $this->r($x, $y) - (($this->_Cw($x, $y) * $this->_Cw($x, $y)) / 4)
            //$this->__r($x, $y) * $this->__r($x, $y) - (($this->_Cw($x, $y) * $this->_Cw($x, $y)) / 4)
        );
    }

    private function _ACDest($x, $y)
    {
        return (
            $this->_H($x, $y) + $this->getOffset()
        );
    }

    private function _RETHICK($x, $y)
    {
        return (
            0.65696 - 0.02029 * $y
        );
    }

    private function _LOPT($x, $y)
    {
        return (
            $y + $this->_RETHICK($x, $y)
        );
    }

    private function _SRKI($x, $y)
    {
        return (($this->getAConstant() - (0.9 * $x)) - (2.5 * $y));
    }

    private function _HOFFERQ($x, $y)
    {
        return (
            1336 / ($y - $this->_HofACD($x, $y) - 0.05)) - (1.336 / ((1.336 / ($x + $this->_HofRef())) -
                (($this->_HofACD($x, $y) + 0.05) / 1000))
//            1336 / ($y - $this->_HofACD($x, $y) - 0.05)) - (1.336 / ((1.336 / ($x + $this->_HofRef($Ref))) -
//                (($this->_HofACD($x, $y) + 0.05) / 1000))
        );
    }

    private function _HOLLADAY($x, $y)
    {
        $__na = $this->getNa();
        $__nc = $this->getNc();
        $__R = $this->_R($x, $y);
        $__RT = $this->getRT();
        $__V = $this->getV();
        $__SF = $this->getSF();
        $__HolACD = $this->_HolACD($x, $y);

        return (

            (1000 * $this->getNa() * ($this->getNa() * $this->_R($x, $y) - ($this->getNc() - 1) * ($y + $this->getRT()) - 0.001 *
                    $this->getRef() * ($this->getV() * ($this->getNa() * $this->_R($x, $y) - ($this->getNc() - 1) *
                            ($y + $this->getRT())) + ($y + $this->getRT()) * $this->_R($x, $y)))) / ((($y + $this->getRT()) -
                    $this->_HolACD($x, $y) - $this->getSF()) * ($this->getNa() * $this->_R($x, $y) - ($this->getNc() - 1) *
                    ($this->_HolACD($x, $y) + $this->getSF()) - 0.001 * $this->getRef() * ($this->getV() * ($this->getNa() *
                            $this->_R($x, $y) - ($this->getNc() - 1) * ($this->_HolACD($x, $y) + $this->getSF())) +
                        ($this->_HolACD($x, $y) + $this->getSF()) * $this->_R($x, $y))))

//            (1000. * $__na * ($__na * $__R - ($__nc - 1) * ($y + $__RT) -
//                    0.001 * $Ref * ($__V * ($__na * $__R - ($__nc - 1) * ($y + $__RT)) +
//                        ($y + $__RT) * $__R))) / ((($y + $__RT) -
//                    $__HolACD - $__SF) * ($__na * $__R -
//                    ($__nc - 1) * ($__HolACD + $__SF) -
//                    0.001 * $Ref * ($__V * ($__na * $__R -
//                            ($__nc - 1) * ($__HolACD + $__SF)) +
//                        ($__HolACD + $__SF) * $__R)))
        );
    }

    private function _KOCH($x, $y, $Ref)
    {
        return ((1000. * $this->getNa() * ($this->getNa() * $this->_R($x, $y) -
                    ($this->getNc() - 1) * ($this->_KochAL($x, $y) + $this->getRT()) -
                    0.001 * $Ref * ($this->getV() * ($this->getNa() * $this->_R($x, $y) - ($this->getNc() - 1) * ($this->_KochAL($x, $y) + $this->getRT())) +
                        ($this->_KochAL($x, $y) + $this->getRT()) * $this->_R($x, $y)))) / ((($this->_KochAL($x, $y) + $this->getRT()) -
                    $this->_HolACD($x, $y) - $this->getSF()) * ($this->getNa() * $this->_R($x, $y) - ($this->getNc() - 1) * ($this->_HolACD($x, $y) + $this->getSF()) -
                    0.001 * $Ref * ($this->getV() * ($this->getNa() * $this->_R($x, $y) - ($this->getNc() - 1) * ($this->_HolACD($x, $y) + $this->getSF())) +
                        ($this->_HolACD($x, $y) + $this->getSF()) * $this->_R($x, $y)))));
    }

    private function _HAIGIS($x, $y)
    {
        return (
            $this->getN() / ($y / 1000 - $this->_d($x, $y)) - ($this->getN() / ($this->getN() / $this->_Zee($x, $y) - $this->_d($x, $y)))
        );
    }

    private function _SRKTHOLD($x, $y)
    {
        return (1000 * $this->getNa() * ($this->getNa() * $this->__r($x, $y) - $this->getNcml() * $this->_LOPT($x, $y) -
                0.001 * $this->getREFTGT() * ($this->getV() * ($this->getNa() * $this->__r($x, $y) - $this->getNcml() * $this->_LOPT($x, $y))
                    + $this->_LOPT($x, $y) * $this->__r($x, $y)))) / (($this->_LOPT($x, $y) -
                $this->_ACDest($x, $y)) * ($this->getNa() * $this->__r($x, $y) - $this->getNcml() * $this->_ACDest($x, $y) -
                0.001 * $this->getREFTGT() * ($this->getV() * ($this->getNa() * $this->__r($x, $y) - $this->getNcml() * $this->_ACDest($x, $y))
                    + $this->_ACDest($x, $y) * $this->__r($x, $y))));
    }

    private function _SRKT2($x, $y){
        return (

            1000 * $this->getNa() * ($this->getNa() * $this->r($x, $y) - $this->getNcml() * $this->_LOPT($x, $y) - 0.001 *
                $this->getREFTGT() * ($this->getV() * ($this->getNa() * $this->r($x, $y) - $this->getNcml() * $this->_LOPT($x, $y)) +
                    $this->_LOPT($x, $y) * $this->r($x, $y)))) / (($this->_LOPT($x, $y) - $this->_ACDest($x, $y)) *
            ($this->getNa() * $this->r($x, $y) - $this->getNcml() * $this->_ACDest($x, $y) - 0.001 * $this->getREFTGT() *
                ($this->getV() * ($this->getNa() * $this->r($x, $y) - $this->getNcml() * $this->_ACDest($x, $y)) +
                    $this->_ACDest($x, $y) * $this->r($x, $y)))

//            1000 * $this->getNa() * ($this->getNa() * $this->_R($x, $y) - $this->getNcml() * $this->_LOPT($x, $y) - 0.001 *
//                $this->getREFTGT() * ($this->getV() * ($this->getNa() * $this->_R($x, $y) - $this->getNcml() * $this->_LOPT($x, $y) +
//                        $this->_LOPT($x, $y) * $this->_R($x, $y)))) / (($this->_LOPT($x, $y) - $this->_ACDest($x, $y)) *
//                ($this->getNa() * $this->_R($x, $y) - $this->getNcml() * $this->_ACDest($x, $y) - 0.001 *
//                    $this->getREFTGT() * ($this->getV() * ($this->getNa() * $this->_R($x, $y) - $this->getNcml() * $this->_ACDest($x, $y)) +
//                        $this->_ACDest($x, $y) * $this->_R($x, $y))))
        );
    }

    public function _SUPERFORMULA($x, $y)
    {
        return (
            $this->_HOFFERQ($x, $y) * ($y <= 21.49) +
            $this->_SRKT2($x, $y) * (($y > 25) && ($this->_SRKT2($x, $y) >= 0)) +
        $this->_HOLLADAY($x, $y) * (($y > 21.49) && ($y <= 25)) +
        $this->_HAIGIS($x, $y) * ($this->_SRKT2($x, $y) < 0)
        );

//        return ($this->_HOFFERQ($x, $y, $Ref) * ($y <= 21.49) +
//            $this->_KOCH($x, $y, $Ref) * (($y > 25) && ($this->_KOCH($x, $y, $Ref) >= 0)) +
//            $this->_HOLLADAY($x, $y, $Ref) * (($y > 21.49) && ($y <= 25)) +
//            $this->_HAIGIS($x, $y, $Ref) * ($this->_KOCH($x, $y, $Ref) < 0));
    }


    public function radians($deg)
    {
        return $deg * M_PI / 180;
    }

    public function degrees($rad)
    {
        return $rad * 180 / M_PI;
    }

    private function toPrecis($val, $precision = 13)
    {
        return floatval(number_format((float)$val, $precision, '.', ''));
    }
}
