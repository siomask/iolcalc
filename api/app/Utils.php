<?php

/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 18.05.2016
 * Time: 12:32
 */
class Utils
{
    private $Result;
    private $startResult;
    private $updateFloatRes;
    private $updateFloatIter;
    private $updateFloatCurInd;

    function __construct()
    {
        $this->setUpdateFloatCurInd(3);
        $this->setUpdateFloatIter(0);
        $this->setUpdateFloatRes(1000);
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->Result;
    }

    /**
     * @param mixed $Result
     */
    public function setResult($Result)
    {
        if (preg_match("/E/", $Result)) $Result *= 1000000000;
        $this->Result = $Result;
    }

    /**
     * @return mixed
     */
    public function getUpdateFloatRes()
    {
        return $this->updateFloatRes;
    }

    /**
     * @param mixed $updateFloatRes
     */
    public function setUpdateFloatRes($updateFloatRes)
    {
        $this->updateFloatRes = $updateFloatRes;
    }

    /**
     * @return mixed
     */
    public function getUpdateFloatIter()
    {
        return $this->updateFloatIter;
    }

    /**
     * @param mixed $updateFloatIter
     */
    public function setUpdateFloatIter($updateFloatIter)
    {
        $this->updateFloatIter = $updateFloatIter;
    }

    /**
     * @return mixed
     */
    public function getUpdateFloatCurInd()
    {
        return $this->updateFloatCurInd;
    }

    /**
     * @param mixed $updateFloatCurInd
     */
    public function setUpdateFloatCurInd($updateFloatCurInd)
    {
        $this->updateFloatCurInd = $updateFloatCurInd;
    }


    private function fNeares($_num, &$func, $round,$withoutAbs=false)
    {
        $res =  abs($func($_num));
        return $res;
    }

    private function checkNearest($_nearest, $_step, &$func, $round)
    {
        $na = ($_nearest + $_step);
        $nb = ($_nearest - $_step);
        $rA = $this->fNeares($na, $func, $round);
        $rB = $this->fNeares($nb, $func, $round);
        $res = ($rA < $rB ? $na : $nb);
        return $res;
    }

    private function updateFloat($res)
    {
        $result = $res;
        if ($this->getUpdateFloatCurInd() > 9) throw new Exception($this->getResult());

        if ($this->getUpdateFloatIter() == 0) {
            $result = $this->getResult();

            $result = floatval(substr_replace($result, 0, $this->getUpdateFloatCurInd(), 1));
            $this->setUpdateFloatIter(1);
        } else if ($this->getUpdateFloatIter() > 9) {
            $this->setUpdateFloatIter(0);
            $this->setUpdateFloatCurInd($this->getUpdateFloatCurInd() + 1);
            $result = floatval(substr_replace($result, 0, $this->getUpdateFloatCurInd(), 1));

        } else {

            $result = (float)(substr_replace($result, $this->getUpdateFloatIter(), $this->getUpdateFloatCurInd(), 1));
            $this->setUpdateFloatIter($this->getUpdateFloatIter() + 1);
        }

        return $result;
    }

    private function _check($reslt, $step, &$func, $round, $up, $updateFloat, $maxIter)
    {
        $avIter = 5000;
        if ($maxIter++ > $avIter) throw new Exception($this->getResult());
//        if (preg_match("/E/", $reslt)) $reslt *= 1000000000;

        if ($updateFloat) {
            $result = $this->updateFloat($reslt);
            $fnear = $this->fNeares($result, $func, $round);
        } else {
            $result = $this->checkNearest($reslt, $step, $func, $round);
            $fnear = $this->fNeares($result, $func, $round);

            $this->setUpdateFloatCurInd((strrpos($result, ".") + 1));
            if ($fnear < $this->getUpdateFloatRes()) {
                $this->setUpdateFloatRes($fnear);
                $this->setResult($result);
            }

        }
        $_value = abs(round($fnear, $round));
        if ($_value >1/10000) {
            if ($updateFloat || abs($fnear)<1/100000 /*&& $maxIter > ($avIter / 100))*/) {
                if ($fnear < $this->getUpdateFloatRes()) {
                    $this->setResult($result);
                    $this->setUpdateFloatRes($fnear);
                }
                $this->_check($result, $result * 0.5, $func, $round, $up, true, $maxIter);
            } else {
                $down = 1;
                $st = $result * 0.5;
                $__val = abs($result);
                if ($__val<1/10000000 &&$this->startResult/$result>0) {
                    $result=-1;
                    $st = $result*0.5;
                }
                $this->_check($result * $down, $st, $func, $round, $up, $updateFloat, $maxIter);
            }

        } else {
            $this->setResult(($result));
        }

    }

    public function fzero(&$func, $num)
    {
        if ($func instanceof Closure && (gettype($num) == 'integer' || gettype($num) == 'double')) {
            $this->setUpdateFloatRes(100000);
            $this->setUpdateFloatCurInd(3);
            $this->setUpdateFloatIter(0);
            $up = true;
            $Step = $num / 2;
            $Numb = $num;
            $this->Result =$this->startResult = $num;
            $round = 3;
            $maxIter = 0;
            if (round($this->fNeares($num, $func, $round), $round) != 0) {
                $this->_check($Numb, $Step, $func, $round, $up, false, $maxIter);
            }

            return $this->Result;
        }
    }
}