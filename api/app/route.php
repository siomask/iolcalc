<?php
header('Access-Control-Allow-Origin:  *');
require_once("app/Mcalc.php");

global $config;
$request = (object)$_REQUEST;
$post = json_decode(file_get_contents("php://input"));
$get = (object)$_GET;
if (!isset($_SESSION)) {
    session_start();

}
if(isSet($_SESSION['user'])){
    if (isSet($_SESSION['started']) && ((mktime() - $_SESSION['started'] - 60*60) > 0)){
        session_unset();
        session_destroy();
        die(json_encode(array("session_time_out"=>"true")));
    } else {
        $_SESSION['started'] = mktime();
    }
}




if (!empty($request->method) || !empty($post->method)) {
    $fgr =   new Mcalc();
    $method = empty($request->method) ? $post->method : $request->method;
    $data = false;
//if($_SESSION['is_requesting'] == "yeas"){
//    $data = array("error"=>"please, wait until your previous request will finish");
//}else {
//    $_SESSION['is_requesting'] = "yeas";
    switch ($method) {
        case "filesMdb":{
            var_dump($_FILES);
//            $data = $fgr->parsemdb();
            break;
        }
        case "backUp":
            $data = $fgr->backUp($post->data);
            break;
        case "deleteFile":
            $data = $fgr->deleteFile($post->data);
            break;
        case "saveUser":
            $data = $fgr->saveUser($post->data);
            if (!$data['error']) {
                $_SESSION['user'] = $data['data'];

            }
            break;
        case "getUser":
            $data = $fgr->getUser($post->data);
            if (!isset($data['error'])) {
                $info = preg_split("/ /", $data['data']['usr_name']);
                $data['data']['firstName'] = $info[0];
                $data['data']['secondName'] = $info[1];
                $_SESSION['user'] = $data['data'];
            }
            break;
        case "getUseres": {
            $data = $fgr->getUseres($post->data, false);
            break;
        }
        case "getCalculations": {
            $data = $fgr->getCalculations();
            break;
        }
        case "editCalculations": {
            $data = $fgr->editCalculations($post->data);
            break;
        }
        case "dropUser":
            $data = $fgr->dropUser($post->data);
            if ($post->data && $post->data->user && $post->data->user->id_user == $_SESSION['user']['id_user']) {
                session_destroy();
            }
            break;
        case "updateUserInfo":
            $data = $fgr->updateUserInfo($post->data);
            break;
        case "getSession":
            $stat = array("statistic" => array("st_visites" => 1), "user" => $_SESSION['user']);
            $fgr->setStatistic(json_decode(json_encode($stat)));
            $data = array('session' => $_SESSION);
            break;
        case "clearSession":
            session_destroy();
            $data = array('session' => array());
            break;
        case "changePsw":
            $data = $fgr->updatePassword($post->data);
            break;
        case "resetPsw":
            $data = $fgr->updatePassword($request, "resetPsw");
            break;
        case "savePromoCode":
            $data = $fgr->promoCode($post->data, 'save');
            break;
        case "editCurCode":
            $data = $fgr->promoCode($post->data, 'update');
            break;
        case "dropCurCode":
            $data = $fgr->promoCode($post->data, 'drop');
            break;
        case "setAdmin":
            $data = $fgr->updateUser($post->data, 'setAdmin');
            break;
        case "activateUser":
            $data = $fgr->updateUser($post->data, 'activateUser');
            break;
        case "getSetData":
            $data = $fgr->getSetData($post->data);
            break;
        case "reqAccess":
            $data = $fgr->reqAccess($post->data);
            break;
        case "dropMsg":
            $data = $fgr->messages($post->data, "dropMsg");
            break;
        case "editMsg":
            $data = $fgr->messages($post->data, "editMsg");
            break;
        case "acceptNote":
            $data = $fgr->notesEdit($post->data, 'acceptNote');
            break;
        case "declineNote":
            $data = $fgr->notesEdit($post->data, 'declineNote');
            break;
        case "updateDefVal":
            $data = $fgr->defValEdit($post->data);
            break;
        case "calculate":
            $data = $fgr->_plot($post->data);
//            var_dump(json_last_error_msg());
            break;
        case "statistic":
            $data = $fgr->setStatistic($post->data);
            break;
        case "loadStatistic":
            $data = $fgr->loadStatistic($post->data);
            break;
        case "contact":
            $data = $fgr->contact($post->data);
            break;
        default:
            $data = array(
                'error' => "false request"
            );
            break;
    }
    $_SESSION['is_requesting'] = null;
//}

    echo(json_encode( Mcalc::utf8ize($data)));
} else {
    die(json_encode(array(
        'error' => "Empty request method!"
    )));
}





