<?php
/**
 * Created by PhpStorm.
 * User: edinorog
 * Date: 31.08.17
 * Time: 14:05
 */
require_once("config.php");
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);
if (mysqli_connect_errno()) {
    printf("Не удалось подключиться: %s\n", mysqli_connect_error());
    exit();
}
$tables = array('mc_promo_codes', 'mc_users', 'mc_msg', 'mc_notes', 'mc_default_val', 'mc_statistic', 'mc_user_calcs');
$sql = "Select * from `{$tables[6]}`";

function decryptData($key, $string)
{
    if (!(isset($string) || !empty($string))) {
        return $string;
    }


    $data = base64_decode($string);
    $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
    $_result = null;

    $_tesmp = mcrypt_decrypt(
        MCRYPT_RIJNDAEL_128,
        hash('sha256', $key, true),
        substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
        MCRYPT_MODE_CBC,
        $iv
    );

    $_result = rtrim($_tesmp, "\0");
    return $_result;

}

$result = mysqli_query($conn, $sql);
$resF = '';
$itr =0;
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
$str = 'Update `'.$tables[6].'` set patient_id ="' . decryptData($row['id_user'], $row['patient_id']) . '",patient_name ="' . decryptData($row['id_user'], $row['patient_name']) . '" Where id_calc = ' . $row['id_calc'] . '; <br/>';
    str_replace('"','.',$str);
    echo $str;
    if($itr++ >10)die();
}

?>
<script>
    function download(content, filename, contentType)
    {
        if(!contentType) contentType = 'application/octet-stream';
        var a = document.createElement('a');
        var blob = new Blob([content], {'type':contentType});
        a.href = window.URL.createObjectURL(blob);
        a.download = filename;
        a.click();
    }
    download(document.body.innerText,'test.sql')
</script>
