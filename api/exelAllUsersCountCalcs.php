<?php
require_once("./app/Mcalc.php");
$calc = new Mcalc();
$res = $calc->getUsrNameEmailCountOfCalcs()['users'];
?>

<script type="text/javascript" src="../bower_components/excellentexport/excellentexport.min.js"></script>
<a download="Calculations.xls" id="exportEXL" style=" font-size: 32px;" href="#"
   onclick="return ExcellentExport.excel(this, 'calcsDatatable', 'Calculations');">Export to Excel</a>
<table id="calcsDatatable">
    <thead>
    <tr>
        <th>Count of Calcs</th>
        <th>User name</th>
        <th>User email</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($res as $key => $val): ?>
        <tr>
            <td><?php echo $val['counts']; ?></td>
            <td><?php echo $val['usr_name']; ?></td>
            <td><?php echo $val['usr_email']; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
