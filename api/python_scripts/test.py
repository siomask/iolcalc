import pandas as pd
import pyrenn as prn
import numpy as np
import openpyxl as opel
import sys



AL = float(sys.argv[4])
K = float(sys.argv[2])
ACD = float(sys.argv[3])
urlF = str(sys.argv[1])
net = prn.loadNN(urlF)
adjustment = prn.NNOut(np.array([[AL],[K],[ACD]]),net)[0]
if adjustment > 0.5:
    adjustment = 0.5
elif adjustment < -0.5:
    adjustment = -0.5

print str(adjustment)
