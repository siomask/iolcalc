<?php
$dom = new DOMDocument();
$dom->loadHTMLFile("http://ocusoft.de/ulib/c1.htm");
$tables = $dom->getElementsByTagName('table');
$trs = $tables[1]->getElementsByTagName('tr');
$res = array();

foreach ($trs as $key => $tr) {
    if ($key == 0) continue;
    $AL= trim($tr->childNodes[5]->textContent);
    $res[] = array("AL" =>  floatval(explode("=", $AL)[1]), "IOL" => trim($tr->childNodes[0]->textContent));
}
file_put_contents('alData.json', json_encode($res));
?>