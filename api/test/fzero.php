<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 08.08.18
 * Time: 9:35
 */

require_once("../app/SuperFormula.php");
require_once("../app/Mcalc.php");
require_once("../app/Utils.php");

$_d =  $_REQUEST['data'];
$_const = array(1.3375);
$_formVal = array('K1', 'K2', 'axialLength', 'AConstant', 'Rfrctn', 'ACD', 'KIndex', 'AD', 'CCT', 'needInputACD');
$resp = array("error" => "you forgot to input the ");


$mcalc = new Mcalc();
//$ACD =/*0;//*/4.88;
//$_AC =/*118;//*/119.2;
//$AL =/*21;//2*/5.6;
//$Ref = $Rfrctn= /*2;//*/-2.5;
//$K1=/*44;//*/51.77;
//$K2=/*44;//*/53.65;

$ACD = 4.88;
$_AC = 119.2;
$AL = 25.6;
$Ref = $Rfrctn= -2.5;
$K1= 51.77;
$K2=53.65;
$K3 = (($K1 + $K2) / 2);
$KIndex = 1.3375;
$AdjustedInputK3 = ($K3 * ($_const[0] - 1)) / ($KIndex - 1);




$mcalc->setLadasForm(new SuperFormula());
$mcalc->setUtils(new Utils());

$_tLS =$t= $mcalc->getLadasForm();
$_tLS->setRef($Ref);
$_tLS->resetFormulaVal(
    $_AC,
    $Ref,
    null,
    null,
    null,
    $ACD
);
$refInd = $_tLS->_SUPERFORMULA(
    $AdjustedInputK3,
    $AL,
    $Ref
);

//var_dump($AdjustedInputK3, $AL, $Rfrctn);exit();
$refInd = $t->_SUPERFORMULA($AdjustedInputK3, $AL, $Rfrctn);
$yPst = ($refInd >= 10) ? $t->_SUPERFORMULA($K3, $AL, $Rfrctn) : $refInd;
$t->setRef($Ref);


$iolRef = $mcalc->iolRefCreate($AdjustedInputK3, $AL, $Ref, null);

var_dump($iolRef);