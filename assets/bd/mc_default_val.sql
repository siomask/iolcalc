-- phpMyAdmin SQL Dump
-- version 4.0.10.12
-- http://www.phpmyadmin.net
--
-- Host: sitc.mysql.ukraine.com.ua
-- Generation Time: May 10, 2016 at 10:06 AM
-- Server version: 5.6.27-75.0-log
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sitc_shared`
--

-- --------------------------------------------------------

--
-- Table structure for table `mc_default_val`
--

CREATE TABLE IF NOT EXISTS `mc_default_val` (
  `id_val` int(11) NOT NULL AUTO_INCREMENT,
  `Rfrctn` float DEFAULT NULL,
  `K1` float DEFAULT NULL,
  `K2` float DEFAULT NULL,
  `axialLength` float DEFAULT NULL,
  `ACD` float DEFAULT NULL,
  `AConstant` float DEFAULT NULL,
  `id_user` int(10) NOT NULL,
  `KIndex` float DEFAULT NULL,
  PRIMARY KEY (`id_val`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `mc_default_val`
--

INSERT INTO `mc_default_val` (`id_val`, `Rfrctn`, `K1`, `K2`, `axialLength`, `ACD`, `AConstant`, `id_user`, `KIndex`) VALUES
(24, NULL, NULL, NULL, NULL, NULL, NULL, 42, 1.31),
(25, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1.332);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
