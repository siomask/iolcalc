-- phpMyAdmin SQL Dump
-- version 4.0.10.12
-- http://www.phpmyadmin.net
--
-- Host: sitc.mysql.ukraine.com.ua
-- Generation Time: May 10, 2016 at 10:05 AM
-- Server version: 5.6.27-75.0-log
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sitc_shared`
--

-- --------------------------------------------------------

--
-- Table structure for table `mc_msg`
--

CREATE TABLE IF NOT EXISTS `mc_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(10000) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `fromeName` varchar(50) NOT NULL DEFAULT 'IOLcalc.com',
  `category` varchar(50) NOT NULL,
  `fromeEmail` varchar(50) NOT NULL DEFAULT 'admin@iolcalc.com',
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `mc_msg`
--

INSERT INTO `mc_msg` (`id`, `text`, `subject`, `fromeName`, `category`, `fromeEmail`) VALUES
(1, '<p>Dear Dr. *USER_NAME*,</p><p>Welcome to IOLcalc.com. We are happy to have you onboard. You can now use the Ladas Super Formula by logging in at <a href="http://www.iolcalc.com/" target="_blank" data-mce-href="http://www.iolcalc.com/">IOLcalc.com</a>.</p><p>Since the interface is still in beta-testing phase, we would appreciate your feedback. We want to make this interface as user-friendly as possible so that surgeons like yourself find it useful.</p><p>Feel free to get in touch with us at <a href="mailto:admin@iolcalc.com" target="_blank" data-mce-href="mailto:admin@iolcalc.com">admin@iolcalc.com</a>.</p><p>Thank you,<br>John, Aazim, Uday, &amp; Bert</p>', 'Thank you for signing up to IOLcalc.com', 'IOLcalc.com', 'AUTH_USER', 'admin@iolcalc.com'),
(3, '<p>Dear Dr. *USER_NAME*,</p><p>We have deleted&nbsp;your account because *UPDATE_USER_REASSON*</p><p>Feel free to get in touch with us at <a href="mailto:admin@iolcalc.com" data-mce-href="mailto:admin@iolcalc.com">admin@iolcalc.com</a>.</p><p>Thank you,<br>John, Aazim, Uday, &amp; Bert</p>', 'Account Deleted', 'IOLcalc.com', 'DROP_USER', 'admin@iolcalc.com'),
(4, '<p>Dear Dr. *USER_NAME*,</p><p>We have upgraded your account to ''administrator'' level.&nbsp;You now have admin privilleges.&nbsp;</p><p>Feel free to get in touch with us at <a href="mailto:admin@iolcalc.com" target="_blank" data-mce-href="mailto:admin@iolcalc.com">admin@iolcalc.com</a>.</p><p>Thank you,<br>John, Aazim, Uday, &amp; Bert</p>', 'Account Upgraded', 'IOLcalc.com', 'USER_TO_ADMIN', 'admin@iolcalc.com'),
(5, '<p>Dear Dr. *USER_NAME*,</p><p>Your account has been downgraded to ''user'' level. You no longer have admin privilleges.&nbsp;</p><p>Feel free to get in touch with us at <a href="mailto:admin@iolcalc.com" target="_blank" data-mce-href="mailto:admin@iolcalc.com">admin@iolcalc.com</a>.</p><p>Thank you,<br>John, Aazim, Uday, &amp; Bert</p>', 'Account Downgraded', 'IOLcalc.com', 'ADMIN_TO_USER', 'admin@iolcalc.com'),
(6, '<p>Dear Dr. *USER_NAME*,</p><p>We have had to ban your account because *UPDATE_USER_REASSON*</p><p>Feel free to get in touch with us at <a href="mailto:admin@iolcalc.com" data-mce-href="mailto:admin@iolcalc.com">admin@iolcalc.com</a>.</p><p>Thank you,<br>John, Aazim, Uday, &amp; Bert</p>', 'Account Banned', 'IOLcalc.com', 'USER_RESTRICTIONS', 'admin@iolcalc.com'),
(7, '<p><br></p>', 'User Access', 'IOLcalc.com', 'USER_ACCESS', 'admin@iolcalc.com'),
(8, '<p>Dear Dr. *USER_NAME*,</p><p>Thank you for your interest in <a href="http://www.iolcalc.com" target="_blank" data-mce-href="http://www.iolcalc.com">IOLcalc.com</a>. We will be reviewing your request to join and activating your account shortly.&nbsp;</p><p>We appreciate your interest and patience.&nbsp;</p><p>Thank you,<br>John, Aazim, Uday, &amp; Bert</p>', 'Thank you for requesting access to IOLcalc.com', 'IOLcalc.com', 'USER_REQUEST', 'admin@iolcalc.com'),
(9, '<p>Dear Dr. *USER_NAME*,<br></p><p>We have received your request to reset your password. Please follow the below instructions. <em>(If you have not requested to reset your password, please contact us and do NOT follow the below instructions)</em>.&nbsp;</p><ol><li>In order to reset your password, please <a href="*LINK_PSW_RESET*" data-mce-href="*LINK_PSW_RESET*">click here</a>. This will reset your password to the new, temporary password: <strong>*NEW_PSW*</strong></li><li>Please use this&nbsp;temporary password to log in. We recommend that you change your password in the SETTINGS menu once you have logged in.</li></ol><p>Thank you,<br>John, Aazim, Uday, &amp; Bert</p>', 'Password Reset', 'IOLcalc.com', 'RESET_PSW', 'admin@iolcalc.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
