-- phpMyAdmin SQL Dump
-- version 4.0.10.12
-- http://www.phpmyadmin.net
--
-- Host: sitc.mysql.ukraine.com.ua
-- Generation Time: May 10, 2016 at 10:06 AM
-- Server version: 5.6.27-75.0-log
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sitc_shared`
--

-- --------------------------------------------------------

--
-- Table structure for table `mc_notes`
--

CREATE TABLE IF NOT EXISTS `mc_notes` (
  `id_note` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(50) NOT NULL,
  `note_status` varchar(20) NOT NULL DEFAULT 'need_confirm',
  `id_code` int(10) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id_note`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `mc_notes`
--

INSERT INTO `mc_notes` (`id_note`, `user_email`, `note_status`, `id_code`, `user_name`) VALUES
(6, 'aazim.siddiqui@gmail.com', 'has_confirmed', 13, 'Aazim Siddiqui'),
(7, 'aazim.siddiqui11@imperial.ac.uk', 'has_confirmed', 13, 'A S'),
(12, 'martianotic@yahoo.com', 'has_confirmed', 13, 'AS'),
(13, 'murilocba@hotmail.com', 'has_confirmed', 18, 'Murilo'),
(15, 'mcampos@terra.com.br', 'has_confirmed', 18, 'Murilo'),
(17, 'reworld2009@gmail.com', 'has_confirmed', 13, 'Ruslan Dzhashymov'),
(21, 'test@bk.ru', 'has_declined', -1, 'unilimes'),
(22, 'Lmurphy@stlukeseye.com', 'has_confirmed', 18, 'Leah Murphy'),
(23, 'Leahstallard@yahoo.com', 'has_confirmed', 18, 'Leah Murphy'),
(24, 'omar.shakir@gmail.com', 'has_confirmed', 18, 'omar shakir'),
(25, 'ccorveddu@stlukeseye.com', 'has_confirmed', 18, 'Christine Corveddu'),
(27, 'pasku@bk.ru', 'has_confirmed', 16, 'Saa'),
(28, 'mjm@mountaineyeassociates.com', 'has_confirmed', 18, 'Miranda'),
(29, 'reworld22009@gmail.com', 'has_confirmed', 18, 'awd'),
(30, 'meteorilp@gmail.com', 'has_confirmed', 18, 'Piu'),
(31, 'joshtpowell@gmail.com', 'has_confirmed', 18, 'Joshua Powell'),
(32, 'Brandon.Posvar@BSWHealth.org', 'has_confirmed', 18, 'Brandon Posvar'),
(33, 'dfg@dgf.gd', 'has_declined', -1, 'vx'),
(34, 'fd@fd.fg', 'has_declined', -1, 'df'),
(38, 'pas@fd.ti', 'need_confirm', 0, 'Alex'),
(39, 'thomas@stuckey.net', 'need_confirm', 0, 'Thomas Stuckey, MD');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
