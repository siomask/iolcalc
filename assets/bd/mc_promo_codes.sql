-- phpMyAdmin SQL Dump
-- version 4.0.10.12
-- http://www.phpmyadmin.net
--
-- Host: sitc.mysql.ukraine.com.ua
-- Generation Time: May 10, 2016 at 10:06 AM
-- Server version: 5.6.27-75.0-log
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sitc_shared`
--

-- --------------------------------------------------------

--
-- Table structure for table `mc_promo_codes`
--

CREATE TABLE IF NOT EXISTS `mc_promo_codes` (
  `id_code` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `mc_promo_codes`
--

INSERT INTO `mc_promo_codes` (`id_code`, `name`) VALUES
(13, 'TheBig4'),
(16, 'BETATEST234'),
(17, 'USRSGRP234'),
(18, 'XYA46373');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
