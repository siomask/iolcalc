-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2016 at 02:40 PM
-- Server version: 5.5.25
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mcalc`
--

-- --------------------------------------------------------

--
-- Table structure for table `mc_statistic`
--

CREATE TABLE IF NOT EXISTS `mc_statistic` (
  `id_statistic` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `st_visites` varchar(250) DEFAULT NULL,
  `st_printes` varchar(50) DEFAULT NULL,
  `st_logines` varchar(50) DEFAULT NULL,
  `st_calcs` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_statistic`),
  UNIQUE KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mc_statistic`
--

INSERT INTO `mc_statistic` (`id_statistic`, `id_user`, `st_visites`, `st_printes`, `st_logines`, `st_calcs`) VALUES
(2, 42, '25', '2', '5', NULL),
(4, 107, '18', NULL, '1', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
