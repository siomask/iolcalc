var app = angular.module('Main', ['ngMaterial', "ui.router", "ui.tinymce","ngTable"]);

app.run(function ($rootScope) {
    $rootScope.grecaptcha = false;
    String.prototype.replaceAll = function(search,  replacement) {
        var target = this;
        for(var i = 0; i < search.length; i++){
            target = target.replace(new RegExp(search[i], 'g'), replacement);
        }
        return target;
    };
});

app.directive("passwordVerify", function () {
    return {
        require: "ngModel",
        scope: {
            passwordVerify: '='
        },
        link: function (scope, element, attrs, ctrl) {
            scope.$watch(function () {
                var combined;

                if (scope.passwordVerify || ctrl.$viewValue) {
                    combined = scope.passwordVerify + '_' + ctrl.$viewValue;
                }
                return combined;
            }, function (value) {
                if (value) {
                    ctrl.$parsers.unshift(function (viewValue) {
                        var origin = scope.passwordVerify;
                        if (origin !== viewValue) {
                            ctrl.$setValidity("passwordVerify", false);
                            return undefined;
                        } else {
                            ctrl.$setValidity("passwordVerify", true);
                            return viewValue;
                        }
                    });
                }
            });
        }
    };
});
app.directive('versionInfo', function ($mdDialog, $timeout) {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        compile: function compile(temaplateElement, templateAttrs) {

            temaplateElement.prepend(VERSION_APP);

            return {
                pre: function (scope, element, attrs) {

                },
                post: function (scope, element, attrs) {

                }
            }
        }
    }
});
app.directive('phoneInput', function ($filter, $browser) {
    return {
        require: 'ngModel',
        link: function ($scope, $element, $attrs, ngModelCtrl) {
            var listener = function () {
                var value = $element.val().replace(/[^0-9]/g, '');

                $element.val($filter('tel')(value, false));
            };
            ngModelCtrl.$validators.tel = function (modelValue) {
                if (modelValue)return !(modelValue.length < 10);
            }

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function (viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0, 10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function () {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function (event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) {
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function () {
                $browser.defer(listener);
            });
        }

    };
});
app.filter('tel', function ($rootScope) {
    return function (tel) {
        //console.log(tel);
        if (!tel) {
            return '';
        }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;

            default:
                city = value.slice(0, 3);
                number = value.slice(3);
        }

        if (number) {
            if (number.length > 3) {
                number = number.slice(0, 3) + '-' + number.slice(3, 7);
            }
            else {
                number = number;
            }

            return ("(" + city + ")-" + number).trim();
        }
        else {
            return "(" + city;
        }

    };
});
app.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            var valLen = ngModelCtrl.$modelValue, dirty;
            //console.log(valLen,ngModelCtrl);

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    val = '';
                }

                val = (val) + '';
                valLen = ngModelCtrl.$viewValue.length > 0,
                    dirty = valLen;
                if (val.length == 1 && val.charAt(0) == "-") {
                    val = "-" + val
                } else {

                }

                var clean = val.replace(/[^-0-9\.]/g, '');

                var negativeCheck = clean.split('-');

                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = '-' + negativeCheck[0] + negativeCheck[1];
                    var s = clean.split("");
                    if (s.reduce(function (a, e, i) {
                            if (e == '-') a.push(i);
                            return a;
                        }, []).length > 1) {
                        s.pop();
                        clean = s.join("");
                    }

                }

                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 10);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean || 1) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return +clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
            element.bind('focusout', function (event) {
                var _dat = scope.data,
                    s = _dat.inputOD && _dat.inputOD.calcsFormSubmitted && attrs.required ,
                    l = _dat.inputOS && _dat.inputOS.calcsFormSubmitted  && attrs.required ,
                    d = _dat._outputs && _dat._outputs.calcsFormSubmitted  && attrs.required ;
                valLen = "" +ngModelCtrl.$viewValue;
                var valF = parseFloat(valLen),
                    min = parseFloat(attrs.min),
                    max = parseFloat(attrs.max);
                dirty = valLen && (valF > max || valF < min) ;

                //if (l || s || d || dirty) {
                setTimeout(function () {

                    ngModelCtrl.$invalid =  dirty ? dirty : ((s || l || d) && (isNaN(valLen) || valLen ==""));
                    ngModelCtrl.$setValidity("range", !ngModelCtrl.$invalid);
                    ngModelCtrl.$setDirty(  !ngModelCtrl.$invalid);
                    ngModelCtrl.$render();

                    if (ngModelCtrl.$invalid) {
                        jQuery(element).addClass('input-err');
                    } else {
                        jQuery(element).removeClass('input-err');
                    }
                }, 100);
                //}
            });
        }
    };
});
app.directive('noSpaces', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue.replace(/ /g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});
app.directive('xmlLoad', function ($mdDialog, $timeout, request, NgTableParams) {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        compile: function compile(temaplateElement, templateAttrs) {
            var fileInpName = 'fileInput',
                tag = '<md-icon md-svg-src="assets/img/ic_attach_file_black_24px.svg" style="transform: rotate(90deg); ">' +
                    '</md-icon><span>Import Patient</span>  <md-tooltip md-direction="Bottom"> Load a Lenstar or IOL Master \'.xml\' patient file to import measurement data. </md-tooltip> ' +
                    '<input class="ng-hide" id="' + fileInpName + '" multiple type="file" accept="text/xml, .mdb,.MDB,.zip"/>';

            temaplateElement.prepend(tag);

            return {
                pre: function (scope, element, attrs) {

                },
                post: function (scope, element, attrs) {
                    var data = scope.$parent.data,
                        callbackOnEyeSelect,
                        preload = scope.$parent.$parent.utils.preload,
                        round = 2,
                        listColumn = [
                            {o: ["patient_id", "Id", "ID"], d: "Patient ID"},
                            {o: ["family_name", "last_name", "LastName"], d: "Last Name"},
                            {o: ["given_name", "first_name", "FirstName"], d: "First Name"},
                            {o: ["birth_date", "patients_birth_date", "DateOfBirth","Birthday"], d: "DOB"},
                            {o: ["exam_date", "measurement_date", "AcquisitionTime","Time"], d: "Exam Date"}
                        ];

                    function setCurrentDataInput(eye, examD) {
                        var exam = examD || data.exam;

                        if (!eye || !exam)return;
                        var isRight = (eye == "Od" || eye == "OD"),
                            input, kindexes, _fd, curEye, _inpF,
                            _dat = data,
                            fields = _dat._fields;

                        switch (_dat.name) {
                            case "HomeCntrl":
                            {
                                _inpF = data._inpF;
                                input = _dat[(isRight ? _inpF.r : _inpF.l)];
                                kindexes = _dat.Kindexes;
                                break;
                            }
                            case "ToricCalcCtrl":
                            {
                                input = _dat._outputs;
                                kindexes = _dat._inputes[1]["s"];
                                break;
                            }
                            case "PostCalcCntrl":
                            {
                                input = _dat._outputs;
                                kindexes = _dat._outputs['optDevice_Indexs'];
                                break;
                            }
                            default :
                            {
                                return console.log("Inputes did not selected");
                            }
                        }

                        var softwareVersion = exam['SoftwareVersion'];
                        //$timeout(function () {
                        //    scope.$apply(function () {
                        if (softwareVersion) {
                            _fd = _dat.file['old'];
                            curEye = exam[(eye.toLowerCase().match("od") ? "Od" : "Os")];

                        } else if (exam && exam.length && eye) {
                            _fd = _dat.file['_new'];
                            curEye = exam[(eye.toUpperCase().match("OD") ? 0 : 1)];
                        }


                        if (curEye) {
                            for (var i = 0; i < _fd.length; i++) {
                                var _el = _fd[i],
                                    _a = curEye[_el.n];

                                for (var di = 0; di < _el.f.length; di++) {
                                    var __elem = _el.f[di],
                                        val;

                                    if('K1Axis' ==__elem.xml)continue;
                                    if (_a) {
                                        val = _a[__elem.xml];
                                        val = val["Value"] ? val["Value"] : val;
                                    } else {
                                        val = curEye[__elem.xml];
                                    }

                                    var selectedIndex = parseFloat(val) + "";
                                    if (val && !isNaN(selectedIndex)) {
                                        if (__elem.or == fields[1] || (_dat.name == "PostCalcCntrl" && __elem.or == fields[26])) {
                                            if (kindexes.indexOf(selectedIndex) < 0)kindexes.push(selectedIndex);
                                            input[__elem.or] = selectedIndex;
                                        } else {
                                            input[__elem.or] = parseFloat(val).toFixed(round);
                                        }
                                    }
                                }
                            }

                            if (_dat.name == "PostCalcCntrl") {

                            } else {
                                input[fields[11]] = (input[fields[18]] && input[fields[19]] ? (parseFloat(input[fields[19]]) + (parseFloat(input[fields[18]]) / 1000)) + "" : input[fields[11]]);
                            }

                        }
                        //})
                        //}, 0);
                    }

                    function showPatients() {
                        $mdDialog.show({
                            clickOutsideToClose: true,
                            scope: scope,        // use parent scope in template
                            preserveScope: true,  // do not forget this if use parent scope
                            template: '<md-dialog class="mdb-pop-up"aria-label="{{fileName}}">' +
                            '  <md-dialog-content style="padding:10px ">' +


                            '<h1 style="    margin: 10px 0;" class="col-sm-6" >Patient Import </h1>' +
                            '   <md-toolbar class="md-table-toolbar md-default col-sm-6" style="background-color:#fff">'+
                            '       <div class="md-toolbar-tools" style="padding-top: 25px;">'+
                            '       <md-input-container  class="md-icon-float md-icon-right md-block col-md-12"  >'+
                            '       <md-icon md-svg-src="assets/img/ic_search_black_24px.svg"></md-icon>'+
                            '       <input    type="text" placeholder="Search patient" ng-change="searchPtnt()" ng-model="searchMdb">'+
                            '       </md-input-container>'+
                            '       </div>'+
                            '       </md-toolbar>'+
                            '<div ng-if="showListTables">' +
                            '<table class="table mdb-table">' +
                            '<thead>' +
                            '<tr>' +
                            '<td>№</td>' +
                            '<td> Name</td>' +
                            '</tr></thead>' +
                            '<tbody>' +
                            '<tr data-ng-repeat="table in mdbData.tables">' +
                            '<td>{{$index+1}}</td>' +

                            '<td><a href="#" data-ng-click="open(table.name)" >{{table.name}}</a></td>' +
                            '</tr></tbody></table></div>' +

                            '<div ng-if="!showListTables">' +
                            '<table ng-table="mdbData.tableParams" show-filter="true" class="table mdb-table">' +
                            '<thead>' +
                            '<tr>' +
                            '<td    >No.</td>' +
                            '<td data-ng-repeat="field in mdbData.keysT" class="sortorder" ng-class="{\'reverse\': reverse[field.d]}" data-ng-click="sortBy(field)">{{field.d}}</td>' +
                            '</tr></thead>' +
                            '<tbody>' +


                            '<tr data-ng-repeat="table in $data ">' +
                            '<td   data-ng-click="setValues(table)"  ><a href="#"> {{$index+1}}</a></td>' +
                            '<td data-ng-repeat="field in mdbData.keysT"   ><span data-ng-repeat="valueF in field.o" ng-if="table[valueF]">{{table[valueF]}}</span></td>' +
                            '</tr>' +
                            '</tbody>' +
                            '</table></div>' +


                            '  </md-dialog-content>' +
                            '  <md-dialog-actions>' +
                            '  <md-button  ng-click="loadFile()" class="md-primary">' +
                            '      New File' +
                            '    </md-button>' +
                            '    <md-button ng-click="closeDialog()" class="md-primary">' +
                            '      Close' +
                            '    </md-button>' +
                            '  </md-dialog-actions>' +
                            '</md-dialog>',
                            controller: function DialogController($scope, $mdDialog, $timeout) {
                                $scope.closeDialog = function () {
                                    $mdDialog.hide();
                                    //$state.go('home');
                                }
                                $scope.setValues = function (tableD) {
                                    if (tableD.selected) {
                                        tableD.selected();
                                    } else {
                                        var _f = scope.mdbData.fields,
                                            _fieldsC = [
                                                {
                                                    eye: 'inputOD', data: [
                                                    {or: 'Axial Length', c: _f[0]},
                                                    {or: 'Optical ACD', c: _f[2]},
                                                    {or: 'K1', c: _f[4]},
                                                    {or: 'K2', c: _f[6]}
                                                ]
                                                },
                                                {
                                                    eye: 'inputOS', data: [
                                                    {or: 'Axial Length', c: _f[1]},
                                                    {or: 'Optical ACD', c: _f[3]},
                                                    {or: 'K1', c: _f[5]},
                                                    {or: 'K2', c: _f[7]}
                                                ]
                                                }
                                            ];
                                        _fieldsC.forEach(function (nam) {

                                            nam.data.forEach(function (val) {
                                                var value = (tableD[val.c]);
                                                if (value && value != Infinity && !isNaN(value))data[nam.eye][val.or] = value;
                                            });
                                        });
                                    }

                                    listColumn[0].o.forEach(function(fieldName){
                                        if(tableD[fieldName])data.ptntID = tableD[fieldName];
                                    })
                                    listColumn[1].o.forEach(function(fieldName,item){
                                        if(tableD[fieldName])data.ptntName = tableD[fieldName] +" "+tableD[ listColumn[2].o[item]];
                                    })
                                    $mdDialog.hide();
                                }
                                //$scope.showListTables=true;



                                var prev;
                                $scope.reverse={};
                                $scope.loadFile=function(){
                                    $scope.closeDialog();
                                    openDialog();
                                }
                                $scope.sortBy=function(field){
                                    var up = this.reverse[field.d] = !this.reverse[field.d];
                                    if(prev != field.d)this.reverse[prev] = false;
                                    prev = field.d;

                                    var fieldsName=field.o;
                                    scope.mdbData.tables.sort(function(a,b){
                                        for(var i=0;i<fieldsName.length;i++){
                                            if(a[fieldsName[i]])return a[fieldsName[i]]<b[fieldsName[i]]?(up?-1:1):(up?1:-1);
                                        }
                                        return 0
                                    });
                                    scope.mdbData.tableParams = new NgTableParams({}, {dataset:  scope.mdbData.tables});
                                }
                                $scope.searchPtnt = function(){
                                    var search =  $scope.searchMdb,
                                        matches = [],
                                        searchIn = scope.mdbData.tables;
                                    if(search =='' || !search){
                                        matches = searchIn;
                                    }else{
                                        var keys =   scope.mdbData.keysT,
                                            keysSize = keys.length;
                                        for(var i= 0 ; i < searchIn.length;i++){
                                            var curSearch = searchIn[i],
                                                isNotMatch = true;
                                            for(var j=0;j< keysSize; j++){
                                                for(var itemK=0;itemK< keys[j].o.length; itemK++){
                                                    var keyVal = curSearch[keys[j].o[itemK]];
                                                    if(keyVal && keyVal.match(search)){
                                                        matches.push(curSearch);
                                                        isNotMatch= false;
                                                        break;
                                                    }
                                                }
                                                if(!isNotMatch)break;
                                            }
                                            if(isNotMatch  && (i+'').match(search)){
                                                matches.push(curSearch);
                                            }
                                        }
                                    }

                                    scope.mdbData.tableParams = new NgTableParams({}, {dataset:  matches});
                                }
                                $scope.sortBy(listColumn[4]);
                                $scope.back = function (name) {
                                    $scope.showListTables = true;
                                }
                                $scope.open = function (name) {

                                    preload(true);
                                    request.mdb(null, 'getInfoFromTable&table=' + name + "&filename=" + $scope.mdbData.fileName, function (res) {

                                        if (res.error || res.tables.length == 0) {
                                            $mdDialog.show($mdDialog.alert({
                                                title: 'Error',
                                                textContent: res.error || "Nothing load from " + name,
                                                ok: 'Close'
                                            }))
                                        } else {
                                            setTimeout(function () {
                                                $scope.$apply(function () {
                                                    var _list = res.tables.sort(function (a, b) {
                                                        return parseInt(b.exam_date) - parseInt(a.exam_date);
                                                    });

                                                    $scope.showListTables = res.tables.length == 0;
                                                    $scope.curNT = $scope.showListTables ? null : name;
                                                    $scope.currentT = _list.splice(0, 10);
                                                    $scope.keysT = Object.keys($scope.currentT[0]);


                                                })
                                            }, 100);
                                        }

                                    }, function () {
                                    })

                                }
                                $timeout(function () {
                                    angular.element('md-backdrop.md-opaque.md-default-theme, md-backdrop.md-opaque').addClass('white-back');
                                }, 0);

                            },
                            onComplete: function () {

                            }
                        });
                    }


                    function openDialog() {
                        angular.element('#' + fileInpName).click();
                    }

                    element.bind('mouseup', function (ev) {
                        if (scope.mdbData) {

                            //if (confirm("You already has been uploading the mdb, maybe you want get info from it")) {
                            showPatients();
                            //} else {
                            //    openDialog();
                            //}
                        } else {
                            openDialog();
                        }

                    });
                    angular.element('#' + fileInpName).bind('change', function (evt) {
                            var fileList = evt.target.files;


                            if (!fileList.length) {
                                return
                            } else {
                                var onError = function (text, callback) {
                                        $mdDialog.show($mdDialog.alert({
                                            title: 'Error',
                                            textContent: text,
                                            ok: 'Close'
                                        })).finally(function () {
                                            if (callback instanceof Function)callback();
                                        })
                                    },
                                    outFiles = {
                                        xml: [],
                                        mdb: [],
                                        zip: []
                                    },
                                    hasFiles = false;
                                for (var i = 0; i < fileList.length; i++) {
                                    var value = fileList[i].name,
                                        fType = value.substr(value.lastIndexOf('.') + 1).toLowerCase();
                                    if (outFiles[fType]) {
                                        outFiles[fType].push(fileList[i]);
                                        hasFiles = true;
                                    }
                                }
                                if (!hasFiles) {
                                    onError("Please select only xml, mdb or zip file");
                                } else {
                                    if (outFiles.xml.length) {
                                        var r = new FileReader(),
                                            patientList = [],
                                            change = listColumn[3].o.concat(listColumn[4].o);

                                        r.onload = function (e) {
                                            var contents = e.target.result;
                                            var json = $.xml2json(contents),
                                                examData = json['ExamData'],
                                                patient = examData ? examData['Patient'] : json['Patient'];

                                            if (examData)  {
                                                patient['AcquisitionTime'] = examData['Exam']['AcquisitionTime'];
                                            } else if(patient['Exam']&& patient['Exam'].length){
                                                patient['Time'] = patient['Exam'][0]['Time']
                                            }

                                            if (examData || patient) {
                                                var exam = examData ? examData['Exam'] : patient['Exam'],
                                                    eye = examData ? ["Od", "Os"] : ["OD", "OS"];

                                                if (exam) {
                                                    if (!(exam instanceof Array) && !examData) {
                                                        exam = [exam];
                                                    }
                                                    patient.selected = (function (e, a) {
                                                        return function () {
                                                            angular.forEach(a, function (obj) {
                                                                setCurrentDataInput(obj, e);
                                                            });
                                                        }
                                                    })(exam, eye);
                                                    patientList.push(patient);
                                                    patientList.forEach(function (a) {
                                                        change.forEach(function (elem) {
                                                            if (a[elem]) {
                                                                var l = a[elem].split(" ")[0].split("T")[0].split("-");
                                                                if(l[1]){
                                                                    l.push(l.shift());
                                                                    a[elem] = l.join("/");
                                                                }else{
                                                                    a[elem] = l[0];
                                                                }

                                                            }
                                                        });
                                                    })
                                                } else {
                                                    onError("Failed load Exam, plese make sure you have correct xml structure");
                                                }

                                            } else {
                                                onError("Failed load examData or patient, plese make sure you have correct xml structure");
                                            }

                                            readXMLFiles();

                                        }
                                        var lastFileIter = 0,
                                            readXMLFiles = function () {
                                                if (lastFileIter < outFiles.xml.length) {
                                                    r.readAsText(outFiles.xml[lastFileIter++]);
                                                } else {
                                                    var defSort = listColumn[4].o.concat([]);
                                                    defSort.forEach(function(fieldName){

                                                    })
                                                    scope.mdbData = {tables: patientList, keysT: listColumn};
                                                    scope.mdbData.tableParams = new NgTableParams({}, {dataset: patientList});
                                                    showPatients();
                                                }

                                            };
                                        readXMLFiles();
                                    } else if (outFiles.mdb.length || outFiles.zip.length) {
                                        console.warn("Only first file will check");

                                        var f = fileList[0],
                                            formData = new FormData();
                                        formData.append('upload', f);

                                        preload(true);
                                        function onSuccess(res) {
                                            if (res.error) {
                                                onError(res);
                                            } else {
                                                var

                                                    _tables = res.tables,
                                                    key = res.id,
                                                    _fileds = res.fields,
                                                    change = ['birth_date', 'exam_date', 'measurement_date', 'patients_birth_date'],
                                                    mainTable = _tables.shift(),
                                                    countTabl = _tables.length
                                                    ;

                                                for (var i = 0, max_i = mainTable.length; i < max_i; i++) {

                                                    var curK = mainTable[i][key];
                                                    for (var ki = 0; ki < countTabl; ki++) {
                                                        var nearT = _tables[ki];
                                                        for (var di = 0, max_di = nearT.length; di < max_di; di++) {
                                                            var curDi = nearT[di];
                                                            if (!key) {
                                                                for (var field in curDi) {
                                                                    mainTable[i][field] = curDi[field];
                                                                }
                                                                nearT.splice(di, 1);
                                                                break;
                                                            } else if (curDi[key] == curK) {
                                                                for (var field in curDi) {
                                                                    if (field != key) mainTable[i][field] = curDi[field];
                                                                }
                                                                nearT.splice(di, 1);
                                                                break;
                                                            }
                                                        }
                                                    }

                                                }


                                                var sortKey = mainTable[0][key] ? key : 'measurement_date';
                                                mainTable.sort(function (b, a) {
                                                    return a[sortKey] - b[sortKey];
                                                });

                                                mainTable.forEach(function (a) {
                                                    delete a[key];
                                                    change.forEach(function (elem) {
                                                        if (a[elem]) {
                                                            a[elem] = a[elem].split(" ")[0];
                                                            a[elem] = a[elem].substr(-2) + "/" + a[elem].substring(4, 6) + "/" + a[elem].substring(0, 4);
                                                        }

                                                    });

                                                    _fileds.forEach(function (el, key) {
                                                        var c = parseFloat(a[el]);
                                                        if (isNaN(c))return;
                                                        if (key > 3)c = 337.5 / c;
                                                        a[el] = (((c )).toFixed(round));
                                                    });

                                                });


                                                scope.mdbData = {tables: mainTable, keysT: listColumn, fields: _fileds};
                                                scope.mdbData.tableParams = new NgTableParams({}, {dataset: mainTable});
                                                scope.fileName = f.name;
                                                showPatients();
                                                preload();
                                            }


                                        }

                                        function onError(res) {
                                            preload();
                                            $mdDialog.show($mdDialog.alert({
                                                title: 'Error',
                                                textContent: res ? res.error : "Something wrong with file",
                                                ok: 'Close'
                                            }))
                                        }

                                        var tableName = 'PatientData';
                                        request.mdb(formData, 'filesMdb', function (res) {
                                            if (res.error) {
                                                onError(res);
                                            } else if (res.repeat) {
                                                request.mdb({}, 'getInfoFromTable&isRepeat=true&table=&filename=' + res.filename, function (resp) {
                                                    if (res.error) {
                                                        onError(resp);
                                                    } else {
                                                        onSuccess(resp);
                                                    }
                                                }, function () {
                                                    request.mdb({}, 'getInfoFromTable&isRepeat=true&table=' + tableName + '&filename=' + res.filename, function (resp) {
                                                        if (res.error) {
                                                            onError(resp);
                                                        } else {
                                                            onSuccess(resp);
                                                        }
                                                    }, onError);
                                                });
                                            } else {
                                                onSuccess(res);
                                            }


                                        }, function () {
                                            request.mdb(formData, 'filesMdb&table=' + tableName, function (resp) {
                                                if (resp.error) {
                                                    onError(resp);
                                                } else {
                                                    onSuccess(resp);
                                                }
                                            }, onError);
                                        });
                                    }

                                }

                            }

                            //this.value = "";
                        }
                    );

                    return;
                    switch (scope.data.name) {
                        case "PostCalcCntrl":
                        {
                            scope.$watch(function (scope) {
                                    return scope.data._outputs.Eyes
                                },
                                function (newValue, oldValue) {
                                    if (callbackOnEyeSelect instanceof Function)callbackOnEyeSelect();
                                }
                            );

                            break;
                        }
                        case "ToricCalcCtrl":
                        {
                            //angular.element('.tor-eye-btns').bind('mouseup', function () {
                            //    if (callbackOnEyeSelect instanceof Function)callbackOnEyeSelect();
                            //});
                            break;
                        }
                    }

                }
            }
        }
    }
});

app.directive('threeJsOut', function ($timeout) {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        compile: function compile(temaplateElement, templateAttrs) {

            return {
                pre: function (scope, element, attrs) {

                },
                post: function (scope, element, attrs) {
                    var webglEl  = new Threejs(element);
                    var _d = scope.data;
                    if(listofWebgl.length > 1)listofWebgl=[];
                    listofWebgl.push(webglEl);

                    switch(_d.name){
                        case "HomeCntrl":{
                            scope.$watch(function(scope) { return scope.data.webgl },
                                function(newValue, oldValue) {
                                    var _ID =  angular.element(element).parent()[0].id;
                                    if(newValue && newValue.name  == _ID) {
                                        if(newValue.act =='print'){
                                            var  imgOut = document.getElementById(_ID+"pdf"),
                                                table =document.getElementById(_ID+"tablepdf"),
                                                strMime = "image/png",
                                                wasDrawed = attrs['wasDrawed'] == 'yeas';
                                            angular.element(table).css('display', wasDrawed?'block':'none');
                                            if (wasDrawed ) {
                                                webglEl.timer.startTimer();
                                                webglEl.render.domElement.getContext("experimental-webgl", {preserveDrawingBuffer: true});
                                                imgOut.src = ( webglEl.render.domElement.toDataURL(strMime));
                                            }
                                        }else{
                                            var isDrawing = newValue.act =='draw' && newValue.arg;
                                            if(isDrawing){
                                                webglEl.build.buildEyeState(newValue.arg);
                                            }else{
                                                webglEl.build.clear();
                                            }

                                            attrs.$set('wasDrawed',isDrawing?"yeas":"no");

                                            _d.isInputing =  !_d.isInputing? _d.isInputing:!isDrawing;
                                            _d.summary =  _d.summary? _d.summary:isDrawing;
                                            angular.element("#"+_ID+'NOT').css('display',(!isDrawing?"block":"none"));
                                            scope.data.webgl = null;
                                        }

                                    }
                                }
                            );
                            break;
                        }
                        case "ToricCalcCtrl":{
                            $timeout(function(){
                                webglEl.build.buildEyeTorState();
                                var  _fields = _d._fields,
                                    _f = [_fields[7], _fields[9], _fields[14]];
                                _f.forEach(function(o){
                                    scope.$watch(function(scope) { return _d._outputs[o] }, function(newValue, oldValue) {
                                        webglEl.build.rotateArrows(o, newValue);
                                    });
                                });
                                scope.$watch(function(scope) { return _d.eyeActive},
                                    function(newValue, oldValue) {
                                        webglEl.build.updateYey(newValue);
                                    }
                                );
                            },10);

                            break;
                        }
                    }
                }
            }
        }
    }
});


app.controller('AppCtrl', ['$scope', 'request', '$location', '$state', "$mdDialog","$timeout", function ($scope, request, $location, $state, $mdDialog,$timeout) {
    var preloader = "preloader";
    document.title = "IOLcalc - Ladas Super Formula";
    $scope.data = {
        userAccept: false,
        userData: {},
        listOfGraph: (listOfGraph ? listOfGraph : ['THREEJSOD', 'THREEJSOS', 'THREEJSTOR']),
        html: {
            title: "Ladas Super Formula"
        },
        iolRef: []
        , input: {
            Rfrctn: 0,
            K1: 44.25,
            K2: 43.98,
            axialLength: 27.45,
            ACD: 3.37,
            AConstant: 119.2/*,
             IOL: */
        }, label: {
            axialLength: "Axial Length",
            ACD: "Measured ACD",
            A: "A-Constant",
            Rfrctn: "Target Refraction",
            IOL: "IOL POWER"
        }
    };
    $scope.session = {};
    $scope.isIE = detectIE();
    $scope.containerVis = true;

    $scope.utils = {
        isAdmin: function () {
            return $scope.session.user && $scope.session.user.usr_type == 'Admin'
        },
        isAuth: function () {
            return $scope.session.user;
        },
        logIn: function () {
            var user = $scope.data.userData;
            $scope.submitted = true;
            if (!user.lgn || !user.usr_psw) {
                return;
            }
            request.get(user, 'getUser').then(function (data) {
                if (data['error']) {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Error',
                        textContent: data['text'],
                        ok: 'Close'
                    })).finally(function () {
                    });
                } else {
                    $scope.session.user = data['data'];
                }

                $scope.submitted = false;
            });
        },
        close: function () {
            $scope.containerVis = false;
            $location.path('/');
        },
        logOut: function () {
            request.session('clearSession').then(function (response) {
                $scope.session = response['session'];
                $state.go('sign_in');
            });
        },
        preload: function (flag) {
            if (flag) {
                jQuery(preloader_animate).fadeIn();
                jQuery('body').css('overflow', 'hidden');
                new ProgreesLoad().start();
            } else {
                jQuery(preloader_animate).fadeOut(10);
                jQuery('body').css('overflow', 'auto');
            }
        },
        move: function (move) {
            $state.go(move);
        },
        changeName: function (user, type) {
            var user = $scope.session.user;
            if (user && user.usr_name) {
                if (type == "usr_name") {
                    var arr = user.usr_name.split(" ");

                    user.firstName = arr[0];
                    user.secondName = '';
                    for (var i = 1; i < arr.length; i++) {
                        user.secondName += arr[i];
                    }

                } else {
                    user.usr_name = user.firstName + " " + user.secondName;
                }
            }

        },
        getSession: function (callback, callbackes) {
            request.session('getSession').then(function (response) {
                $scope.session = response['session'] ? response['session'] : $scope.session;
                if ($scope.session.user && callback) {
                    if (callback instanceof Function)callback();
                } else if (callbackes instanceof Function) {
                    callbackes();
                } else {
                    $state.go('sign_in');
                }
            });
        },
        version: function () {
            $mdDialog.show({
                clickOutsideToClose: true,
                scope: $scope,        // use parent scope in template
                preserveScope: true,  // do not forget this if use parent scope
                template: '<md-dialog aria-label="LADAS SUPER FORMULA">' +
                '  <md-dialog-content style="padding:10px ">' +
                VERSION_APP +
                '  </md-dialog-content>' +
                '  <md-dialog-actions>' +
                '    <md-button ng-click="closeDialog()" class="md-primary">' +
                '      Close' +
                '    </md-button>' +
                '  </md-dialog-actions>' +
                '</md-dialog>',
                controller: function DialogController($scope, $mdDialog, $timeout) {
                    $scope.closeDialog = function () {
                        $mdDialog.hide();
                    }
                    $timeout(function () {
                    }, 1500);

                },
                onComplete: function () {
                }
            });
        },
        updateInputData: function (data, outputs, fil, klist, argSet) {
            var user = $scope.session.user, id_val, uploadData, flag;
            if (argSet) {
                id_val = argSet['i'],
                    uploadData = argSet['d'],
                    flag = argSet['f']
            }

            if (flag) {
                var defInput = uploadData,
                    _inp = data[outputs[0]];
                user.calc_input = user.calc_input ? user.calc_input : {};
                for (var field in defInput) {
                    if (field == 'id_user' || field == 'id_val') {
                        user.calc_input[field] = (defInput[field]);
                    } else {
                        user.calc_input[field] = (  defInput[field] || defInput[field] == 0) ? (defInput[field]) : defInput[field];
                    }
                }
                if (!user.calc_input['id_val'] && id_val) {
                    user.calc_input['id_val'] = id_val;
                    _inp['id_val'] = id_val;
                }
            } else {
                if (!$scope.session.user)return;
                var defInput = flag ? uploadData : user.calc_input,
                    _dat = data,
                    fields = [{or: "KIndex", cstm: fil['KI']}, {or: "AConstant", cstm: fil['AC']}, {or: "Rfrctn", cstm: fil['Rfrctn']}];
                if (defInput instanceof Array) {
                    defInput.session.user.calc_input = {};
                }
                for (var field in defInput) {
                    if (((field == 'id_user' || field == 'id_val') && !argSet) ||  (!defInput[field] && isNaN(parseFloat(defInput[field]))) )continue;
                    var value = defInput[field], curFil = field;

                    switch (field) {
                        case fields[0].or:
                        {
                            var el = value + "";
                            if (klist.indexOf(el) < 0)klist.push(el);
                            value = el;
                            curFil = fields[0].cstm;
                            break;
                        }
                        case fields[1].or:
                        {
                            curFil = (fields[1].cstm);
                            value = parseFloat(value);
                            break;
                        }
                        case fields[2].or:
                        {
                            curFil = (fields[2].cstm);
                            value = parseFloat(value);
                            break;
                        }
                    }

                    if (value || value === 0) {
                        outputs.forEach(function (o) {
                            _dat[o][curFil] = value;
                        });
                    }
                }
            }
        }
    }

    loadPrintCSS($scope.isIE);

}]);
app.controller("AuthCtrl", ["$scope", "$state", "request", "$mdDialog", "$location", "$stateParams", "$timeout",
    function ($scope, $state, request, $mdDialog, $location, $stateParams, $timeout) {
        var $parent = $scope.$parent;
        var ses = $parent.utils.isAuth();
        $scope.patientName = '';
        if (!ses) {
            $parent.utils.getSession(false, function () {
                if ($parent.utils.isAuth() ) {
                    $parent.utils.move('home');
                } else {
                    fadePreload(true);
                }
            });
        } else {
            $parent.utils.move('home');
        }

        $parent.containerVis = true;
        $scope.data = {
            req_ac: true,
            types: ['customer', 'executor']
        };
        $scope.validate = {
            dft: {
                help: 'help-block',
                feedback: ['glyphicon-ok', 'glyphicon-warning-sign', 'glyphicon-remove'],
                hasFeedback: ['has-success', 'has-warning', 'has-error']
            },
            lgn: {
                help: 'help',
                feedback: 'help',
                hasFeedback: ''
            },
            msg: {
                help: 'help',
                feedback: 'help',
                hasFeedback: ''
            },
            usr_email: {
                help: 'help',
                feedback: 'help',
                hasFeedback: ''
            },
            usr_code: {
                help: 'help',
                feedback: 'help',
                hasFeedback: ''
            },
            usr_name: {
                help: 'help',
                feedback: 'help',
                hasFeedback: ''
            },
            usr_psw: {
                help: 'help',
                feedback: 'help',
                hasFeedback: ''
            },
            usr_psw_rep: {
                help: 'help',
                feedback: 'help',
                hasFeedback: ''
            },
            usr_phone: {
                help: 'help',
                feedback: 'help',
                hasFeedback: ''
            },
            checkName: function (flag) {
                var usrName = flag ? $scope.userData.lgn : $scope.userData.usr_name,
                    type = flag ? 'lgn' : 'usr_name',
                    text = flag ? 'lgn' : 'name';
                if (usrName && usrName.length == 1) {
                    $scope.validate.setVal(type, {
                        help: this.dft.help,
                        text: 'your ' + text + ' is too short',
                        feedback: this.dft.feedback[1],
                        hasFeedback: this.dft.hasFeedback[1]
                    });
                } else if (usrName && usrName.length > 1 && (flag || /[A-z]/.test(usrName))) {
                    $scope.validate.setVal(type, {
                        help: 'help',
                        text: '',
                        feedback: this.dft.feedback[0],
                        hasFeedback: this.dft.hasFeedback[0]
                    });
                } else {
                    $scope.validate.setVal(type, {
                        help: this.dft.help,
                        text: 'your ' + text + ' is not correct',
                        feedback: this.dft.feedback[2],
                        hasFeedback: this.dft.hasFeedback[2]
                    });
                }
            },
            checkPsw: function (repPsw) {
                //console.log($scope.userData.usr_psw,$scope.userData.usr_psw_rep,$scope.userData);
                var isSame = $scope.userData.usr_psw != $scope.userData.usr_psw_rep;
                repPsw.psw_rep.$error.passwordVerify = isSame;
                repPsw.psw_rep.$touched = isSame;
                repPsw.psw_rep.$invalid = isSame;
                repPsw.$invalid = repPsw.$invalid ? repPsw.$invalid : isSame;
                /* var psw = $scope.userData.usr_psw,
                 type = 'usr_psw';

                 $scope.userData[type] = $scope.userData[type] ? $scope.userData[type].split(' ').join('') : $scope.userData[type];
                 if (!psw) {
                 $scope.validate.setVal(type, {
                 help: this.dft.help,
                 text: 'your password is empty',
                 feedback: this.dft.feedback[2],
                 hasFeedback: this.dft.hasFeedback[2]
                 });
                 } else if (!(/^\S+$/.test(psw))) {
                 $scope.validate.setVal(type, {
                 help: this.dft.help,
                 text: 'you had some spaces in your password',
                 feedback: this.dft.feedback[1],
                 hasFeedback: this.dft.hasFeedback[1]
                 });
                 } else if (psw && (flag || /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,}$/.test(psw))) {
                 $scope.validate.setVal(type, {
                 help: 'help',
                 text: '',
                 feedback: this.dft.feedback[0],
                 hasFeedback: this.dft.hasFeedback[0]
                 });
                 } else {
                 $scope.validate.setVal(type, {
                 help: this.dft.help,
                 text: 'your password is not strong enough',
                 feedback: this.dft.feedback[1],
                 hasFeedback: this.dft.hasFeedback[1]
                 });
                 }*/
            },
            checkRepPsw: function () {
                var psw = $scope.userData.usr_psw_rep,
                    psw1 = $scope.userData.usr_psw,
                    type = 'usr_psw_rep';
                $scope.userData.repeatError = !psw1 || psw != psw1;
                if (!psw1 || psw != psw1) {
                    $scope.validate.setVal(type, {
                        help: this.dft.help,
                        text: 'your password\'s do not match',
                        feedback: this.dft.feedback[2],
                        hasFeedback: this.dft.hasFeedback[2]
                    });
                } else {
                    $scope.validate.setVal(type, {
                        help: 'help',
                        text: '',
                        feedback: this.dft.feedback[0],
                        hasFeedback: this.dft.hasFeedback[0]
                    });
                }
            },
            checkEmail: function (usrname) {
                var type = usrname ? usrname : 'usr_email';
                $scope.userData[type] = $scope.userData[type] ? $scope.userData[type].split(' ').join('') : $scope.userData[type];

                if (/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test($scope.userData[type])) {
                    $scope.validate.setVal(type, {
                        help: 'help',
                        text: '',
                        feedback: this.dft.feedback[0],
                        hasFeedback: this.dft.hasFeedback[0]
                    });
                } else {
                    $scope.validate.setVal(type, {
                        help: this.dft.help,
                        text: 'your ' + (usrname ? 'login' : 'email') + ' is not correct',
                        feedback: this.dft.feedback[2],
                        hasFeedback: this.dft.hasFeedback[2]
                    });
                }

            },
            checkPhone: function () {
                var type = 'usr_phone';
                if (/^[0-9]\d{9}$/.test($scope.userData.usr_phone)) {
                    $scope.validate.setVal(type, {
                        help: 'help',
                        text: '',
                        feedback: this.dft.feedback[0],
                        hasFeedback: this.dft.hasFeedback[0]
                    });
                } else {
                    $scope.validate.setVal(type, {
                        help: this.dft.help,
                        text: 'your mobile  phone is not correct',
                        feedback: this.dft.feedback[2],
                        hasFeedback: this.dft.hasFeedback[2]
                    });
                }

            },
            checkPromo: function (name) {
                if (!name)name = 'usr_code';
                $scope.userData[name] = $scope.userData[name] ? $scope.userData[name].split(' ').join('') : $scope.userData[name];

                if (!$scope.userData[name]) {
                    $scope.validate.setVal(name, {
                        help: this.dft.help,
                        text: 'your have to enter invite code',
                        feedback: this.dft.feedback[2],
                        hasFeedback: this.dft.hasFeedback[2]
                    });
                } else {
                    $scope.validate.setVal(name, {
                        help: 'help',
                        text: null,
                        feedback: this.dft.feedback[0],
                        hasFeedback: this.dft.hasFeedback[0]
                    });
                }
            },
            checkMsg: function (name) {
                if (!name)name = 'msg';
                if (!$scope.userData[name]) {
                    $scope.validate.setVal(name, {
                        help: this.dft.help,
                        text: 'your have to enter some message',
                        feedback: this.dft.feedback[1],
                        hasFeedback: this.dft.hasFeedback[1]
                    });
                } else {
                    $scope.validate.setVal(name, {
                        help: 'help',
                        text: null,
                        feedback: this.dft.feedback[0],
                        hasFeedback: this.dft.hasFeedback[0]
                    });
                }
            },
            setVal: function (el, val) {
                el = $scope.validate[el];
                el.feedback = val.feedback;
                el.help = val.help;
                el.hasFeedback = val.hasFeedback;
                el.text = val.text;
            }
        };
        $scope.userData = {
            usr_email: null,
            usr_name: null,

            msg: 'I would like to get access to IOLcalc.com',
            //usr_phone: null,
            //usr_type: $scope.data.types[0],
            usr_psw: null
        };
        $scope.sign = {
            isAuth: false,
            agree: "nope",
            inn: function (form) {
                //console.log(form);
                $scope.myFormSsubmitted = true;
                $timeout(function () {
                    if (form.$invalid) {
                        form['userEmail'].$touched = true;
                        form['userEmail'].$dirty = true;
                        form['userEmail'].$setDirty(true);
                        form['psw'].$touched = true;
                        form['psw'].$dirty = true;
                        form['psw'].$setDirty(true);
                        return;
                    }
                    var user = $scope.userData,
                        vldt = $scope.validate,
                        v = vldt.dft,
                        $parent = $scope.$parent;
                    if (!user.lgn || !user.usr_psw) {
                        return;
                    }

                    request.get($scope.userData, 'getUser').then(function (data) {

                        if (data['error']) {
                            $mdDialog.show($mdDialog.alert({
                                title: 'Error',
                                textContent: data['text'],
                                ok: 'Close'
                            }));
                        } else {
                            $parent.session.user = data['data'];
                            if (data['data']['needToShowPoPUp'] ) {
                                $scope.sign.version();
                            } else {
                                $state.go('home');
                            }
                            angular.element('md-backdrop.md-opaque.md-default-theme, md-backdrop.md-opaque').css("background-color","rgba(33,33,33,1.0)!important");
                        }
                    });
                }, 100);
            },
            up: function (form) {
                $scope.myFormRsubmitted = true;
                if (this.agree == "nope") return;
                $timeout(function () {
                    if (form && form.$invalid) {
                        var arr = ['psw', 'secondName', 'userEmail', 'userMsg', 'userName'];
                        for (var i = 0; i < arr.length; i++) {
                            form[arr[i]].$touched = true;
                            form[arr[i]].$dirty = true;
                            form[arr[i]].$setDirty(true);
                        }
                    } else {
                        /*jQuery.ajax({
                         type:'post',//тип запроса: get,post либо head
                         url:'http://webgl.unilimes.com/captcha.php',//url адрес файла обработчика
                         data:{
                         secret:'6LfT4CATAAAAAAsfuhYwAng47mvcJ4fuY74CX8kK',
                         response:"",
                         remoteip:""
                         },//параметры запроса
                         headers: {
                         'Content-Type':'application/json',
                         "My-Second-Header":"Access-Control-Allow-Origin"
                         },
                         response:'text',//тип возвращаемого ответа text либо xml
                         success:function (data) {//возвращаемый результат от сервера
                         console.log(data);
                         //req();
                         },error:function(data){
                         console.log(data);
                         }
                         });*/
                        req();

                    }

                }, 100);
                function timezone() {
                    var offset = new Date().getTimezoneOffset();
                    var minutes = Math.abs(offset);
                    var hours = Math.floor(minutes / 60);
                    var prefix = offset < 0 ? "+" : "-";
                    return prefix + hours;
                }

                function req() {
                    var user = {},
                        d = new Date();
                    angular.copy($scope.userData, user);
                    user.usr_solt = d.getTime();
                    user.userAccept = true;
                    user.usr_name = user.secondName ? user.usr_name + ' ' + user.secondName : user.usr_name;
                    //var a = moment( user.usr_solt).tz(moment.tz.Zone.name).format();
                    //var a = moment.tz(new Date().getTime(), "EST").format();
                    var d = ( new Date().toISOString()).split("T");
                    d[1] = d[1].split(".")[0];
                    d = d.join(" ");
                    user.created = d + " (UTC/GMT" + timezone() + "h)";
                    request.add(user, 'saveUser').then(function (data) {
                        if (data['error']) {
                            if (data['type']) {
                                $mdDialog.show($mdDialog.alert({
                                    title: 'Error',
                                    textContent: data['type'],
                                    ok: 'Close'
                                })).finally(function () {
                                });
                            } else {
                                $mdDialog.show($mdDialog.alert({
                                    title: 'Error',
                                    textContent: data['error'],
                                    ok: 'Close'
                                })).finally(function () {
                                });
                            }
                        } else {
                            //$state.go('home');
                            //$parent.session.user = data['data']?data['data']:user;
                            //alert(data['success']);
                            //$scope.data.req_ac = false;
                            $mdDialog.show($mdDialog.alert({
                                title: 'Attention',
                                textContent: data['success'],
                                ok: 'Close'
                            })).finally(function () {
                                $state.go('sign_in');
                            });
                        }
                    });
                }
            },
            req_ac: function () {
                var user = $scope.userData,
                    vldt = $scope.validate,
                    $parent = $scope.$parent;
                /*  if (!user['usr_email'] || !user['usr_name'] || vldt['usr_name']['hasFeedback'] == vldt.dft.hasFeedback[2] || vldt['usr_email']['hasFeedback'] == vldt.dft.hasFeedback[2]) {
                 vldt.checkEmail();
                 vldt.checkName();
                 vldt.checkMsg();
                 return;
                 }*/
                //console.log('test');
                request.add(user, 'reqAccess').then(function (data) {
                    if (data['error']) {
                        alert(data['error']);
                    } else {
                        $scope.data.req_ac = false;
                        $scope.data.invite = data['success'];
                    }
                    //console.log(data);
                });
            },
            move: function (move) {
                $state.go(move);
                ;
            },
            close: function () {
                $parent.utils.close();
            },
            changePsw: function (form) {
                $scope.myFormFClicked = true;
                $timeout(function () {
                    if (!$scope.userData.lgn || form.$invalid) {
                        form['user_email'].$touched = true;
                        form['user_email'].$dirty = true;
                        form['user_email'].$setDirty(true);
                    } else {
                        request.get({login: $scope.userData.lgn}, 'changePsw').then(function (data) {
                            //console.log(data);
                            $mdDialog.show($mdDialog.alert({
                                title: data['error'] ? 'Error' : "Password Reset",
                                textContent: data['error'] ? data['error'] : data['success'],
                                ok: 'Close'
                            })).finally(function () {
                                if (data['success']) {
                                    $scope.changePsw = false;
                                }
                            });
                        })
                    }
                }, 100);

            },
            resetPsw: function () {
                var _t = this;
                if (!$scope.userData.psw)return;
                request.get({psw: $scope.userData.psw, token: $location.url()}, 'resetPsw').then(function (data) {
                    $mdDialog.show($mdDialog.alert({
                        title: data['error'] ? 'Error' : "Password Reset",
                        textContent: data['error'] ? data['error'] : data['success'],
                        ok: 'Close'
                    })).finally(function () {
                        if (data['success']) {
                            _t.move('sign_in');
                        }
                    });


                })
            },
            version: function () {
                $mdDialog.show({
                    clickOutsideToClose: true,
                    scope: $scope,        // use parent scope in template
                    preserveScope: true,  // do not forget this if use parent scope
                    template: '<md-dialog aria-label="LADAS SUPER FORMULA">' +
                    '  <md-dialog-content style="padding:10px ">' +
                    VERSION_APP +
                    '  </md-dialog-content>' +
                    '  <md-dialog-actions>' +
                    '    <md-button ng-click="closeDialog()" class="md-primary">' +
                    '      Close' +
                    '    </md-button>' +
                    '  </md-dialog-actions>' +
                    '</md-dialog>',
                    controller: function DialogController($scope, $mdDialog, $timeout) {
                        $scope.closeDialog = function () {
                            $mdDialog.hide();
                            $state.go('home');
                        }
                        $timeout(function () {
                            angular.element('md-backdrop.md-opaque.md-default-theme, md-backdrop.md-opaque').addClass('white-back');
                        }, 0);

                    },
                    onComplete: function () {

                    }
                });
            }
        }

        var classes = ['sign-up', 'sign', 'code-up'];
        for (var _cl = 0; _cl < classes.length; _cl++) {
            var _el = document.getElementsByClassName(classes[_cl])[0];
            if (_el) {
                _el.addEventListener('keydown', function (ev) {
                    if (ev.keyCode == 13) {
                        document.getElementsByClassName('md-raised md-hue-1 my-btn')[0].click();
                    }

                }, false);
            }

        }

        //setColorSvg()
        //console.log('*******************----------init Auth Cntr------------*****************');
    }]);
app.controller("AgreeCntrl", ["$scope", "$state", "request", "$rootScope", function ($scope, $state, request, $rootScope) {
    var $parent = $scope.$parent;
    $parent.containerVis = true;
    return $parent.utils.move('home');
    $scope.calc = {
        denied: function () {
            /* if (window.parent) {
             window.parent.location.assign("http://iolcalc.com/")
             } else {
             window.location.assign("http://iolcalc.com/")
             }*/
            //$state.go('home_en');
            alert("Please make right choose!")
        },
        access: function () {
            //fadePreload();
            //$parent.data.userAccept = true;
            //request.session('getSession').then(function (response) {
            //    $parent.session = response['session'] ? response['session'] : $parent.session;
            //    if ($parent.session.user) {
            localStorage.setItem('agreement',true)
            $state.go('home');
            //} else {
            //    $state.go('sign_in');
            //}
            //});
        }
    };

    var ses = $parent.utils.isAuth();
    if (!ses && !localStorage.getItem('agreement')) {
        $parent.utils.getSession(false, function () {
            if ($parent.utils.isAuth() || localStorage.getItem('agreement')) {
                $parent.utils.move('home');
            } else {
                $scope.calc.showPopup = true;
                fadePreload(true);
            }
        });
    } else {
        $parent.utils.move('home');
    }

    console.log('init AgreeCntrl');
}]);



