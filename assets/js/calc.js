var CALC = function () {
    var AConstant = 115.3,
        SF = (AConstant * 0.5663) - 65.6,
        ACD = (SF + 3.595) / 0.9704,
        Ref = 0,
        nc = 4 / 3,
        HofRef = Ref / (1 - (0.012 * Ref)),
        pACD = (0.58357 * AConstant) - 63.896,
        RT = 0.2,
        MWVKpr = 3.37,
        MWALpr = 23.39,
        VKpr = MWVKpr,
        ACDKonstant = (AConstant * 0.62467) - 68.747,
        a1 = 0.4,
        a2 = 0.1,
        a0 = ((ACDKonstant / 1000) - (a1 * (MWVKpr / 1000))) - (a2 * (MWALpr / 1000)),
        nCC = 1.3315,
        n = 1.336,
        na = 1,
        V = 1,
        dBC = 12;


    this.R = function (xx, yy) {
        return (337.5 / xx);
    }
    this.HofAL = function (xx, yy) {
        return (((18.5 * (yy < 18.5)) + (31 * (yy > 31))) + (yy * ((yy >= 18.5) && (yy <= 31))));
    }
    this.M = function (xx, yy) {
        return ((1 * (yy <= 23)) + (-1 * (yy > 23)));
    }
    this.G = function (xx, yy) {
        return ((28 * (yy <= 23)) + (23.5 * (yy > 23)));
    }
    this.HofACDPlaceholder = function (xx, yy) {
        return ((((pACD + (0.3 * (this.HofAL(xx, yy) - 23.5))) + (Math.pow(Math.tan(Math.radians(xx)), 2))) +
        (((0.1 * this.M(xx, yy)) * (Math.pow((23.5 - this.HofAL(xx, yy)), 2))
        ) * Math.tan(Math.radians(((0.1 * (Math.pow((this.G(xx, yy) - this.HofAL(xx, yy)), 2))))))) - 0.99166));
    }

    this.HofACD = function (xx, yy) {
        var a = (2.5 * ( this.HofACDPlaceholder(xx, yy) < 2.5)),
            b = (6.5 * ( this.HofACDPlaceholder(xx, yy) > 6.5)),
            c = (this.HofACDPlaceholder(xx, yy) * ((this.HofACDPlaceholder(xx, yy) >= 2.5) && (this.HofACDPlaceholder(xx, yy) <= 6.5)));
        return ((a + b) + c);
    }
    this.Rag = function (xx, yy) {
        return ((7 * ( this.R(xx, yy) < 7)) + ( this.R(xx, yy) * ( this.R(xx, yy) >= 7)));
    };
    this.AG = function (xx, yy) {
         return ((13.5 * (((12.5 * yy) / 23.45) > 13.5)) + (((12.5 * yy) / 23.45) * (((12.5 * yy) / 23.45) <= 13.5)));
     };
    this.AG = function (xx, yy) {
        return ((13.5 * (((12.5 * yy) / 23.45) > 13.5)) + (((12.5 * yy) / 23.45) * (((12.5 * yy) / 23.45) <= 13.5)));
    };
    this.HolACD = function (xx, yy) {
        return ((0.56 +  this.Rag(xx, yy)) - Math.sqrt(((this.Rag(xx, yy) *  this.Rag(xx, yy)) - (( this.AG(xx, yy) *  this.AG(xx, yy)) / 4))));
    };
    this.KochAL = function (xx, yy) {
        return ((0.8814 * yy) + 2.8701);
    };
    this.ALpr = function (xx, yy) {
         return yy;
     };
        this.d = function (xx, yy) {
     return ((a0 + ((a1 * VKpr) / 1000)) + ((a2 * this.ALpr(xx, yy)) / 1000));
     };
        this.RC = function (xx, yy) {
     return (337.5 / xx);
     };
        this.DC = function (xx, yy) {
     return ((nCC - 1) / (this.RC(xx, yy) / 1000));
     };
        this.Zee = function (xx, yy) {
     return (this.DC(xx, yy) + (Ref / (1 - ((Ref * dBC) / 1000))));
     };
        this.SRKI = function (xx, yy) {
     return ((AConstant - (0.9 * xx)) - (2.5 * yy));
     };
        this.HOFFERQ = function (xx, yy) {
     return ((1336 / ((yy - this.HofACD(xx, yy)) - 0.05)) - (1.336 / ((1.336 / (xx + HofRef)) - ((this.HofACD(xx, yy) + 0.05) / 1000))));
     };
        this.HOLLADAY = function (xx, yy) {
     return (((1000 * na) * (((na * this.R(xx, yy)) - ((nc - 1) * (yy + RT))) - ((0.001 * Ref) * ((V * ((na * this.R(xx, yy)) - ((nc - 1) * (yy + RT)))) + ((yy + RT) * this.R(xx, yy)))))) / ((((yy + RT) - this.HolACD(xx, yy)) - SF) * (((na * this.R(xx, yy)) - ((nc - 1) * (this.HolACD(xx, yy) + SF))) - ((0.001 * Ref) * ((V * ((na * this.R(xx, yy)) - ((nc - 1) * (this.HolACD(xx, yy) + SF)))) + ((this.HolACD(xx, yy) + SF) * this.R(xx, yy)))))));
     };
        this.KOCH = function (xx, yy) {
     return (((1000 * na) * (((na * this.R(xx, yy)) - ((nc - 1) * (this.KochAL(xx, yy) + RT))) - ((0.001 * Ref) * ((V * ((na * this.R(xx, yy)) - ((nc - 1) * (this.KochAL(xx, yy) + RT)))) + ((this.KochAL(xx, yy) + RT) * this.R(xx, yy)))))) / ((((this.KochAL(xx, yy) + RT) - this.HolACD(xx, yy)) - SF) * (((na * this.R(xx, yy)) - ((nc - 1) * (this.HolACD(xx, yy) + SF))) - ((0.001 * Ref) * ((V * ((na * this.R(xx, yy)) - ((nc - 1) * (this.HolACD(xx, yy) + SF)))) + ((this.HolACD(xx, yy) + SF) * this.R(xx, yy)))))));
     };
        this.HAIGIS = function (xx, yy) {
     return ((n / ((yy / 1000) -this.d(xx, yy))) - (n / ((n / this.Zee(xx, yy)) - this.d(xx, yy))));
     };
        this.SUPERFORMULA = function (xx, yy) {
     return ((((this.HOFFERQ(xx, yy) * (yy <= 21.49)) + (this.KOCH(xx, yy) * ((yy > 25) & (this.KOCH(xx, yy) >= 0)))) + (this.HOLLADAY(xx, yy) * ((yy > 21.49) & (yy <= 25)))) + (this.HAIGIS(xx, yy) * (this.KOCH(xx, yy) < 0)));
     };

}

Math.radians = function (degrees) {
    return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function (radians) {
    return radians * 180 / Math.PI;
};
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
