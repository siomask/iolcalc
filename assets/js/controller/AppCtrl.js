app.controller('AppCtrl', ['$scope', 'request', '$location', '$state', "$mdDialog", "$timeout", function ($scope, request, $location, $state, $mdDialog, $timeout) {
    var preloader = "preloader";
    document.title = "IOLcalc - Ladas Super Formula";
    $scope.data = {
        userAccept: false,
        userData: {},
        listOfGraph: (listOfGraph ? listOfGraph : ['THREEJSOD', 'THREEJSOS', 'THREEJSTOR']),
        html: {
            title: "IOLcalc",
            calcs: [

                {
                    n: "Toric Calculator",
                    d: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
                    s: "assets/img/home/final.png",
                    c: "tor",
                    url: 'post-lasik-calc'
                },
                {
                    n: "Ladas Super Formula Calc",
                    d: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
                    s: "assets/img/home/ladas-calc.png",
                    c: "lad",
                    url: 'home'
                },
                {
                    n: "Post Lasic Calc",
                    d: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
                    s: "assets/img/home/ladas-calc2.png",
                    c: "post",
                    url: 'toric_calc'
                }
            ]
        },
        iolRef: []
        , input: {
            Rfrctn: 0,
            K1: 44.25,
            K2: 43.98,
            axialLength: 27.45,
            ACD: 3.37,
            AConstant: 119.2/*,
             IOL: */
        }, label: {
            axialLength: "Axial Length",
            ACD: "Measured ACD",
            A: "A-Constant",
            Rfrctn: "Target Refraction",
            IOL: "IOL POWER"
        }
    };
    $scope.session = {};
    $scope.isIE = detectIE();
    $scope.containerVis = true;
    $scope.contact={};

    $scope.utils = {
        sign: {
            userData:{
                remember:true
            },
            inn: function (form,callback) {
                //console.log(form);
                if(form)form.myFormSsubmitted = true;
                var self = this;
                    $timeout(function () {
                        if (form && form.$invalid) {
                            form['userEmail'].$touched = true;
                            form['userEmail'].$dirty = true;
                            form['userEmail'].$setDirty(true);
                            form['psw'].$touched = true;
                            form['psw'].$dirty = true;
                            form['psw'].$setDirty(true);
                            return;
                        }

                    var user = self.userData ;
                    if (!user.lgn || !user.usr_psw) {
                        return;
                    }

                    request.get(user, 'getUser').then(function (data) {
                        if (data['error']) {
                            $mdDialog.show($mdDialog.alert({
                                title: 'Error',
                                textContent: data['text'],
                                ok: 'Close'
                            }));
                        } else {
                            if(!self.cacheUser(null,true))self.cacheUser(true);
                            self.userData={remember:true};
                            $scope.session.user = data['data'];
                            $scope.utils.move();
                            self.innA = !1;

                            if (data['data'] && data['data']['needToShowPoPUp']) {
                                $scope.utils.version();
                            } else {
                                if(form ){
                                    $state.go('home_en');
                                }
                                else if(callback){
                                    callback();

                                }else{
                                    $state.go('home');
                                }

                            }
                            angular.element('md-backdrop.md-opaque.md-default-theme, md-backdrop.md-opaque').css("background-color", "rgba(33,33,33,1.0)!important");
                        }
                    });
                }, 100);
            },
            cacheUser:function(flag,isGetting){
                if(isGetting){
                    return  JSON.parse(window.localStorage.getItem('userData'));
                }else if(flag){
                    if(this.userData.remember) window.localStorage.setItem('userData',JSON.stringify(this.userData));
                }else{
                    window.localStorage.removeItem('userData');
                }

            },
            up: function (form) {
                form.myFormRsubmitted = true;
                var self = this;
                if (!self.userData.agree) return;
                $timeout(function () {
                    if (form && form.$invalid) {
                        var arr = ['psw', 'secondName', 'userEmail', 'userMsg', 'userName'];
                        for (var i = 0; i < arr.length; i++) {
                            if( form[arr[i]]){
                                form[arr[i]].$touched = true;
                                form[arr[i]].$dirty = true;
                                form[arr[i]].$setDirty(true);
                            }

                        }
                    } else {
                        req();
                    }

                }, 100);
                function timezone() {
                    var offset = new Date().getTimezoneOffset();
                    var minutes = Math.abs(offset);
                    var hours = Math.floor(minutes / 60);
                    var prefix = offset < 0 ? "+" : "-";
                    return prefix + hours;
                }

                function req() {
                    var user = {},
                        d = new Date();
                    angular.copy(self.userData, user);
                    user.usr_solt = d.getTime();
                    user.userAccept = true;
                    user.usr_name = user.secondName ? user.usr_name + ' ' + user.secondName : user.usr_name;
                    delete user['agree'];
                    delete user['lgn'];
                    var d = ( new Date().toISOString()).split("T");
                    d[1] = d[1].split(".")[0];
                    d = d.join(" ");
                    user.created = d + " (UTC/GMT" + timezone() + "h)";
                    request.add(user, 'saveUser').then(function (data) {
                        if (data['error']) {
                            if (data['type']) {
                                $mdDialog.show($mdDialog.alert({
                                    title: 'Error',
                                    textContent: data['type'],
                                    ok: 'Close'
                                })).finally(function () {
                                });
                            } else {
                                $mdDialog.show($mdDialog.alert({
                                    title: 'Error',
                                    textContent: data['error'],
                                    ok: 'Close'
                                })).finally(function () {
                                });
                            }
                        } else {
                            //$state.go('home');
                            //$parent.session.user = data['data']?data['data']:user;
                            //alert(data['success']);
                            //$scope.data.req_ac = false;
                            self.userData={};
                            $mdDialog.show($mdDialog.alert({
                                title: 'Attention',
                                textContent: data['success'],
                                ok: 'Close'
                            })).finally(function () {
                                $state.go('home_en');
                            });
                        }
                    });
                }
            },
            changePswrd: function (form) {
                var self=this;
                form.myFormFClicked = true;
                $timeout(function () {
                    if (!self.userData.lgn || form.$invalid) {
                        form['user_email'].$touched = true;
                        form['user_email'].$dirty = true;
                        form['user_email'].$setDirty(true);
                    } else {
                        request.get({login: self.userData.lgn}, 'changePsw').then(function (data) {
                            //console.log(data);
                            $mdDialog.show($mdDialog.alert({
                                title: data['error'] ? 'Error' : "Password Reset",
                                textContent: data['error'] ? data['error'] : data['success'],
                                ok: 'Close'
                            })).finally(function () {
                                if (data['success']) {
                                    self.changePsw = false;
                                    self.cacheUser();
                                }
                            });
                        })
                    }
                }, 100);

            },
            resetPsw: function () {
                var _t = this;
                if (!$scope.userData.psw)return;
                request.get({psw: $scope.userData.psw, token: $location.url()}, 'resetPsw').then(function (data) {
                    $mdDialog.show($mdDialog.alert({
                        title: data['error'] ? 'Error' : "Password Reset",
                        textContent: data['error'] ? data['error'] : data['success'],
                        ok: 'Close'
                    })).finally(function () {
                        if (data['success']) {
                            _t.move('sign_in');
                        }
                    });


                })
            }
        },
        modal: function (type) {
            if (type == "auth") {

            } else if (type == "register") {

            }
        },
        isAdmin: function () {
            return $scope.session.user && $scope.session.user.usr_type == 'Admin'
        },
        isAuth: function () {
            return $scope.session.user;
        },
        logIn: function () {
            var user = $scope.data.userData;
            $scope.submitted = true;
            if (!user.lgn || !user.usr_psw) {
                return;
            }
            request.get(user, 'getUser').then(function (data) {
                if (data['error']) {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Error',
                        textContent: data['text'],
                        ok: 'Close'
                    })).finally(function () {
                    });
                } else {
                    $scope.session.user = data['data'];
                }

                $scope.submitted = false;
            });
        },
        close: function () {
            $scope.containerVis = false;
            $location.path('/');
        },
        logOut: function () {
            var self = this;
            request.session('clearSession').then(function (response) {
                $scope.session = response['session'];
                self.sign.cacheUser();
                self.move('home_en');
            });
        },
        preload: function (flag) {

            if (flag) {
                jQuery(preloader_animate).fadeIn();
                jQuery('body').css('overflow', 'hidden');
                new ProgreesLoad().start();
            } else {

                jQuery(preloader_animate).fadeOut(10);
                jQuery('body').css('overflow', 'auto');


            }
        },
        move: function (m) {
            var auth = ['post-lasik-calc','toric_calc','home'/*,'calculators'*/],
                move = m?m:this.lastMove;

            this.lastMove = m;
            for(var i=0;i<auth.length;i++){
                if(auth[i] == move && !$scope.session.user){
                    return $scope.utils.sign.innA = true;
                }
            }
            if(move){
                // if(move =='home_en')this.preload(true);
                $timeout(function(){
                    $state.go(move);
                },1)

                $scope.mobile_nav_menu= null;
            }
        },
        checkState:function(){
            var auth = ['post-lasik-calc','toric_calc','home','calculators'];
            var url = location.pathname;

            for(var i=0;i<auth.length;i++){
                if(url.match(auth[i])){

                    return  $state.go('home_en');
                }
            }
        },
        changeName: function (user, type) {
            var user = $scope.session.user;
            if (user && user.usr_name) {
                if (type == "usr_name") {
                    var arr = user.usr_name.split(" ");

                    user.firstName = arr[0];
                    user.secondName = '';
                    for (var i = 1; i < arr.length; i++) {
                        user.secondName += arr[i];
                    }

                } else {
                    user.usr_name = user.firstName + " " + user.secondName;
                }
            }

        },
        getSession: function (callback, callbackes) {
            var self = this;
            request.session('getSession').then(function (response) {
                $scope.session = response['session'] ? response['session'] : $scope.session;

                if ($scope.session.user && callback) {
                    if (callback instanceof Function)callback();
                } else if (callbackes instanceof Function) {
                    callbackes();
                } else {
                    var userCacheData =   self.sign.cacheUser(null,true);
                    if(userCacheData){
                        for(var f in userCacheData){
                            self.sign.userData[f] = userCacheData[f];
                        }
                        self.sign.inn(null,callback);
                    }else{
                        self.move('home_en');
                    }


                }
            });
        },
        version: function (terms) {
            var result = terms?TERMS_CONDITIONS:VERSION_APP;
            $mdDialog.show({
                clickOutsideToClose: true,
                scope: $scope,        // use parent scope in template
                preserveScope: true,  // do not forget this if use parent scope
                template: '<md-dialog aria-label="IOLcalc">' +
                '  <md-dialog-content style="padding:10px; ">' +
                    '<span class="close" data-ng-click="closeDialog()">X</span>'+
                result+
                '  </md-dialog-content>' +
                //'  <md-dialog-actions>' +
                //'    <md-button ng-click="closeDialog()" class="md-primary">' +
                //'      Close' +
                //'    </md-button>' +
                //'  </md-dialog-actions>' +
                '</md-dialog>',
                controller: function DialogController($scope, $mdDialog, $timeout) {
                    $scope.closeDialog = function () {
                        $mdDialog.hide();
                    }
                    $timeout(function () {
                    }, 1500);

                },
                onComplete: function () {
                }
            });
        },
        updateInputData: function (data, outputs, fil, klist, argSet) {
            var user = $scope.session.user, id_val, uploadData, flag;
            if (argSet) {
                id_val = argSet['i'],
                    uploadData = argSet['d'],
                    flag = argSet['f']
            }

            if (flag) {
                var defInput = uploadData,
                    _inp = data[outputs[0]];
                user.calc_input = user.calc_input ? user.calc_input : {};
                for (var field in defInput) {
                    if (field == 'id_user' || field == 'id_val') {
                        user.calc_input[field] = (defInput[field]);
                    } else {
                        user.calc_input[field] = (  defInput[field] || defInput[field] == 0) ? (defInput[field]) : defInput[field];
                    }
                }
                if (!user.calc_input['id_val'] && id_val) {
                    user.calc_input['id_val'] = id_val;
                    _inp['id_val'] = id_val;
                }
            } else {
                if (!$scope.session.user)return;
                var defInput = flag ? uploadData : user.calc_input,
                    _dat = data,
                    fields = [{or: "KIndex", cstm: fil['KI']}, {or: "AConstant", cstm: fil['AC']}, {or: "Rfrctn", cstm: fil['Rfrctn']}];
                if (defInput instanceof Array) {
                    defInput.session.user.calc_input = {};
                }
                for (var field in defInput) {
                    if (((field == 'id_user' || field == 'id_val') && !argSet) ||  (!defInput[field] && isNaN(parseFloat(defInput[field]))) )continue;
                    var value = defInput[field], curFil = field;

                    switch (field) {
                        case fields[0].or:
                        {
                            var el = value + "";
                            if (klist.indexOf(el) < 0)klist.push(el);
                            value = el;
                            curFil = fields[0].cstm;
                            break;
                        }
                        case fields[1].or:
                        {
                            curFil = (fields[1].cstm);
                            value = parseFloat(value);
                            break;
                        }
                        case fields[2].or:
                        {
                            curFil = (fields[2].cstm);
                            value = parseFloat(value);
                            break;
                        }
                    }

                    if (value || value === 0) {
                        outputs.forEach(function (o) {
                            _dat[o][curFil] = value;
                        });
                    }
                }
            }
        },
        contact:function(form){
            $scope.contact.isClicked = !0;

            setTimeout(function(){
                if(form.$valid ){
                    request.add( $scope.contact, 'contact').then(function (data) {
                        console.log(data);
                        $mdDialog.show($mdDialog.alert({
                            title: data.error?'Error':'Info',
                            textContent: data.error?data.error:data.success,
                            ok: 'Close'
                        })).finally(function () {
                            $scope.contact={};
                        })
                    }, function (error) {
                        console.log(error);
                    });
                }
            },300)

        },
        verify: function(res){}
    };
    loadPrintCSS($scope.isIE);
    $timeout(function(){
        $scope.utils.getSession(function(){});
    },500);

    $scope.$watch(function (scope) {
            return $location.$$path;
        },
        function (newValue, oldValue) {
            $scope.curLocal= newValue;
        });
    var classes = ['sign-up', 'sign', 'code-up'];
    for (var _cl = 0; _cl < classes.length; _cl++) {
        var _el = document.getElementsByClassName(classes[_cl])[0];
        if (_el) {
            _el.addEventListener('keydown', function (ev) {
                if (ev.keyCode == 13) {
                    document.getElementsByClassName('md-raised md-hue-1 my-btn')[0].click();
                }

            }, false);
        }

    }

}]);
