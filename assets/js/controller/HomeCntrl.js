app.controller("HomeCntrl", ["$scope", "$state", "request", "$timeout", "$mdDialog", "$compile", "$http",
    function ($scope, $state, request, $timeout, $mdDialog, $compile, $http) {
        jQuery(preloader_animate).css('display', 'block');
        var $parent = $scope.$parent;
        var ses = $parent.utils.isAuth();
        $scope.patientName = '';
        if (!ses) {
            $parent.utils.getSession(updateInputData);
        }
        $parent.containerVis = true;

        $scope.calc = {
            open: function (url) {
                request.add({}, 'parsemdb').then(function (data) {
                    console.log(data);
                });
            },
            plot: function (threejs) {
                var isRight = threejs == listOfGraph[0],
                    _t = this,
                    _dat = $scope.data,
                    form = [_dat[_inpF.rF], _dat[_inpF.lF]],
                    input = _dat.input,
                    _lidtIn = [_inpF.r, _inpF.l],
                    _eye = isRight ? "OD" : "OS",
                    d = _dat[_lidtIn[(isRight ? 0 : 1)]],
                    curFormCalc = isRight ? form[0] : form[1],
                    _iol = "#" + (isRight ? _inpF.iol.od : _inpF.iol.os),
                    inputTabl = _iol + "input";


                if (!IS_WEBGL_AVIALABLE) {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Warning',
                        textContent: "WebGl is not supported in your browser",
                        ok: 'Close'
                    })).finally(function () {
                        checkBothForm();
                    });
                } else {
                    checkBothForm();
                }
                function checkBothForm() {

                    var onFinish = function (flag) {
                        if (flag) {
                            $scope.data.webgl = {name: threejs, arg: flag, act: 'draw'};
                        } else {
                            $scope.data.webgl = {name: threejs, act: 'clear'};
                            //jQuery(inputTabl).fadeOut();
                        }

                        if (isRight) {
                            _t.plot(listOfGraph[1]);
                        } else {

                            // var s = angular.element('#' + listOfGraph[0]).find('three-js-out').attr('was-drawed') == "yeas",
                            //     d = angular.element('#' + listOfGraph[1]).find('three-js-out').attr('was-drawed') == "yeas";
                            //
                            // _dat.isInputing = !(s || d);
                            // _dat.summary = s || d;
                            setTimeout(function () {

                                $parent.utils.preload();
                            }, 300);


                        }
                    };
                    var arr = ['a', 'k', 'kk', 'f', 'b', 'e', 'd'], inputError = false;
                    for (var i = 0; i < arr.length; i++) {
                        var _c = arr[i],
                            fieldName = _inpF[_c],
                            val = parseFloat(d[fieldName]);

                        if (isNaN(val)) {
                            if (_c == "b") continue;
                            inputError = true;
                        }
                        var inputValInter = _intVl[_c];
                        if (val && !isNaN(val) && (val < inputValInter.min || val > inputValInter.max)) {
                            if (!isRight) $parent.utils.preload();
                            return (isRight && form[1].$invalid || !isRight && form[0].$invalid || !_dat[_lidtIn[0]].calcsFormSubmitted ? $mdDialog.show($mdDialog.alert({
                                title: 'Error',
                                textContent: fieldName + " on " + (isRight ? "Right (OD)" : "Left (OS)") + " must be between " + inputValInter.min + " and " + inputValInter.max,
                                ok: 'Close'
                            })).finally(function () {
                                onFinish();
                            })
                                : onFinish());
                        } else {
                            var fil = curFormCalc[fieldName];
                            fil.$setValidity("range", 1);
                            fil.$setDirty(1);
                            fil.$render();
                            // angular.element(fil).removeClass('input-err');
                        }

                    }
                    _dat[_lidtIn[0]].calcsFormSubmitted = _dat[_lidtIn[1]].calcsFormSubmitted = true;
                    if (isRight)$parent.utils.preload(isRight);
                    $timeout(function () {
                        var _fF = form[0].$invalid,
                            _fS = form[1].$invalid;
                        if ((!_dat.neeSecEye && (  _fF && _fS )) || inputError) {
                            for (var k = 0; k < form.length; k++) {
                                var curForm = form[k],
                                    domF = document.getElementsByName(curForm.$name);
                                if (curForm.$invalid) {
                                    for (var i = 0; i < arr.length; i++) {
                                        var fieldName = _inpF[arr[i]],
                                            inputValInter = _intVl[arr[i]];
                                        if (curForm[fieldName] && (curForm[fieldName].$invalid || (parseFloat(curForm[fieldName].$viewValue) < inputValInter.min || parseFloat(curForm[fieldName].$viewValue) > inputValInter.max))) {
                                            curForm[fieldName].$touched = true;
                                            curForm[fieldName].$dirty = true;
                                            curForm[fieldName].$setDirty(true);
                                            jQuery(domF).find(document.getElementsByName(fieldName)).removeClass('input-succeces').addClass('input-err');
                                        } else {
                                            jQuery(domF).find(document.getElementsByName(fieldName)).removeClass('input-err').addClass('input-succeces');
                                        }

                                    }
                                }
                            }
                            //if(!isRight)  }
                            onFinish();

                        } else {

                            $timeout(function () {
                                var calcVal = {
                                    type: 'homeCalculate',
                                    AConstant: (d[fields[5]]),
                                    ACD: (d[fields[11]]        ),
                                    K1: (d[fields[2]]          ),
                                    K2: (d[fields[3]]          ),
                                    axialLength: (d[fields[10]] ),
                                    KIndex: parseFloat(d[fields[1]]),
                                    needInputACD: (d[fields[11]] && d[fields[17]]),
                                    Rfrctn: (d[fields[12]]),
                                    patient: {name: _dat.ptntName, id: _dat.ptntID, eye: _eye}
                                };
                                var onSucceces = function (_r) {
                                    if (_r) {
                                        calcVal._r = _r;
                                        if (isRight) {
                                            _dat.iolODRef = _r.iolRef;
                                        } else {
                                            _dat.iolOSRef = _r.iolRef;
                                        }
                                    }
                                    jQuery(_iol).fadeIn();

                                    onFinish(_r);
                                    //jQuery(inputTabl).fadeIn();
                                };
                                var onError = function (text, av) {
                                    document.getElementsByClassName('tor-list-menu')[0].focus();
                                    $parent.utils.preload();
                                    $mdDialog.show($mdDialog.alert({
                                        title: 'Error',
                                        textContent: text,
                                        ok: 'Close'
                                    })).finally(function () {
                                        _dat.neeSecEye = isRight;
                                        if (av) {
                                            if (curFormCalc && curFormCalc[fields[11]])curFormCalc[fields[11]].$setValidity("required", true);
                                            jQuery(document.getElementsByName(curFormCalc.$name)).find(document.getElementsByName(fields[11])).addClass('input-err');
                                            d[_inpF.an] = "yeas";
                                        }

                                        onFinish();
                                        jQuery(_iol).fadeOut();
                                        //jQuery(inputTabl).fadeOut();
                                        //resizeThreejsDiv();

                                    });
                                };
                                /* function isSamInputData(values) {
                                 if (values) {
                                 for (var field in calcVal) {
                                 if (calcVal[field] != values[field]) {
                                 return false;
                                 }
                                 }
                                 } else {
                                 return false;
                                 }
                                 return true;
                                 }

                                 var curVal = $parent['calcVal'+threejs];
                                 if(isSamInputData(curVal)){  onFinish();if((isRight && _fS)|| !isRight){  $parent.utils.preload(); return  }}
                                 var check = (isRight?$parent['calcVal'+listOfGraph[1]]:$parent['calcVal'+listOfGraph[0]]);
                                 if( isSamInputData(check)) return onSucceces(check._r);*/

                                $parent['calcVal' + threejs] = calcVal;
                                request.add(calcVal, 'calculate').then(function (data) {

                                    $scope.submitted = false;
                                    if (data['error']) {
                                        onError(data['error'], data['ACDneedToBeRequred']);
                                    } else {
                                        if (data["success"]) {

                                            var _r = data['calc'], needDrop = [];
                                            for (var i = 0; i < _r.iolRef.length; i++) {
                                                var _c = _r.iolRef[i];
                                                _c.iol = (_c.iol).toFixed(2);
                                                _c.ref = (_c.ref).toFixed(2);
                                                /*if( _c.middle){
                                                 if(_r.iolRef[i-1].ref ==_c.ref)needDrop.push(i-1);
                                                 if(_r.iolRef[i+1].ref ==_c.ref)needDrop.push(i+1);
                                                 }*/
                                            }
                                            //needDrop.forEach(function(o){_r.iolRef.splice(o,1)});
                                            for (var i = 0; i < _r.faces.length; i++) {
                                                var _c = _r.faces[i];
                                                _r.faces[i] = new THREE.Face3(_c.x, _c.y, _c.z);
                                            }
                                            for (var i = 0; i < _r.points.length; i++) {
                                                var _c = _r.points[i];
                                                _r.points[i] = new THREE.Vector3(_c.x, _c.y, _c.z);
                                            }
                                            //$scope.$apply();
                                            onSucceces(_r);
                                        } else {
                                            onError("Some bad calculate");
                                        }

                                    }


                                }, function (error) {
                                    onError("Some bad calculate");
                                });
                            }, 100);

                        }
                    }, isRight ? 200 : 1);
                }

            },
            reset: function (form, threejs, isResetBgn, isEdit) {
                var isRight = threejs == listOfGraph[0],
                    _dat = $scope.data,
                    _iol = '#' + (isRight ? _inpF.iol.od : _inpF.iol.os),
                    _inp = isRight ? _inpF.r : _inpF.l;
                $scope.submitted = true;
                $scope.data.lensModel.reset();

                $scope.isEdit = isEdit;

                $scope.data.webgl = {name: threejs, act: 'clear'};

                if (!isEdit) {
                    _dat.ptntName = _dat.ptntID = null;
                }

                if (isResetBgn || isEdit) {
                    angular.element(preloader_animate).css('display', 'block');
                    if (!isEdit) _dat.ptntName = _dat.ptntID = null;
                    _dat.isInputing = 1;
                    _dat.summary = 0;
                    if ($scope.toggle)this.toggle();
                }

                if (!isEdit) {
                    _dat.patient = {};
                    _dat[_inp] = {};
                    _dat[_inp][fields[1]] = _dat.Kindexes[3];
                }

                $timeout(function () {
                    if (!isEdit) {
                        var arr = ['a', 'b', 'k', 'kk', 'e', 'd'],
                            domF = document.getElementsByName(isRight ? form.$name : _inpF.lF);
                        for (var i = 0; i < arr.length; i++) {
                            var inpName = _inpF[arr[i]];
                            if (form[inpName]) {
                                form[inpName].$touched = false;
                                form[inpName].$dirty = false;
                                angular.element(domF).find(document.getElementsByName(inpName)).removeClass('input-err').addClass('input-succeces');
                            }

                        }
                        updateInputData();
                    }

                    angular.element(_iol).fadeOut();
                    if (isRight) {
                        $scope.calc.reset(_dat[_inpF.lF], listOfGraph[1], isResetBgn, isEdit);
                    } else {
                        $parent.utils.preload();
                        //angular.element('.iolData').parent().css("height", 0);
                    }
                }, 100);

            },
            print: function (threejs) {
                var strMime = "image/png",
                    isRight = threejs == listOfGraph[0],
                    _t = this;
                $parent.utils.preload(true);
                listOfGraph.forEach(function (o, k) {
                    if (k < 2) {
                        $timeout(function () {
                            $scope.data.webgl = {name: o, act: 'print'};
                        }, 100);

                    }
                });

                $scope.data.eyeActive = isRight ? "Right (OD)" : "Left (OS)";

                jQuery('#pdf-content').css({display: "block"});
                jQuery('.pdf-content').css({top: 0 + 'px', left: 0 + 'px'});
                jQuery('body').css({overflow: 'hidden'});

                setTimeout(function () {
                    var pdfHtml = document.getElementsByClassName('pdf-content'),
                        sd = pdfHtml[0];

                    var onfinish = function () {
                        var _y = jQuery('.pdf-content').height(),
                            _x = jQuery('.pdf-content').width();
                        jQuery('.pdf-content').css({
                            top: -_y + 'px',
                            left: -_x + 'px'
                        });
                        document.title = "IOLcalc - Ladas Super Formula";
                        jQuery('body').css({overflow: 'auto'});
                        request.add({statistic: {st_printes: 1}}, 'statistic').then(function (data) {
                        });
                    };


                    html2canvas(sd).then(function (canvas) {
                        var imgData = canvas.toDataURL(strMime);
                        var link = document.getElementById('contvd');
                        link.src = imgData;
                        $parent.utils.preload();

                        var title = "LSF IOL Calc" +
                            ($scope.data.ptntID ? " - " + $scope.data.ptntID : "") +
                            ($scope.data.ptntName ? " - " + $scope.data.ptntName : "");
                        document.title = title;

                        setTimeout(function () {
                            if ($parent.isIE) {
                                onfinish();
                                var delta = 18,
                                    _h = 750,
                                    _w = 545;
                                jQuery(link).height((_h + _h * 0.2));
                                _h += delta;
                                var myWindow = window.open('', 'IE PRINT', 'width=' + _w + ',height=' + (_h));
                                myWindow.document.title = title;
                                myWindow.document.body.innerHTML = jQuery('#contvdParent').html();
                                myWindow.document.close(); //missing code
                                myWindow.focus();
                                myWindow.print();

                            } else {
                                window.print();
                            }
                            onfinish();
                        }, 100);
                    }, function (err) {
                        console.log(err, "--------------------");
                    });

                }, 200);

            },
            changePrintView: function (flag) {
                var _t = this;
                if (!_t.isOpenPDF) {
                    jQuery('.pdf-content').css({
                        transform: "scale(0.2)",
                        left: '-980px',
                        top: '-1380px',
                        position: "relative"
                    });
                    _t.isOpenPDF = true;
                }

                _t.backSubmit = true;
                if (flag) {
                    $parent.utils.preload();
                    jQuery('.pdf-content').css({left: '-980px', top: '-1380px', "z-index": 10, position: "relative"});
                    //jQuery('.pdf-content').css({"z-index":10,left:'0px',top:'0px'});
                    jQuery('.cont').css({display: "none"});
                    jQuery('#backSubmit').css({display: "block"});
                    jQuery('#calcCostum').css({display: "none"});
                    jQuery('body').css({overflow: "hidden"});

                } else {
                    var _y = jQuery('.pdf-content').height(),
                        _x = jQuery('.pdf-content').width()
                    jQuery('.pdf-content').css({
                        top: -_y + 'px',
                        left: -_x + 'px'
                    });
                    jQuery('.pdf-content').css({"z-index": -10, position: "relative"});
                    //jQuery('.pdf-content').css({ "z-index":-10});
                    jQuery('.cont').css({display: "block"});
                    jQuery('#calcCostum').css({display: "block"});
                    jQuery('#backSubmit').css({display: "none"});
                    jQuery('#pdf-content').css({display: "none"});
                    jQuery('body').css({overflow: "scroll"});
                    resizeThreejsDiv();
                }


            },
            cutImage: function (im) {
                var c = document.createElement('canvas');
                var img = new Image();
                img.src = im;
                c.width = img.width;
                c.height = img.height;
                var ctx = c.getContext('2d');
                ctx.drawImage(img, 0, 0, c.width, c.height);
                var _pst = getCanvasActivePixels(c);

                var canvas = document.createElement('canvas');
                $(canvas).css('position', 'absolute');
                canvas.width = _pst.x2 - _pst.x1;//endX-firstX ;
                canvas.height = _pst.y2 - _pst.y1;//endY-firstY ;
                var context = canvas.getContext('2d');
                var st = 0;
                context.drawImage(img, _pst.x1 - st, _pst.y1 - st, canvas.width, canvas.height, st, st, canvas.width, canvas.height);
                return canvas.toDataURL("image/png");
            },
            printOld: function () {
                //$('#actions').fadeOut();
                $('#threejsPrint').fadeIn();
                renderer.domElement.getContext("experimental-webgl", {preserveDrawingBuffer: true});
                var strMime = "image/png",
                    img = document.getElementById('threejsPrint').childNodes[1],
                    img_ = document.getElementById('threejsPrint_').childNodes[1]
                    ;
                img.src = renderer.domElement.toDataURL(strMime);
                img_.src = renderer.domElement.toDataURL(strMime);
                var bodyH = window.innerHeight;
                //console.log(bodyH);
                var sd = document.getElementsByClassName('container_main')[0];
                $(sd).css(
                    //'height',(bodyH)+'px',
                    'width', (bodyH * 1.414) + 'px');
                //$('.contvd').css('width',(bodyH*1.4143)+'px');
                html2canvas(sd).then(function (canvas) {
                    var imgData = canvas.toDataURL(strMime);
                    var link = document.getElementById('contvd')//document.createElement('a');
                    link.src = imgData//.replace(strMime, "image/octet-stream");
                    $('#threejsPrint').fadeOut();
                    $(sd).css(
                        'width', '100%');
                    setTimeout(function () {
                        window.print();
                    }, 100);

                });
                //$('#actions').fadeIn();
            },
            isAdmin: function () {
                return $parent.utils.isAdmin();
            },
            createPromo: function () {
                $scope.submitted = true;
                if (!$scope.data.promocode) {
                    return
                }
                request.add({
                    code: $scope.data.promocode,
                    user: $parent.session.user
                }, 'savePromoCode').then(function (data) {
                    if (data['error']) {
                        alert(data['error']);
                    } else {
                        $scope.promoCode = false;
                        alert(data['success']);
                    }
                    $scope.submitted = false;
                });
            },
            upload: function () {
                document.querySelector('#fileInput').click();
            },
            changeFile: function ($file, $message, $flow) {
            },
            toggle: function () {
                $scope.toggle = !$scope.toggle;
                var iyt = setInterval(function () {
                    resizeThreejsDiv();
                }, 10);

                var _h = jQuery(".body-card").height() + 16;
                jQuery(".body-card").css({"min-height": _h, "max-height": _h})
                $(".target").toggle("slow", function () {
                    jQuery(".body-card").css({"min-height": 0, "max-height": 10 * _h})
                    resizeThreejsDiv();
                    clearInterval(iyt);
                });
            },
            edit: function () {

            },
            onInput: function (name, isRight) {

                var _dat = $scope.data,
                    input = isRight ? _dat[_inpF.r] : _dat[_inpF.l],
                    domF = isRight ? _inpF.rF : _inpF.lF;
                domF = document.getElementsByName(domF);
                switch (name) {
                    case _inpF.a:
                    {
                        if ((input[_inpF.an] || input[_inpF.ar])) {
                            input[_inpF.ar] = true;
                            var isCor = input[_inpF.a] > 30 || input[_inpF.a] < 20 || !input[_inpF.a];
                            input[_inpF.an] = isCor ? "yeas" : null;
                            if (!input[_inpF.b])jQuery(domF).find(document.getElementsByName(_inpF.b)).removeClass(isCor ? _classes.ok : _classes.er).addClass(isCor ? _classes.er : _classes.ok);

                        }
                        break;
                    }
                }
            }
        };

        var exam,
            fields = [
                "T",
                "K Index",
                "K1",
                "K2",
                "T",
                "A-Constant",
                "K1",
                "T",
                "K2",
                "T",
                "Axial Length",
                "Optical ACD",
                "Target Refraction",
                "T",
                "T",
                "T",
                "WTW",
                "ACDneedToBeRequred",
                "Cct",
                "AD",
                "ACDResultFromServer",
                "Lens Model"
            ],
            _inpF = {
                a: fields[10],
                b: fields[11],
                c: fields[20],
                ad: fields[19],
                g: fields[2],
                h: fields[3],
                d: fields[12],
                e: fields[5], //const
                k: fields[2],
                kk: fields[3],
                f: fields[1],
                an: fields[17],
                ar: fields[20],
                lensModel: fields[21],
                r: "inputOD",
                l: "inputOS",
                rF: "calcsODForm",
                lF: "calcsOSForm",
                iol: {
                    od: "iolODData",
                    os: "iolOSData"
                }
            },
            calcSubmit = "calcsFormSubmitted",
            _intVl = {
                a: {min: 15, max: 35, r: calcSubmit},
                b: {min: 0, max: 5, r: "ACDneedToBeRequred"},
                c: {min: -1000, max: 100, r: calcSubmit},
                ad: {min: -10, max: 10, r: calcSubmit},
                d: {min: -10, max: 10, r: calcSubmit},
                e: {min: 110, max: 120, r: calcSubmit, addiTional: '. Please use ULIB A-Constant'},
                k: {min: 35, max: 55, r: calcSubmit},
                kk: {min: 35, max: 55, r: calcSubmit},
                f: {min: 0, max: 2, r: calcSubmit}
            },
            _classes = {
                er: 'input-err',
                ok: 'input-succeces'
            };

        $scope.data = {
            name: 'HomeCntrl',
            _inpF: _inpF,
            _intVl: _intVl,
            _fields: fields,
            curFieldes: ['a', 'k', 'kk', 'f', 'b', 'e', 'd'],
            calcs: ["1", "2"],
            inpDts: [
                {head: "Measurements:", list: ['a', 'k', 'kk', 'f', 'b']},
                {head: "Lens Selection:", list: ['lensModel', 'e', 'd']}
            ],
            isInputing: 1,
            Kindexes: ['1.3315', '1.3320', '1.3360', '1.3375', '1.3380'],
            lensModel: {
                reset:function(){
                    this.selected = this.other = this.showOthers = null;
                    this.onChange();
                },
                onChange: function () {
                    var self = this,
                        _selected = this.selected;
                    if (this.selected) {
                        this.other = this.selected;
                        $scope.data[_inpF.r][_inpF.e] = '';
                        $scope.data[_inpF.l][_inpF.e] = '';

                        _selected = _selected.toLowerCase();
                        for (var i = 0; i < this._curItems.length; i++) {
                            this._curItems[i].els = [];
                            for (var u = 0; u < this._items[i].els.length; u++) {
                                if (this._items[i].els[u].IOL.toLowerCase().match(_selected))this._curItems[i].els.push(this._items[i].els[u]);
                            }
                        }
                    } else {
                        for (var i = 0; i < this._curItems.length; i++) {
                            this._curItems[i].els = [];
                            for (var u = 0; u < this._items[i].els.length; u++) {
                                this._curItems[i].els.push(this._items[i].els[u]);
                            }
                        }
                    }
                    this.showOthers = !(this._curItems[0].els.length || this._curItems[1].els.length);
                },
                toggleInput: function (formsInput, show) {
                    if(this.timeout)clearTimeout(this.timeout);
                    this.timeout = setTimeout(function () {
                        formsInput.showLensItems = !!show;
                        if (show)setTimeout(function () {
                            document.getElementById('search_lens' + show).focus();
                        }, 500);
                        $scope.$apply();
                    }, 100)
                },
                onSelect: function (item) {
                    this.selected = item.IOL;
                    this.other = null;
                    var _dat = $scope.data;
                    _dat[_inpF.r][_inpF.e] = _dat[_inpF.l][_inpF.e] = item.AL;
                    for (var i = 0; i < this._curItems[0].els.length; i++) {
                        if (this._curItems[0].els[i].IOL == item.IOL){
                            this._curItems[0].els.unshift(this._curItems[0].els.splice(i, 1)[0]);
                            return;
                        }
                    }
                    this._curItems[0].els.splice(0, 0, item);
                    for (var i = 0; i < this._curItems[1].els.length; i++) {
                        if (this._curItems[1].els[i].IOL == item.IOL){
                            this._curItems[1].els.splice(i, 1);
                            break;
                        }
                    }
                    this._curItems[1].els.push(this._curItems[0].els.pop());
                    this._curItems[1].els.sort(function (a, b) {
                        if (a.IOL > b.IOL) {
                            return 1;
                        }
                        if (a.IOL < b.IOL) {
                            return -1;
                        }
                        return 0;
                    });
                    // TODO: Save the order position by request
                },
                showOthers: false,
                selected: null,
                other: null,
                showItems: false,
                _items: [],
                _curItems: [
                    {title: "Commonly Used Lenses", els: []},
                    {title: "More Lenses", els: [],scrollable:true},
                    {title: "The __ lens was not found. Use ___ lens?", els: [],custom:true}
                ],
                items: [],
                search: '',
                value: 0,
                autoCompleteSet: {
                    isDisabled: true,
                    states: [],
                    querySearch: function querySearch(query) {
                        var results = query ? $scope.data.lensModel.items.filter((function (a) {
                            return a && a.IOL && a.IOL.toLowerCase().match(query)
                        })) : $scope.data.lensModel.items;

                        return results;
                    },
                    selectedItem: function (item) {
                        if (!item)return;
                        var _dat = $scope.data;
                        _dat[_inpF.r][_inpF.e] = _dat[_inpF.l][_inpF.e] = item.AL;
                    },
                    selectedItemChange: false,
                    searchTextChange: false,
                    searchText: '',
                    newState: false,
                }
            },
            userData: {},
            inputOD: {},
            inputOS: {},
            iolODRef: [],
            iolOSRef: [],
            cards: [
                {
                    eye: "Right (OD)",
                    formName: _inpF.rF,
                    outPut: _inpF.r,
                    listPrint: $parent.data.listOfGraph[0],
                    outData: _inpF.iol.od,
                    outValues: 'iolODRef'
                },
                {
                    eye: "Left (OS)",
                    formName: _inpF.lF,
                    outPut: _inpF.l,
                    listPrint: $parent.data.listOfGraph[1],
                    outData: _inpF.iol.os,
                    outValues: 'iolOSRef'
                }
            ],
            file: {
                old: [
                    {
                        n: 'AxialData',
                        f: [{xml: 'Acd', or: fields[11]},
                            {xml: 'Al', or: fields[10]},
                            {xml: 'Cct', or: fields[18]}]
                    },
                    {
                        n: 'Keratometry',
                        f: [{xml: 'SteepK', or: fields[8]},
                            {xml: 'FlatK', or: fields[6]},
                            {xml: "RefractiveIndex", or: fields[1]}]
                    }
                ],
                _new: [
                    {
                        f: [
                            {xml: 'K1', or: fields[2]},
                            {xml: 'n', or: fields[1]},
                            {xml: 'K2', or: fields[3]},
                            {xml: 'CCT', or: fields[18]},
                            {xml: 'AD', or: fields[19]},
                            {xml: 'AL', or: fields[10]},
                            {xml: 'WTW', or: fields[16]},
                            {xml: 'K1Axis', or: fields[5]}
                        ]
                    }
                ]
            },
            label: {
                web_header: "Ladas Super Formula"
            }
        };
        $scope.data[_inpF.r][fields[1]] = $scope.data.Kindexes[3];
        $scope.data[_inpF.l][fields[1]] = $scope.data.Kindexes[3];

        if (ses) updateInputData();
        function updateInputData() {
            $parent.utils.updateInputData($scope.data,
                [_inpF.r, _inpF.l],
                {KI: fields[1], AC: fields[5], Rfrctn: fields[12]}, $scope.data.Kindexes);

            $timeout(function () {
                setColorSvg(0, [{selector: '.lad-calc-btn', col: "#ffffff"}, {
                    selector: '.lad-imortant',
                    col: "#1476bd"
                }]);
                $parent.utils.preload();
            }, 200);

        }



        (function () {
            jQuery.getJSON("api/storage/alData.json", function (data) {
                //$scope.data.lensModel.items = $scope.data.lensModel.curItems = $scope.data.lensModel.autoCompleteSet.states = data;
                //$scope.data.lensModel.autoCompleteSet.isDisabled = false;

                var lensModel = $scope.data.lensModel;
                lensModel._items[0] = {els: data.splice(0, 5)};
                lensModel._items[1] = {els: data};
                lensModel._items[2] = {els: []};


                for (var i = 0; i < lensModel._curItems.length; i++) {
                    lensModel._curItems[i].els = [];
                    for (var u = 0; u < lensModel._items[i].els.length; u++) {
                        lensModel._curItems[i].els.push(lensModel._items[i].els[u]);
                    }
                }
            });
        })()

        document.getElementsByClassName('calc-content')[0].addEventListener('keydown', function (ev) {

            if (ev.keyCode == 13) {
                jQuery("#plotf").click();
            }
        }, false);
    }]);