app.controller("PostCalcCntrl", ["$scope", "$state", "request", "$timeout", "$mdDialog", "$compile",
    function ($scope, $state, request, $timeout, $mdDialog, $compile) {
        jQuery(preloader_animate).css('display', 'block');
        var $parent = $scope.$parent;
        var ses = $parent.utils.isAuth();
        $scope.patientName = '';
        if (!ses) {
            $parent.utils.getSession(updateInputData);
        }
        $scope.calc = {
            plot: function (postForm) {
                var min = 10, max = 50, _t = this,
                    _d = $scope.data;
                if (postForm.$invalid) {
                    return;
                }
                function onError(text) {
                    $parent.utils.preload();
                    $mdDialog.show($mdDialog.alert({
                        title: 'Error',
                        textContent: text,
                        ok: 'Close'
                    })).finally(function () {

                    });
                }

                if (!webglEl) {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Warning',
                        textContent: "WebGl is not supported in your browser",
                        ok: 'Close'
                    })).finally(function () {
                        calculation();
                    });
                } else {
                    calculation();
                }
                function calculation() {
                    $parent.utils.preload(true);
                    $timeout(function () {

                        var calcVal = {};
                        angular.copy(_d._outputs,calcVal);
                        calcVal[fields[26]] = calcVal[fields[26]] =="Other"?calcVal[fields[25]]:calcVal[fields[26]];
                        calcVal.type='postCalculate';

                        request.add(calcVal, 'calculate').then(function (data) {
                            var _dt = data['data'];
                            $scope.isCalc = _dt;

                            if (data['error']) {
                                onError(data['error'])
                            } else {
                                    var _ls = _d.curList == "nic" ? [_d.res.rk] : [_d.res.mr, _d.res.prior],
                                    resDt = {min: 10000000, max: 0, aver: 0, itr: 0},
                                    av = 4;

                                if (!data || !_dt)return onError("calculate data is not avialable");
                                for (var i in _dt) {
                                    var z = -1;
                                    _ls.forEach(function (o) {
                                        if (z > 0)return;
                                        for (var k = 0; k < o.length; k++) {
                                            if (o[k]['l'] == [i]) {
                                                z = 1;
                                                if (!isNaN(_dt[i])) {
                                                    o[k]['val'] = (+_dt[i]).toFixed(av) + " D";
                                                    resDt.aver += _dt[i];
                                                    resDt.itr++;
                                                    resDt.min = resDt.min > _dt[i] ? _dt[i] : resDt.min;
                                                    resDt.max = resDt.max < _dt[i] ? _dt[i] : resDt.max;
                                                }
                                                break;
                                            }
                                        }
                                    });
                                }
                                _d.res.middle[0]['val'] = resDt.itr ? (resDt.aver / resDt.itr).toFixed(av) : '--';
                                _d.res.middle[1]['val'] = resDt.min != 10000000 ? (resDt.min).toFixed(av) : '--';
                                _d.res.middle[2]['val'] = (resDt.max).toFixed(av);
                                $parent.utils.preload();
                            }
                        });
                    }, 500);
                }
            },
            reset: function () {
                //if (webglEl)webglEl.build.clear();

                $scope.data._outputs={
                    postVersionS: "Version 5.2.1 or later",
                        optDevice_Index: '1.3375',
                        Eyes: "Right (OD)", Pics: "Myopic",optDevice_Indexs:kindex
                };
                $scope.data.pre_lasik = {};
                $scope.data.post_lasik = {postVersionS: "Version 5.2.1 or later"};
                $scope.data.optical = {optDevice_Index: '1.3375'};
                $scope.data.group_ = {Eyes: "Right (OD)", Pics: "Myopic"};
                $scope.isCalc = false;
                jQuery('#iolData').fadeOut();
            },
            onInput: function (fName, out, form) {
                var _d = $scope.data,
                    val = _d[out][fName];
                switch (fName) {
                    case fields[31]:
                    {
                        if (val) {
                            _d[out][fields[32]] = parseFloat((val * 0.5663 - 65.6).toFixed(10));
                            _d[out][fields[33]] = parseFloat(((val * 0.662 - 77.469) / 1000).toFixed(10));
                        }
                        break;
                    }
                    case "Pics":
                    {
                        $scope.data.curList = val == calcSelect[2] ? "nic" : "preonic";
                        setTimeout(function () {
                            if (buildPostForm instanceof Function)buildPostForm();
                            //    console.log(callback);
                            //form.$setPristine();
                            $scope.$apply();
                        }, 500);
                        break;
                    }
                }
            },
            openInfoData: function (html) {
                if (!html)return;
                $mdDialog.show({
                    clickOutsideToClose: true,
                    scope: $scope,        // use parent scope in template
                    preserveScope: true,  // do not forget this if use parent scope
                    template: '<md-dialog aria-label="LADAS SUPER FORMULA">' +
                    '  <md-dialog-content style="padding:10px ">' +
                    html +
                    '  </md-dialog-content>' +
                    '  <md-dialog-actions>' +
                    '    <md-button ng-click="closeDialog()" class="md-primary">' +
                    '      Close' +
                    '    </md-button>' +
                    '  </md-dialog-actions>' +
                    '</md-dialog>',
                    controller: function DialogController($scope, $mdDialog, $timeout) {
                        $scope.closeDialog = function () {
                            //console.log($scope.data);
                            $mdDialog.hide();
                            //onFinish();
                        }
                        $timeout(function () {
                            //$compile(html)($scope);
                        }, 1500);

                    },
                    onComplete: function () {
                        //$parent.utils.preload();
                        //console.log($scope.data);
                        //jQuery('.preloader-text').fadeOut();
                    }
                });
            }
        };
        var calcSubmit = "calcFormSubmit",
            notReq = "none",
            kindex=['1.3375', '1.332', "Other"],
            fields = [
                "preRfrctn",
                "preSph",
                "preCyl",
                "preVertex",
                "preK1",
                "preK2",

                "postSph",
                "postCyl_D",
                "postVertex",
                "postEyeSysEffRP",
                "postTomey_ACCP",
                "postGalilei_TCP",
                "postVersion",
                "postVersionS",
                "postAtlasZone",
                "postPentacam_zone",
                "postAtlas_ring_fst",
                "postAtlas_ring_scd",
                "postAtlas_ring_thd",
                "postAtlas_ring_fhd",
                "postNet_Power",
                "postPosterior_Power",
                "postCentral_Power",

                "optK1",
                "optK2",
                "optinput_KIndex",
                "optDevice_Index",
                "optAL",
                "optACD",
                "optLens_Thick",
                "optWTW",
                "optA_const",
                "optSF",
                "optHaigis_a_f",
                "optHaigis_a_s",
                "optHaigis_a_t",

                "octEffrp",
                "octACP",
                "octAtlf",
                "octAtls",
                "octAtlt",
                "octAtlfth",
                "octKm",
                "octCT_MIN",
                "octNCP",
                "octPp",
                "octCCT",
                "octRef"
            ],
            calcSelect = ["Myopic", 'Hyperopic', 'RK'],
            _pre_lasik = [
                {t: "number", n: fields[0], l: "Target Ref (D)", min: -10, max: 10, r: calcSubmit},
                {t: "number", n: fields[1], l: "Sph(D)", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[2], l: "Cyl(D)*", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[3], l: "Vertex (If empty, 12.5 mm is used)", min: 0, max: 130, r: notReq},
                {t: "number", n: fields[4], l: "K1(D)", min: 35, max: 55, r: notReq},
                {t: "number", n: fields[5], l: "K2(D)", min: 35, max: 55, r: notReq}
            ],
            post_lasik = [
                {t: "number", n: fields[6], l: "Sph(D)", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[7], l: "Cyl(D)*", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[8], l: "Vertex (If empty, 12.5 mm is used)", min: 0, max: 130, r: notReq},
                {t: "number", n: fields[9], l: "EyeSys EffRP", min: 0, max: 130, r: calcSubmit},
                {
                    t: "number",
                    n: fields[10],
                    l: "Tomey ACCP#Nidek ACP/APP",
                    min: 0,
                    max: 130,
                    r: calcSubmit,
                    h: calcSelect[1]
                },
                {t: "number", n: fields[11], l: "Galilei TCP", min: 0, max: 130, r: notReq, h: calcSelect[1]},
                {
                    t: "number",
                    n: fields[12],
                    l: "Version",
                    h: calcSelect[1],
                    s: ["Version 5.2.1 or later", "Version 5.2 or earlier"],
                    o: {n: fields[13]}
                },
                {
                    t: "number",
                    n: fields[14],
                    l: "Atlas 9000 4mm zone",
                    min: 0,
                    max: 130,
                    r: calcSubmit,
                    h: calcSelect[1]
                },
                {
                    t: "number",
                    n: fields[15],
                    l: "Pentacam TNP_Apex_4.0 mm Zone",
                    min: 0,
                    max: 130,
                    r: notReq,
                    h: calcSelect[1]
                },
                {t: "number", n: fields[16], l: "Atlas Ring Value 0mm", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[17], l: "Atlas Ring Value 1mm", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[18], l: "Atlas Ring Value 2mm", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[19], l: "Atlas Ring Value 3mm", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[20], l: "Net Corneal Power", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[21], l: "Posterior Corneal Power", min: -10, max: 10, r: calcSubmit},
                {t: "number", n: fields[22], l: "Central Corneal Thickness", min: -1000, max: 1000, r: calcSubmit},
            ],
            optical = [
                {t: "number", n: fields[23], l: "K1(D)", min: 35, max: 55, r: calcSubmit},
                {t: "number", n: fields[24], l: "K2(D)", min: 35, max: 55, r: calcSubmit},
                {
                    t: "number",
                    n: fields[25],
                    l: "Device Keratometric Index (n)",
                    min: 0,
                    max: 2,
                    r: notReq,
                    s: kindex,
                    o: {n: fields[26], min: "none", max: 2}
                },
                {t: "number", n: fields[27], l: "AL(mm)", min: 15, max: 35, r: calcSubmit},
                {t: "number", n: fields[28], l: "ACD(mm)", min: 0, max: 5, r: calcSubmit},
                //{t: "number", n: fields[29], l: "Lens Thick (mm)", min: 0, max: 130, r: notReq},
                //{t: "number", n: fields[30], l: "WTW (mm)", min: 0, max: 130, r: notReq},
                {t: "number", n: fields[31], l: "A-const(SRK/T)", min: 110, max: 120, r: notReq},
                {t: "number", n: fields[32], l: "SF(Holladay1)", min: -10, max: 10, r: calcSubmit},
                {

                    t: "number",
                    n: fields[33],
                    l: "Haigis a0 (If empty, converted value is used)",
                    min: -1,
                    max: 1,
                    r: calcSubmit
                    , h: calcSelect[2]
                },
                {
                    t: "number",
                    n: fields[34],
                    l: "Haigis a1 (If empty, 0.4 is used)",
                    min: 0,
                    max: 130,
                    r: notReq,
                    h: calcSelect[2]
                },
                {
                    t: "number",
                    n: fields[35],
                    l: "Haigis a2 (If empty, 0.1 is used)",
                    min: 0,
                    max: 130,
                    r: notReq,
                    h: calcSelect[2]
                },
            ],
            group_ = [
                //{t: "text", n: "ptnName", l: "Patient Name", r: calcSubmit},
                //{t: "text", n: "ptnId", l: "Patient ID", r: calcSubmit},
                {t: "number", n: "Pic", l: "Calculator Type", s: calcSelect, o: {n: "Pics"}},
                {t: "number", n: "Eye", l: "Eye", s: ["Right (OD)", "Left (OS)"], o: {n: "Eyes"}},

            ],
            oct = [
                {t: "number", n: fields[47], l: "Target Ref (D)", min: -10, max: 10, r: calcSubmit},
                {t: "number", n: fields[36], l: "EyeSys EffRP", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[37], l: "Average Central Power", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[38], l: "Atlas  1mm", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[39], l: "Atlas  2mm", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[40], l: "Atlas  3mm", min: 0, max: 130, r: calcSubmit},
                {t: "number", n: fields[41], l: "Atlas  4mm", min: 0, max: 130, r: calcSubmit},
                {
                    t: "number",
                    n: fields[42],
                    l: "Pentacam ",
                    link: "PWR_SF_Pupil_4.0 mm Zone**",
                    min: 0,
                    max: 130,
                    r: calcSubmit,
                    link_val: '<img src="assets/img/Pentacam image.bmp" style="width:450px"/> '
                },
                {t: "number", n: fields[43], l: "CT_MIN** ", min: 400, max: 1000, r: calcSubmit},
                {
                    t: "number",
                    n: fields[44],
                    l: "OCT (RTVue or Avanti XR)   Net Corneal Power",
                    min: 0,
                    max: 130,
                    r: calcSubmit
                },
                {t: "number", n: fields[45], l: "Posterior Corneal Power", min: -10, max: 10, r: calcSubmit},
                {t: "number", n: fields[46], l: "Central Corneal Thickness", min: -1000, max: 1000, r: calcSubmit},

            ],
            lists=[
                [
                    {name:"Basic",arr:group_}

                ],
                [
                    {name:'Refraction*',arr:[_pre_lasik[0],_pre_lasik[1],_pre_lasik[2],_pre_lasik[3]]},
                    {name:'Keratometry',arr:[_pre_lasik[4],_pre_lasik[5]]},
                ],
                [
                    {name:'Refraction*',arr:[post_lasik[0],post_lasik[1],post_lasik[2]]},
                    {name:'Topography',arr:[post_lasik[3],post_lasik[4],post_lasik[5],post_lasik[6]]},
                    {name:'Atlas Zone value',arr:[post_lasik[7],post_lasik[8]]},
                    {name:'Atlas Ring value	',arr:[post_lasik[9],post_lasik[10],post_lasik[11],post_lasik[12]]},
                    {name:'OCT (RTVue or Avanti XR)',arr:[post_lasik[13],post_lasik[14],post_lasik[15]]}
                ],
                [
                    {name:'Ks**',arr:[optical[0],optical[1],optical[2],optical[3],optical[4]]},
                    {name:'Lens Constants**',arr:[optical[5],optical[6],optical[7],optical[8],optical[9]]},
                ],
                [
                    {name:'EyeSys',arr:[oct[0],oct[1],oct[2]]},
                    {name:'Atlas',arr:[oct[3],oct[4],oct[5],oct[6]]},
                    {name:'Pentacam',arr:[oct[7],oct[8]]},
                    {name:'OCT (RTVue or Avanti XR)',arr:[oct[9],oct[10],oct[11]]},
                ]
            ];

        $scope.data = {
            name:"PostCalcCntrl",
            _inpF: fields,
            calcFormSubmit: true,
            none: false,
            listInp: {
                preonic: [

                    {ints: lists[0], name: "group_", header: "Basic Info"}
                    ,
                    {ints: lists[1], name: "pre_lasik", header: "Pre-LASIK / PRK  Data"}
                    ,
                    {ints: lists[2], name: "post_lasik", header: "Post-LASIK / PRK Data" }
                    ,
                    {ints: lists[3], name: "optical", header: "Optical (IOLMaster / Lenstar) / Ultrasound Biometric Data"}
                ],
                nic: [
                    {ints: lists[0], name: "group_", header: "Basic Info:"},
                    {ints: lists[4], name: "oct_", header: "Topographic / OCT Data:"},
                    {ints: lists[3], name: "optical", header: "Optical (IOLMaster / Lenstar) / Ultrasound Biometric Data"}

                ]
            },
            curList: "preonic",
            pre: _pre_lasik,
            post: post_lasik,
            optic: optical,
            oct: oct,
            userAccept: false,
            eyeActive: null,
            userData: {},
            _outputs:{
                postVersionS: "Version 5.2.1 or later",
                optDevice_Index: kindex[0],
                Eyes: "Right (OD)", Pics: "Myopic",optDevice_Indexs:kindex
            },
            file:{old:[
                {
                    n: 'AxialData',
                    f: [{xml: 'Acd', or: fields[28]}, {xml: 'Al', or: fields[27]},
                        {xml: 'Cct', or: fields[46]}]
                },
                {
                    n: 'Keratometry',
                    f: [ {xml: 'SteepK', or: fields[23]},
                        {xml: 'FlatK', or: fields[24]},
                        {xml: "RefractiveIndex", or: fields[26]}]
                }
            ],_new:[
                {
                    f: [
                        {xml: 'K1', or: fields[23]},
                        {xml: 'n', or: fields[26]},
                        {xml: 'K2', or: fields[24]},
                        {xml: 'CCT', or: fields[46]},
                        {xml: 'AL', or: fields[27]},
                        {xml: 'K1Axis', or: fields[31]}
                    ]
                }
            ]},
            _fields:fields,
            res: {
                mr: [
                    {
                        name: "Adjusted EffRP",
                        sub: 1,
                        l: "AdjEffRPDKHOL",
                        link_val: "#",
                        val: "--"
                    },
                    {
                        name: "Adjusted Atlas 9000 (4mm zone)",
                        sub: 2,
                        l: "AdjAtl9000",
                        val: "--",
                        link_val: ADJUSTED_ATLAS_9000
                    },
                    {
                        name: "Adjusted Atlas Ring Values",
                        sub: 1,
                        l: "AARVDKHOL",
                        val: "--",
                        link_val: ADJUSTED_ATLAS_RINGS
                    },
                    {
                        name: "Masket Formula",
                        val: "--",
                        l: "MASKET",
                        link_val: MASKET_FORMULA
                    },
                    {
                        name: "Modified-Masket",
                        val: "--",
                        l: "MODMASKET",
                        link_val: MODIFIED_MASKET
                    },
                    {
                        name: "Adjusted ACCP/ACP/APP",
                        sub: 1,
                        val: "--",
                        l: "AACCPDKHOL",
                        link_val: ADJUSTED_ACCP
                    },
                    /*  {
                     name: "Barrett True K",
                     sub: 5,
                     val: "--",
                     link_val:BARRETT_K
                     },*/
                ],
                prior: [
                    {
                        name: "Wang-Koch-Maloney",
                        sub: 2,
                        l: "WKM",
                        link_val: "#",
                        val: "--"
                    },
                    {
                        name: "Shammas	",
                        sub: 2,
                        l: "SHAMMAS",
                        link_val: "#",
                        val: "--"
                    },
                    {
                        name: "Haigis-L",
                        sub: 3,
                        l: "HAIGISL",
                        link_val: "#",
                        val: "--"
                    },
                    {
                        name: "Galilei",
                        sub: 1,
                        l: "GALILEI",
                        link_val: "#",
                        val: "--"
                    },
                    {
                        name: "Potvin-Hill Pentacam	",
                        sub: 2,
                        l: "PSH",
                        link_val: "#",
                        val: "--"
                    },
                    {
                        name: "OCT",
                        sub: 4,
                        l: "OCTIOL",
                        link_val: "#",
                        val: "--"
                    },
                    /* {
                     name: "Barrett True K No History",
                     sub: 5,
                     link_val: "#",
                     val: "--"
                     },*/
                ],
                rk: [
                    {
                        name: "EyeSys EffRP",
                        sub: 1,
                        l: "AdjEffRPDKHOL",
                        link_val: "#",
                        val: "--"
                    },
                    {
                        name: "Average Central Power (other)",
                        sub: 1,
                        l: "AACCPDKHOL",
                        link_val: "#",
                        val: "--"
                    },
                    {
                        name: "Atlas 1-4",
                        sub: 1,
                        l: "AARVDKHOL",
                        link_val: "#",
                        val: "--"
                    },
                    {
                        name: "Pentacam",
                        sub: 1,
                        l: "PENTACAMIOL",
                        link_val: "#",
                        val: "--"
                    },
                    {
                        name: "IOLMaster/Lenstar",
                        sub: 1,
                        l: "IOLMLENSTAR",
                        link_val: "#",
                        val: "--"
                    },
                    {
                        name: "OCT",
                        sub: 2,
                        l: "OCTIOL",
                        link_val: "#",
                        val: "--"
                    }
                ],
                middle: [
                    {
                        name: "Average IOL Power (All Available Formulas):",
                        val: "--"
                    },
                    {
                        name: "Min:",
                        val: "--"
                    },
                    {
                        name: "Max:",
                        val: "--"
                    },
                ]
            }
            , label: {
                IOL: "IOL POWER",
                web_header: "Post-Lasik Calculator"
            }
        };

        if (ses) updateInputData();
        function updateInputData() {
            $parent.utils.updateInputData($scope.data,
                ['_outputs'],
                {KI: fields[26], AC: fields[31]},kindex);

            setColorSvg(0, [{selector: '.post-calc-btn', col: "#ffffff"}, {
                selector: '.lad-imp-cont',
                col: "#3f51b5"
            }]);
            fadePreload(true);
        }
    }]);

