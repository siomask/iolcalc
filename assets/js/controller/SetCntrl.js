app.controller("SettingsCntrl", ["$scope", "$state", "request", "$mdDialog", "$timeout", "NgTableParams", "ngTableEventsChannel", function ($scope, $state, request, $mdDialog, $timeout, NgTableParams, ngTableEventsChannel) {
    var $parent = $scope.$parent;
    var fadePreload = function (a) {
        return $parent.utils.preload(!a)
    };

    $parent.containerVis = true;
    $scope.editMsgVis = true;
    $scope.curMsgEdit = {};
    $scope.inputData = {input_KIndex: '1.3375', Kindexes: ['1.3315', '1.3320', '1.3360', '1.3375', '1.3380', 'Other']};

    // for correct sorting by last_calc_sort column
    ngTableEventsChannel.onAfterDataSorted(function () {
        var sortState = $scope.loadData.usersPag._params.sorting,
            listOfUsers = $scope.loadData.users;

        if (sortState.last_calc_sort && sortState.last_calc_sort === 'asc') {
            listOfUsers.forEach(function (el) {
                if (el.last_calc_sort === 'N') el.last_calc_sort = '-';
            });
            $scope.loadData.usersPag.settings({dataset: listOfUsers});
        }
        else if (sortState.last_calc_sort && sortState.last_calc_sort === 'desc') {
            listOfUsers.forEach(function (el) {
                if (el.last_calc_sort === '-') el.last_calc_sort = 'N';
            });
            $scope.loadData.usersPag.settings({dataset: listOfUsers});
        }

        if (sortState.last_log_in_sort && sortState.last_log_in_sort === 'asc') {
            listOfUsers.forEach(function (el) {
                if (el.last_log_in_sort === 'N') el.last_log_in_sort = '-';
            });
            $scope.loadData.usersPag.settings({dataset: listOfUsers});
        }
        else if (sortState.last_log_in_sort && sortState.last_log_in_sort === 'desc') {
            listOfUsers.forEach(function (el) {
                if (el.last_log_in_sort === '-') el.last_log_in_sort = 'N';
            });
            $scope.loadData.usersPag.settings({dataset: listOfUsers});
        }

    }, $scope);

    $scope.loadData = {
        codes: [],
        users: [],
        msgs: [],
        notes: [],
        promocode: null,
        personal: {},
        curStatistic: {},
        userCounts: 99,
        useresLimit: 0,
        settingsTotalInfo: {}
    };
    $scope.tinymceOptions = ({
        selector: 'textarea',
        height: 410,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
    $scope.sortBy = function (propertyName, arr) {
        //console.log(arr);
        //arr.sorting(propertyName,'asc');
        //arr.sort(function(a,b){
        //    return a[propertyName] > b[propertyName]
        //})

        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.selected = [];
    $scope.hasActivated = false;
    $scope.hasBanned = false;
    $scope.hasUser = false;
    $scope.hasAdmin = false;
    $scope.hasSubAdmin = false;

    $scope.$watch('selected.length', function () {

        let hasActivated = false;
        let hasBanned = false;
        let newUsers = false;

        let hasUser = false;
        let hasAdmin = false;
        let hasSubAdmin = false;

        for(let i = 0; i < $scope.selected.length; i++){

            if ($scope.selected[i].usr_active === 'active') hasActivated = true;
            if ($scope.selected[i].usr_active === 'ban') hasBanned = true;
            if ($scope.selected[i].usr_active === '0') newUsers = true;
            if ($scope.selected[i].usr_type === '0') hasUser = true;
            if ($scope.selected[i].usr_type=== 'Admin') hasAdmin = true;
            if ($scope.selected[i].usr_type === 'subAdmin') hasSubAdmin = true;
        }

        $scope.hasActivated = hasActivated && !newUsers;
        $scope.hasBanned = hasBanned && !newUsers;
        $scope.hasAdmin = hasAdmin;
        $scope.hasUser = hasUser;
        $scope.hasSubAdmin = hasSubAdmin;
    });

    $scope.query = {
        order: 'name',
        limit: 5,
        page: 1
    };
    var calcs = {
        limit: 50,
        lastLoaded: -50
    };

    var day = new Date();
    day.setDate(day.getDate() - 1);
    $scope.setting = {
        selection: {
            showActions: false,
            all: null,
            selectUser: function (type, user) {
                const self = this,
                    listOfUsers = $scope.loadData.usersPag.data;
                switch (type) {
                    case 'similar':
                    {
                        if(user.isSelected){
                            $scope.selected.push(user);
                        } else {
                            $scope.selected.splice($scope.selected.indexOf(user), 1);
                        }

                        self.showActions = false;
                        for (var i = 0; i < listOfUsers.length; i++) {
                            if (listOfUsers[i].isSelected) {
                                self.showActions = true;
                                break;
                            }
                        }

                        break;
                    }
                    case 'all':
                    {
                        self.showActions = self.all;
                        listOfUsers.forEach(function (el) {
                            el.isSelected = self.all;

                            if($scope.selected.indexOf(el) === -1 && el.isSelected){
                                $scope.selected.push(el);
                            } else if($scope.selected.indexOf(el) > -1 && !el.isSelected){
                                $scope.selected.splice($scope.selected.indexOf(el), 1);
                            }

                            if (self.all) {
                                if (el.usr_active === 'ban') $scope.hasBanned = true;
                                if (el.usr_active === 'active') $scope.hasActivated = true;
                                if (el.usr_type === '0') $scope.hasUser = true;
                                if (el.usr_type === 'Admin') $scope.hasAdmin = true;
                                if (el.usr_type === 'subAdmin') $scope.hasSubAdmin = true;
                            } else {
                                $scope.selected.length = 0;
                                $scope.hasBanned = false;
                                $scope.hasActivated = false;
                                $scope.hasUser = false;
                                $scope.hasAdmin = false;
                                $scope.hasSubAdmin = false;
                            }
                        });
                        break;
                    }
                    case 'nonActivated':
                    {
                        self.showActions = self.nonActivated;

                        $scope.loadData.users.forEach(function (el) {
                            if(el.usr_active === '0') {

                                el.isSelected = self.nonActivated;

                                if($scope.selected.indexOf(el) === -1 && el.isSelected){
                                    $scope.selected.push(el);
                                } else if($scope.selected.indexOf(el) !== -1 && !el.isSelected){
                                    $scope.selected.splice($scope.selected.indexOf(el), 1);
                                }
                            }
                            if(el.isSelected) self.showActions = true;
                        });

                        break;
                    }
                }
            },
            updateUsers: function (type) {
                var curActive = '',
                    curReson,
                    updateUsers = [],
                    _type = type,
                    self = this,
                    _allUsers = $scope.loadData.users,
                    listOfusers = $scope.loadData.usersPag.data;


                if (type === "activateUserS") {
                    $scope.selected.forEach(function (selectedUser) {
                        if (selectedUser.usr_active === '0' || selectedUser.usr_active === 'ban') {
                            updateUsers.push(selectedUser);
                        }
                    });
                } else if(type === "banUserS") {

                    $scope.selected.forEach(function (selectedUser) {
                        if (selectedUser.usr_active === 'active') {
                            updateUsers.push(selectedUser);
                        }
                    });

                } else {
                    $scope.selected.forEach(function (selectedUser) {
                        updateUsers.push(selectedUser);
                    });
                }

                switch (type) {
                    case 'dropUserS':
                    {
                        curReson = "We've noticed some bad request frome your account";
                        curActive = 'delete user(s)';
                        break;
                    }
                    case 'setAdminS':
                    {
                        curReson = "Congratulations, we've upgrade your account ";
                        curActive = 'make user(s) an admin(s)';
                        break;
                    }
                    case 'setUserS':
                    {
                        curReson = "Sorry, but we need to limit your access";
                        curActive = 'make  user(s) an user(s)';
                        break;
                    }
                    case 'activateUserS':
                    {
                        curReson = "";
                        curActive = 'active user(s)';
                        break;

                    }
                    case 'banUserS':
                    {
                        curReson = "We've noticed some bad request frome your account";
                        curActive = 'inactive user(s)';
                        break;

                    }
                    case 'abilityPostData':
                    {
                        curReson = "Congratulations, we've upgrade your account";
                        curActive = ' allow enter post-data for user(s)';
                        break;

                    }
                    case 'noAbilityPostData':
                    {
                        curReson = "We've noticed some bad request frome your account";
                        curActive = ' not allow enter post-data for user(s)';
                        break;

                    }

                }
                var delUser = type == 'dropUserS',
                    actUser = type == 'activateUserS',
                    banUser = type == 'banUserS',
                    req = function (reason) {
                        request.add({
                            users: updateUsers.map(function (el) {
                                return {usr_email: el.usr_email, usr_name: el.usr_name}
                            }),
                            type: type,
                            reason: reason
                        }, 'updateUsers').then(function (data) {
                            //console.log(data,updateUser);
                            if (data['error']) {
                                $mdDialog.show($mdDialog.alert({
                                    title: 'Error',
                                    textContent: data['error'],
                                    ok: 'Close'
                                })).finally(function () {
                                });
                            } else {
                                for (var i = 0; i < listOfusers.length; i++) {
                                    var _user = listOfusers[i];
                                    switch (type) {
                                        case 'dropUserS':
                                        {
                                            for (var j = 0; j < _allUsers.length; j++) {
                                                if (_allUsers[j].id == _user.id) {
                                                    _allUsers.splice(i--, 1);
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                        case 'setAdminS':
                                        {
                                            _user.usr_type = 'Admin';
                                            break;
                                        }
                                        case 'setUserS':
                                        {
                                            _user.usr_type = '0';
                                            break;
                                        }
                                        case 'activateUserS':
                                        {
                                            updateUsers.forEach(function (updatedUser) {
                                                updatedUser.usr_active = 'active';
                                            });
                                            break;
                                        }
                                        case 'banUserS':
                                        {
                                            updateUsers.forEach(function (updatedUser) {
                                                updatedUser.usr_active = 'ban';
                                            });
                                            break;

                                        }
                                        case 'abilityPostData':
                                        {
                                            _user.usr_type = 'subAdmin';
                                            break;

                                        }
                                        case 'noAbilityPostData':
                                        {
                                            _user.usr_type = '0';
                                            break;

                                        }

                                    }
                                }
                                $mdDialog.show($mdDialog.alert({
                                    title: 'Attention',
                                    textContent: 'User(s) was update',
                                    ok: 'Close'
                                })).finally(function () {
                                    if (delUser || actUser || banUser) $scope.loadData.usersPag = new NgTableParams({sorting: {created: 'desc'}}, {dataset: $scope.loadData.users});
                                });
                            }
                        });
                    };


                $mdDialog.show($mdDialog.confirm()
                        .title('Attention')
                        .textContent("Are you sure to  " + curActive + " ?")
                        .ok("OK")
                        .cancel("Cancel")
                        .openFrom("#left")
                        .closeTo(angular.element(document.querySelector('#right')))
                ).then(function () {
                        //setTimeout(function(){
                        //    $scope.dialog.result = curReson;
                        //},100) ;
                        if (_type == 'activateUserS') {
                            req('');
                        } else {
                            $mdDialog.show($mdDialog.prompt()
                                    .title('Please, enter your reason to ' + curActive)
                                    .result(curReson)
                                    .placeholder('reason')
                                    .ariaLabel('Dog name')
                                    .ok('Update')
                                    .cancel('Don\'t want to confirm a reason')
                            ).then(function (result) {
                                    req((result ? result : ''));
                                }, function () {
                                    req('');
                                });
                        }
                    }, function () {
                    });


            },
        },

        load: {
            startFromDate: day,
            action: function (type, usr_id) {
                var self = this;
                request.get({
                    type: type,
                    id_user: usr_id,
                    start_from: this.startFromDate
                }, 'getStatistics').then(function (data) {
                    if (data.error || !data.data || !data.data.length) {
                        $mdDialog.show($mdDialog.alert({
                            title: 'Error',
                            textContent: data['error'] || 'User doesn`t have any statistic yet',
                            ok: 'Close'
                        })).finally(function () {
                        });
                    } else {
                        var _type = '';
                        switch (type) {
                            case 'lastLogIn':
                            {
                                _type = 'Last User Log IN';
                                break;
                            }
                            case 'lastCalc':
                            {
                                _type = 'Last User Calc';
                                break;
                            }
                            case 'lastLogIns':
                            {
                                _type = 'Last User Log INs';
                                break;
                            }
                            case 'lastCalcs':
                            {
                                _type = 'Last User Calcs';
                                break;
                            }
                        }
                        self._type = _type;
                        self._data = data.data;
                        showStatistic();
                    }
                })
            }
        },
        createPromo: function () {
            $scope.submitted = true;
            var name = $scope.loadData.promocode;
            if (!name) {
                return
            }
            name = $scope.loadData.promocode = name.split(" ").join("");
            var c = {name: name, id_code: -1, user: $parent.session.user};

            request.add({
                code: c
            }, 'savePromoCode').then(function (data) {
                if (data['error']) {
                    alert(data['error']);
                } else {
                    $scope.submitted = true;
                    c.id_code = data['id_code'];
                    $scope.loadData.codes.push(c);
                }
            });
        },
        editCurCode: function (code) {
            if (code.name) {
                code.disabled = !code.disabled;
                if (code.disabled) {
                    var updData = {};
                    angular.copy(code, updData);
                    updData.user = $parent.session.user;
                    delete updData['disabled'];
                    request.add({code: updData}, 'editCurCode').then(function (data) {
                        if (data['error']) {
                            code.disabled = false;
                            console.log(data);
                            alert(data['error']);
                        } else {
                            alert(data['success']);
                        }
                    });
                }
            }
        },
        dropCurCode: function (code, id) {
            if (code) {
                if (confirm("Are you sure to delete " + code.name + " ?")) {
                    var updData = {};
                    angular.copy(code, updData);
                    updData.user = $parent.session.user;
                    request.add({code: updData}, 'dropCurCode').then(function (data) {
                        if (data['error']) {
                            alert(data['error']);
                        } else {
                            $scope.loadData.codes.splice(id, 1);
                        }
                    });
                }
            }
        },
        updateUser: function (user, type, id) {
            var curActive,
                curReson,
                updateUser = {},
                _type = type;
            angular.copy(user, updateUser);
            switch (type) {
                case 'dropUser':
                {
                    curReson = "We've noticed some bad request frome your account";
                    curActive = 'delete ' + user['usr_email'];
                    break;
                }
                case 'setAdmin':
                {
                    curReson = "Congratulations, we've upgrade your account ";
                    updateUser.usr_type = 'Admin';
                    curActive = 'make ' + user['usr_email'] + ' an admin';
                    break;
                }
                case 'setUser':
                {
                    curReson = "Sorry, but we need to limit your access";
                    updateUser.usr_type = '0';
                    curActive = 'make  ' + user['usr_email'] + ' an user';
                    type = 'setAdmin';
                    break;
                }
                case 'activateUser':
                {
                    curReson = "";
                    updateUser.usr_active = 'active';
                    curActive = 'active ' + user['usr_email'];
                    break;

                }
                case 'banUser':
                {
                    curReson = "We've noticed some bad request frome your account";
                    updateUser.usr_active = '0';
                    curActive = 'inactive ' + user['usr_email'];
                    type = 'activateUser';
                    break;

                }
                case 'upgradeUser':
                {
                    curReson = "Congratulations, we've upgrade your account";
                    curActive = ' allow enter post-data ' + user['usr_email'];
                    if (updateUser.usr_type == 'subAdmin') {
                        curReson = "We've noticed some bad request frome your account";
                        curActive = ' not allow enter post-data ' + user['usr_email'];
                    }

                    break;

                }
                /*case 'cancelResetPsw':
                 {
                 curReson = "We've noticed some bad request frome your account";
                 updateUser.usr_active = '0';
                 curActive = 'inactive ' + user['usr_email'];
                 type = 'activateUser';
                 break;

                 } */

            }
            var delUser = type == 'dropUser',
                req = function (reason) {
                    request.add({
                        user: updateUser,
                        admin: $parent.session.user,
                        reason: reason
                    }, type).then(function (data) {
                        //console.log(data,updateUser);
                        if (data['error']) {
                            $mdDialog.show($mdDialog.alert({
                                title: 'Error',
                                textContent: data['error'],
                                ok: 'Close'
                            })).finally(function () {
                            });
                        } else {
                            if (delUser) {
                                angular.forEach($scope.loadData.users, function (a, k) {
                                    if (a.usr_email == updateUser.usr_email) {
                                        $scope.loadData.users.splice(k, 1);
                                        return false;
                                    }
                                });
                                if (user && user.id_user == $parent.session.user.id_user) {
                                    $parent.session.user = undefined;
                                    $state.go('home_en');
                                }
                            } else {
                                if (data['user'])updateUser = data['user'];
                                angular.forEach($scope.loadData.users, function (a, k) {
                                    if (a.usr_email == updateUser.usr_email) {
                                        $scope.loadData.users.splice(k, 1, updateUser);
                                        return false;
                                    }
                                });
                            }
                            $mdDialog.show($mdDialog.alert({
                                title: 'Attention',
                                textContent: 'User was update',
                                ok: 'Close'
                            })).finally(function () {
                                $scope.loadData.usersPag = new NgTableParams({sorting: {created: 'desc'}}, {dataset: $scope.loadData.users});
                            });

                        }
                    });
                };


            $mdDialog.show($mdDialog.confirm()
                    .title('Attention')
                    .textContent("Are you sure to  " + curActive + " ?")
                    .ok("OK")
                    .cancel("Cancel")
                    .openFrom("#left")
                    .closeTo(angular.element(document.querySelector('#right')))
            ).then(function () {
                    //setTimeout(function(){
                    //    $scope.dialog.result = curReson;
                    //},100) ;
                    if (_type == 'activateUser') {
                        req('');
                    } else {
                        $mdDialog.show($mdDialog.prompt()
                                .title('Please, enter your reason to ' + curActive)
                                .result(curReson)
                                .placeholder('reason')
                                .ariaLabel('Dog name')
                                .ok('Update')
                                .cancel('Don\'t want to confirm a reason')
                        ).then(function (result) {
                                req((result ? result : ''));
                            }, function () {
                                req('');
                            });
                    }
                }, function () {
                })

        },
        updateDefVal: function () {
            var _i = $scope.inputData,
                uploadData = {
                    calc_input: {
                        Rfrctn: _i.Rfrctn,
                        K1: _i.K1,
                        K2: _i.K2,
                        axialLength: _i.axialLength,
                        ACD: _i.ACD,
                        AConstant: _i.AConstant,
                        KIndex: (_i.input_KIndex != "Other" ? _i.input_KIndex : (_i.KIndex ? _i.KIndex : null) ),
                        id_val: _i.id_val,
                        id_user: _i.id_val
                    },
                    user: {
                        usr_email: $parent.session.user.usr_email,
                        id_user: $parent.session.user.id_user
                    }
                };
            request.add(uploadData, 'updateDefVal').then(function (data) {
                //console.log(data);

                if (data['error']) {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Error',
                        textContent: data['error'],
                        ok: 'Close'
                    })).finally(function () {
                    });
                } else {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Attention',
                        textContent: data['success'],
                        ok: 'Close'
                    })).finally(function () {
                        setTimeout(function () {
                            $scope.$apply(function () {
                                updateInputdefVal(true, data['val'], uploadData.calc_input);
                                // var  _v  = uploadData.calc_input.KIndex;
                                // $scope.inputData.KIndex = _i.input_KIndex > 0 ? null:(_v || _v === 0?parseFloat(_v):null);
                                //$scope.inputData.Rfrctn = uploadData.calc_input.Rfrctn;
                            })
                        }, 100);

                    });
                }

            });
        },
        updateUserInfo: function () {
            var userUpload = $parent.session.user,
                req = function () {
                    var _f = userUpload.firstName ? userUpload.firstName.replace(/ /g, '') : "",
                        _s = userUpload.secondName ? userUpload.secondName.replace(/ /g, '') : "";
                    userUpload.usr_name = _f + " " + _s;//userUpload.secondName ? userUpload.usr_name.split(" ")[0] + " " + userUpload.secondName : userUpload.usr_name;
                    request.add(userUpload, 'updateUserInfo').then(function (data) {
                        //console.log(data);
                        if (data['error']) {
                            $mdDialog.show($mdDialog.alert({
                                title: 'Error',
                                textContent: data['error'],
                                ok: 'Close'
                            })).finally(function () {
                            });
                        } else {
                            $mdDialog.show($mdDialog.alert({
                                title: 'Attention',
                                textContent: data['success'],
                                ok: 'Close'
                            })).finally(function () {
                                if (data['text']) {
                                    $mdDialog.show($mdDialog.alert({
                                        title: 'Attention',
                                        textContent: data['text'],
                                        ok: 'Close'
                                    })).finally(function () {
                                        userUpload.usr_old_psw_rep = userUpload.usr_psw = userUpload.usr_psw_rep = null
                                    });
                                }
                                ;
                            });
                        }
                    });
                };

            if (userUpload.usr_psw) {
                $mdDialog.show($mdDialog.confirm()
                        .title('Atention')
                        .textContent('Are you sure you want to update your password?')
                        .ok("OK")
                        .cancel("Cancel")
                        .openFrom("#left")
                        .closeTo(angular.element(document.querySelector('#right')))
                ).then(function () {
                        req();
                    })
            } else {
                req()
            }
        },
        updateMsg: function (msg, type, id) {
            var uploadObj = $scope.curMsgEdit;

            if (
                !uploadObj.fromeEmail || !uploadObj.fromeEmail.trim().length || !uploadObj.fromeName || !uploadObj.fromeName.trim().length || !uploadObj.subject || !uploadObj.subject.trim().length
            ) {
                $mdDialog.show($mdDialog.alert({
                    title: 'Error',
                    textContent: 'you forgot something!!!',
                    ok: 'Close'
                }));
                return false;
            }
            var str = jQuery("iframe").contents().find("body").html();
            //console.log(CKEDITOR.instances.msg.getData());

            var //str = $scope.tinymceModel + "",
                newStrng;
            if (str.split('\\"').length > 1) {
                newStrng = str;

            } else {
                newStrng = str.split('"').join('\\"');
                ;//CKEDITOR.instances.msg.getData();//str.replace(/"/,'\"');
            }
            $scope.curMsgEdit.text = uploadObj.text = newStrng;

            request.add({msg: uploadObj, user: $parent.session.user}, 'editMsg').then(function (data) {
                //console.log(data,uploadObj);
                if (data['error']) {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Error',
                        textContent: data['error'],
                        ok: 'Close'
                    }));
                } else {
                    if (type == 'dropMsg') {
                        $scope.loadData.msgs.splice(id, 1);
                    }
                    angular.forEach($scope.loadData.msgs, function (o) {
                        if (o.id == uploadObj.id) {
                            o.text = str;
                            return false;
                        }
                    });
                    //console.log(data,uploadObj);
                    $mdDialog.show($mdDialog.alert({
                        title: 'Attention',
                        textContent: data['success'],
                        ok: 'Close'
                    }));
                }
            });
        },
        userConfirm: function (note, type, id) {
            note.confirm = type == 'acceptNote';

            if (note.confirm && !note.selectedCode) return;
            request.add({
                id_code: (note.selectedCode ? note.selectedCode.id_code : -1),
                id_note: note.id_note,
                user_email: note.user_email,
                user_name: note.user_name,
                note_status: (note.confirm ? 'has_confirmed' : 'has_declined'),
                user: $parent.session.user
            }, type).then(function (data) {
                console.log(data, note);
                if (data['error']) {
                    alert(data['error']);
                } else {
                    $scope.loadData.notes.splice(id, 1);
                    alert(data['success']);
                }
            });
        },
        setCurrentMsgToEdit: function (msg) {
            setTimeout(function () {
                $scope.$apply(function () {
                    $scope.tinymceModel = msg ? (msg.text) : $scope.tinymceModel;
                    $scope.curMsgEdit = msg ? msg : $scope.curMsgEdit;
                    $scope.editMsgVis = !msg;
                })
            }, 100);
        },
        loadData: function () {
            var _self = this;
            //$scope.loadData.IsAllUsers = true;
            if ($parent.loadData && $parent.loadData.isloaded) {
                $scope.loadData = $parent.loadData;
            } else {
                fadePreload();
                request.get({
                    user: $parent.session.user,
                    limit: $scope.loadData.useresLimit
                }, 'getSetData').then(function (data) {
                    // console.log(data);
                    if (data['error']) {
                        $mdDialog.show($mdDialog.alert({
                            title: 'Error',
                            textContent: data['error'],
                            ok: 'Close'
                        })).finally(function () {
                            fadePreload(true);
                        });
                    } else if (data['data']) {
                        $scope.loadData.useresLimit += $scope.loadData.userCounts
                        // $scope.loadData.codes = data['data']['codes'];
                        $scope.loadData.users = data['data']['users'];
                        if ($scope.loadData.users) {
                            updateTime($scope.loadData.users);
                            $scope.loadData.usersPag = new NgTableParams({sorting: {created: 'desc'}}, {dataset: $scope.loadData.users});
                        }
                        $scope.loadData.msgs = data['data']['msgs'];
                        // $scope.loadData.notes = data['data']['notes'];
                        $scope.loadData.info = data['data']['info'];
                        $scope.loadData.backFiles = data['data']['backFiles'];
                        $scope.loadData.isloaded = true;
                        $scope.getSettingsTotalInfo();
                        $parent.loadData = $scope.loadData;
                        // angular.forEach(  data['data']['backFiles'],function(o,k){
                        // var a = moment.tz(o.usr_solt).tz("EST").format('Z');;
                        //     o.created = o.created +" EST("+a+"h)";
                        //  if(typeof o == "string"){
                        //      data['data']['backFiles'].splice(k,1);
                        //  }

                        // });
                        for (var fiels in data['data']['backFiles']) {
                            if (typeof data['data']['backFiles'][fiels] == "string")delete data['data']['backFiles'][fiels]
                        }
                        _self.getUsers();
                        fadePreload(1);
                    }
                });
            }

        },
        search: function (object) {
          console.log('searchData', this.searchData);
            var search = this.searchData.toLowerCase(),
                matches = [],
                searchIn,
                keys, keysSize,
                out;
            switch (object) {
                case 'user':
                {
                    searchIn = $parent.loadData.users;
                    keys = Object.keys(searchIn[0]);
                    keysSize = keys.length;
                    out = 'usersPag';
                    break;
                }
            }

            if (search == '' || !search) {
                matches = searchIn;
            } else {
                for (var i = 0; i < searchIn.length; i++) {
                    var curSearch = searchIn[i],
                        isNotMatch = true;
                        console.log(curSearch);
                    for (var itemK = 0; itemK < keys.length; itemK++) {
                        var keyVal;

                        //curSearch[keys[itemK]] ? keyVal = curSearch[keys[itemK]].toLowerCase() : keyVal = null;


                        if(typeof curSearch[keys[itemK]] == 'string' && curSearch[keys[itemK]]){

                            keyVal = curSearch[keys[itemK]].toLowerCase();

                        }else{
                            keyVal = null;
                        }

                        if (keyVal) {
                            if(typeof keyVal == 'string' && keyVal.match(search) ) {
                                matches.push(curSearch);
                                isNotMatch = false;
                                break
                            }
                        }
                    }
                    if (isNotMatch && (i + '').match(search)) {
                        matches.push(curSearch);
                    }
                }
            }

            console.log('matches', matches);
            var res = new NgTableParams({sorting: {created: 'desc'}}, {dataset: matches});

            switch (object) {
                case 'user':
                {
                    $scope.loadData.usersPag = res;
                    break;
                }
            }
        },
        loadStatistic: function (user) {
            if (!user || !user['id_user']) {
                $mdDialog.show($mdDialog.alert({
                    title: 'Error',
                    textContent: 'user not selected',
                    ok: 'Close'
                })).finally(function () {
                });
            } else {

                if (false/*user.statistic*/) {
                    $scope.loadData.curStatistic = user.statistic;

                    if (user.statistic['isEmpty']) {
                        warUserHasNoStat();
                    } else {
                        $timeout(function () {
                            $scope.$apply(function () {
                                $scope.statistics = {};
                            });
                        }, 500);
                    }

                } else {
                    fadePreload();
                    request.add({user: user}, 'loadStatistic').then(function (data) {
                        if (data["error"]) {
                            $mdDialog.show($mdDialog.alert({
                                title: 'Error',
                                textContent: data["error"],
                                ok: 'Close'
                            })).finally(function () {
                                //fadePreload(true);
                            });
                        } else {
                            $scope.loadData.curStatistic = data['statistic'];
                            user.statistic = data['statistic'] ? data['statistic'] : {isEmpty: true};

                            if (user.statistic['isEmpty']) {
                                warUserHasNoStat();
                            } else {
                                $timeout(function () {
                                    $scope.$apply(function () {
                                        $scope.statistics = {};
                                    });
                                }, 500);

                            }
                            user.statistic.usr_email = user.usr_email;


                        }
                        fadePreload(true);
                    });
                }
                function warUserHasNoStat() {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Warning',
                        textContent: 'user has not had statistic yet',
                        ok: 'Close'
                    })).finally(function () {

                    });
                }

            }

        },
        statisticBack: function () {
            $timeout(function () {
                $scope.$apply(function () {
                    $scope.statistics = null;
                });
            }, 500);
        },
        getUsers: function (allusres) {
            if ($scope.loadData.IsAllUsers ) return;
            //fadePreload();
            var users = $scope.loadData.users,
                _self = this, usersMatch = null;// document.getElementById('searchUser').value;
            //usersMatch = usersMatch && usersMatch.length ? usersMatch : null;

            var count = allusres ? allusres : $scope.loadData.useresLimit;
            request.get({
                user: $parent.session.user,
                limit: count,
                usersMatch: (usersMatch)
            }, 'getUseres').then(function (data) {
                if (data["error"]) {
                    console.log(data['error']);
                } else {
                    $scope.loadData.useresLimit += $scope.loadData.userCounts;
                    if (data['users']) {
                        if (data['users'].length) {
                            updateTime(data['users']);

                            for (var ii = 0; ii < data['users'].length; ii++) {
                                var _c = data['users'][ii],
                                    needToAdd = true;
                                //var a = moment.tz(_c.usr_solt).tz("EST").format('Z');;
                                //_c.created = _c.created +" EST("+a+"h)";


                                for (var i = 0; i < users.length; i++) {
                                    if (users[i]['usr_email'] == _c['usr_email']) {
                                        needToAdd = false;
                                        data['users'].splice(ii--, 1);
                                        break;
                                    }
                                }
                                if (needToAdd)users.push(_c);
                            }
                            if (allusres) {
                                $scope.loadData.IsAllUsers = true;
                            }
                            $scope.loadData.usersPag = new NgTableParams({sorting: {created: 'desc'}}, {dataset: users});
                            _self.getUsers();
                        } else {
                            $scope.loadData.IsAllUsers = true;
                            if (usersMatch) {
                                $mdDialog.show($mdDialog.alert({
                                    title: 'Warning',
                                    textContent: "couldn't get user by " + usersMatch,
                                    ok: 'Close'
                                })).finally(function () {

                                })
                            } else {

                                /*$mdDialog.show($mdDialog.alert({
                                 title: 'Warning',
                                 textContent: "couldn't get user more users ",
                                 ok: 'Close'
                                 })).finally(function () {

                                 })*/
                            }
                        }

                    }
                }
                fadePreload(true);
            });

        },
        backUp: function () {
            fadePreload();
            request.get({}, 'backUp').then(function (data) {
                // console.log(data);
                $mdDialog.show($mdDialog.alert({
                    title: (data["error"] ? 'Error' : "Warning"),
                    textContent: data['error'] ? data["error"] : data["success"],
                    ok: 'Close'
                })).finally(function () {
                    if (data["success"]) {
                        data["files"].forEach(function (o, k) {
                            $scope.loadData.backFiles[o['fileName']] = o;
                        });
                        // $timeout(function () {
                        //     $scope.$apply(function(){
                        //         $scope.loadData.backFiles.splice(idFile,1);
                        //     });
                        // }, 500);
                    }
                });
                fadePreload(true);
            });
        },
        deleteFile: function (file, idFile) {
            $mdDialog.show($mdDialog.confirm()
                    .title('Attention')
                    .textContent("Are you sure to  delete file " + file + " ?")
                    .ok("OK")
                    .cancel("Cancel")
                    .openFrom("#left")
                    .closeTo(angular.element(document.querySelector('#right')))
            ).then(function () {
                    var upload = {
                        user: $parent.session.user,
                        file: file
                    }
                    request.get(upload, 'deleteFile').then(function (data) {
                        // console.log(data);
                        $mdDialog.show($mdDialog.alert({
                            title: (data["error"] ? 'Error' : "Warning"),
                            textContent: data['error'] ? data["error"] : data["success"],
                            ok: 'Close'
                        })).finally(function () {
                            if (data["success"]) {
                                $timeout(function () {
                                    $scope.$apply(function () {
                                        delete $scope.loadData.backFiles[file];
                                    });
                                }, 500);
                            }
                        });
                        fadePreload(true);
                    });
                });

        },
        exportEXL: function (user) {
            var _self = this,
                exportEXL = document.getElementById('exportEXL');
            if (_self.isNotFinish)return;
            _self.isNotFinish = true;
            if (user) {
                loadCalculations(
                    {
                        next: function () {
                        },
                        onFinish: function (result) {
                            _self.curEXL = result;

                            setTimeout(function () {
                                exportEXL.click();
                                _self.curEXL = null;
                                _self.isNotFinish = false;
                            }, 1000);
                        },
                        onError: function (data) {
                            _self.isNotFinish = false;
                            $mdDialog.show($mdDialog.alert({
                                title: 'Error',
                                textContent: data.error,
                                ok: 'Close'
                            })).finally(function () {
                            });
                        },
                        req: {user: user},
                    });
            } else {
                _self.curEXL = $scope.calcso;
                setTimeout(function () {
                    exportEXL.click();
                    _self.curEXL = null;
                    _self.isNotFinish = false;
                }, 1000);
            }
        },
        uploadCalculation: function (all) {
            loadCalculations(
                {
                    next: function (calcs) {
                        $scope.calcso = calcs;
                        $scope.calcs = $parent.calculationsData = new NgTableParams({sorting: {local_created: 'desc'}}, {dataset: calcs});
                    }
                });

        },
        editCalcs: function (fieldName, obj) {

            var edit = {id_calc: obj.id_calc};
            if ('calc_input' == fieldName) {
                obj[fieldName] = "AL = " + obj[fieldName + "AL"] + ",K1 = " + obj[fieldName + "K1"] + ",K2 = " + obj[fieldName + "K2"];
            }
            edit[fieldName] = obj[fieldName];
            request.get(edit, 'editCalculations').then(function (data) {
                    console.log(data);
                }
            );
        },
        dropCalc: function (calc, i) {
            $mdDialog.show($mdDialog.confirm()
                    .title('Attention')
                    .textContent("Are you sure you want to delete this eye from your 'My Calculations' log?")
                    .ok("Yes")
                    .cancel("No")
                    .openFrom("#left")
                    .closeTo(angular.element(document.querySelector('#right')))
            ).then(function () {
                    request.get(calc, 'dropCalc').then(function (data) {
                            if (data['error']) {
                                $mdDialog.show($mdDialog.alert({
                                    title: 'Error',
                                    textContent: data['error'],
                                    ok: 'Close'
                                })).finally(function () {
                                });
                            } else {
                                //loadedList.splice(calc.item, 1);
                                $scope.calcso.splice(calc.item, 1);
                                $scope.calcso.forEach(function (el, index) {
                                    el.item = index
                                });
                                $scope.calcs.data.splice(i, 1);
                                //$scope.calcs = $parent.calculationsData = new NgTableParams({sorting: {created: 'desc'}}, {dataset: loadedList});
                            }

                        }
                    );

                }, function () {
                })

        },
        getNeuralFiles: function () {
            if ($scope.loadData.neuralList)return;
            request.get({}, 'getNeoron').then(function (data) {
                if (data["error"]) {
                    $scope.loadData.neuralList = [];
                } else {
                    $scope.loadData.neuralList = data['list'];
                }
            })
        },
        neuronFileChange: function (e) {
            if (!e.target.files.length)return;
            var formData = new FormData();
            formData.append('neoronFile', e.target.files[0]);
            request.form(formData, 'saveNeoron', function (data) {
                if (data["error"]) {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Error',
                        textContent: data['error'],
                        ok: 'Close'
                    })).finally(function () {

                    })
                } else {
                    if (!$scope.loadData.neuralList)$scope.loadData.neuralList = [];
                    setTimeout(function () {
                        $scope.loadData.neuralList.push(data.value);
                    }, 100);

                }
            }, function (er) {
                $mdDialog.show($mdDialog.alert({
                    title: 'Error',
                    textContent: er,
                    ok: 'Close'
                })).finally(function () {

                })
            });
        },
        neuronUpdate: function (neuron) {
            request.add(neuron, 'activateNeoron').then(function (data) {
                if (data["error"]) {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Error',
                        textContent: data['error'],
                        ok: 'Close'
                    })).finally(function () {

                    })
                } else {
                    if (neuron.active == 1) {
                        for (var i = 0; i < $scope.loadData.neuralList.length; i++) {
                            $scope.loadData.neuralList[i].active = 1;
                        }
                    }
                    neuron.active = neuron.active == 1 ? 2 : 1;
                }
            });
        },
        neuronDrop: function (neuron, index) {
            request.add(neuron, 'dropNeoron').then(function (data) {
                if (data["error"]) {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Error',
                        textContent: data['error'],
                        ok: 'Close'
                    })).finally(function () {

                    })
                } else {
                    $scope.loadData.neuralList.splice(index, 1);
                }
            });
        }
    };

    var _curTimeZone = moment.tz(moment.tz.guess()).zoneAbbr();

    function updateTime(els, isNotUser) {

        var _numbers = ['total_calcs','total_log_ins','total_prints','last_count_two_week_log_ins','last_count_two_weeks_calsc'],
            _dates =  ['created','last_calc', 'last_log_in'];
        for (var ii = 0; ii < els.length; ii++) {
            var _c = els[ii];


            for (var i = 0; i < _numbers.length; i++) {
                _c[_numbers[i]] = parseFloat(_c[_numbers[i]]||'0');
            }
            for (var i = 0; i < _dates.length; i++) {
                var _date = _dates[i];

                _c[_date+'_sort'] = !_c[_date] ? '-' : _c[_date];
                if (_c[_date]) {
                    _c[_date] = moment.tz(new Date(0).setUTCSeconds(_c[_date] ), _curTimeZone).format('YYYY/MM/DD HH:mm:ss')

                }else{
                    _c[_date] = 'N/A';
                }

            }

        }

    }

    function showStatistic(title, dat) {
        $mdDialog.show({
            clickOutsideToClose: true,
            scope: $scope,        // use parent scope in template
            preserveScope: true,  // do not forget this if use parent scope
            template: '<md-dialog class="mdb-pop-up"aria-label="{{setting.load._type}}">' +
            '  <md-dialog-content style="padding:10px ">' +


            '<h1 style=" width: 100%;min-width: 381px;margin: 10px 0;" class="col-sm-6" >{{setting.load._type}}</h1>' +
            '<div >' +
            '   <table ng-table="setting.load.dataE" class="table mdb-table">' +
            '       <thead>' +
            '           <tr>' +
            '               <td>№</td>' +
            '               <td> Created</td>' +
            '           </tr>' +
            '       </thead>' +
            '       <tbody>' +
            '           <tr data-ng-repeat="statistic in $data">' +
            '               <td>{{$index+1}}</td>' +
            '               <td>{{::statistic.created | date:\'MM/dd/yyyy hh:mm\'}}</td>' +
            '           </tr>' +
            '       </tbody>' +
            '   </table>' +
            '</div>' +


            '  </md-dialog-content>' +
            '  <md-dialog-actions>' +
            '    <md-button ng-click="closeDialog()" class="md-primary">' +
            '      Close' +
            '    </md-button>' +
            '  </md-dialog-actions>' +
            '</md-dialog>',
            controller: function DialogController($scope, $mdDialog, $timeout) {

                $scope.closeDialog = function () {
                    $mdDialog.hide();
                    //$state.go('home');
                }
                $scope.setting.load.dataE = new NgTableParams({}, {dataset: $scope.setting.load._data});
                $timeout(function () {
                    angular.element('md-backdrop.md-opaque.md-default-theme, md-backdrop.md-opaque').addClass('white-back');
                }, 0);

            },
            onComplete: function () {

            }
        });
    }

    function loadCalculations(options) {
        var
            next = options.next,
            finish = options.onFinish,
            args = options.req,
            startFrom = 0,
            limmit = 100,
            results = [];
        (function loadCalcs() {
            var req = {limit: limmit, lastLoaded: startFrom};
            if (args)Object.assign(req, args);
            request.get(req, 'getCalculations').then(function (data) {
                    if (data.error) {
                        console.log(data);
                        if (options.onError)options.onError(data);
                    } else {
                        startFrom += limmit;
                        data.data.forEach(function (e, iter) {
                            var name = e.patient_name.split(" ");
                            e.fName = name[0];
                            e.sName = name[1];
                            e.calc_input = e.calc_input.split(",");

                            e.calc_input.forEach(function (val, item) {
                                if ((val || val.trim().length) && val.split("=")[1]) {
                                    e['calc_input' + val.split("=")[0].replaceAll([" ", "-"], "")] = val.split("=")[1].trim();
                                }
                            });


                            e.item = iter;
                            results.push(e);
                        });
                        next(results);
                        if (data.data.length == 0) {
                            if (finish)finish(results);
                        } else {
                            loadCalcs();
                        }
                    }


                }
            );
        })()

    }

    $scope.getSettingsTotalInfo = function(){
        if ($parent.utils.isAdmin()){

            request.get({},'getSettingsTotalInfo').then(function (data) {
                if (data['error']) {
                    $mdDialog.show($mdDialog.alert({
                        title: 'Error',
                        textContent: data['error'],
                        ok: 'Close'
                    })).finally(function () {
                        fadePreload(true);
                    });
                } else if (data['res']) {
                    $scope.loadData.settingsTotalInfo.totalUsers = data['res'].total_users[0];
                    $scope.loadData.settingsTotalInfo.calcsDone = data['res'].calcs_done[0];
                    $scope.loadData.settingsTotalInfo.totalLogins = data['res'].total_logins[0];
                    $scope.loadData.settingsTotalInfo.totalPrints = data['res'].total_prints[0];
                    $scope.loadData.settingsTotalInfo.last30daysCalcs = data['res'].last_30d_calcs[0];
                    $scope.loadData.settingsTotalInfo.last30daysLogins = data['res'].last_30d_logins[0];
                    $scope.loadData.settingsTotalInfo.last30daysPrints = data['res'].last_30d_prints[0];
                    $scope.loadData.settingsTotalInfo.last30daysCreated = data['res'].last_30d_created[0];
                }
            });
        };
        //

    setTimeout(function(){
        var a = document.querySelectorAll('._md')[1];
        a.style.position = 'inherit';
        setTimeout(function(){
            a.style.position = 'absolute';
            }, 50);
        },50);
    };

    var loadedList = $parent.loadedCalcList || [];
    if ($parent.utils.isAuth()) {
        updateInputdefVal();
    } else {
        $parent.utils.getSession(updateInputdefVal)

    }

    function updateInputdefVal(flag, id_val, uploadData) {
        if ($parent.utils.isAdmin())  $scope.setting.loadData();
        $parent.utils.updateInputData($scope, ['inputData'],
            {KI: 'input_KIndex', AC: 'AConstant', Rfrctn: 'Rfrctn'},
            $scope.inputData.Kindexes, {f: flag, d: uploadData, i: id_val});
    }

    $timeout(function () {
        angular.element('#version-output').html(VERSION_APP);
        angular.element('#neuronFile').on('change', function (e) {
            $scope.setting.neuronFileChange(e);
        });
    }, 1000);

}]);