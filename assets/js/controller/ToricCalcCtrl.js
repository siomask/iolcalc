app.controller("ToricCalcCtrl", ["$scope", "$state", "request", "$timeout", "$mdDialog", "$compile",
    function ($scope, $state, request, $timeout, $mdDialog, $compile) {
        jQuery(preloader_animate).css('display', 'block');
        var $parent = $scope.$parent;
        var ses = $parent.utils.isAuth();
        $scope.patientName = '';
        $scope.calc = {
            plot: function (form, threejs) {
                var _t = this,
                    _dat = $scope.data,
                    isRight = _dat.eyeActive == 'Right (OD)',
                    input = _dat.input,
                    d = _dat._outputs,
                    _iol = domEl[0];

                var onFinish = function(){
                    $parent.utils.preload();
                }
                var arr = _inputes,
                    inputError = false;

                /*if(d[req[0]]) {
                    for (var i = 0; i < arr.length; i++) {
                        var _c = arr[i],
                            fieldName = _c.n,
                            val = parseFloat(d[fieldName]);

                        if (fieldName == fields[11]) continue;

                        if (_c.r == req[0] && (isNaN(val) || (val < _c.min || val > _c.max))) {
                            onFinish();
                            return $mdDialog.show($mdDialog.alert({
                                title: 'Error',
                                textContent: fieldName + " must be between " + _c.min + " and " + _c.max,
                                ok: 'Close'
                            })).finally(function () {
                            });
                        }

                    }
                }*/

                d[req[0]] = true;
                $timeout(function () {
                    if ((  form[0].$invalid  ) ) {
                        for (var k = 0; k < form.length; k++) {
                            var curForm = form[k],
                                domF = document.getElementsByName(curForm.$name);
                            if (curForm.$invalid) {
                                for (var i = 0; i < arr.length; i++) {
                                    var _c = arr[i],
                                        fieldName = _c.n ;
                                    if (_c.r == req[0] && curForm[fieldName] && (curForm[fieldName].$invalid || (parseFloat(curForm[fieldName].$viewValue) < _c.min || parseFloat(curForm[fieldName].$viewValue) > _c.max))) {
                                        curForm[fieldName].$touched = true;
                                        curForm[fieldName].$dirty = true;
                                        curForm[fieldName].$setDirty(true);
                                        jQuery(domF).find(document.getElementsByName(fieldName)).addClass('input-err');
                                    } else {
                                        jQuery(domF).find(document.getElementsByName(fieldName)).removeClass('input-err');
                                    }
                                }
                            }
                        }
                        onFinish();

                    } else {
                        if (!IS_WEBGL_AVIALABLE) {
                            _dat.neeSecEye = 1;
                            $mdDialog.show($mdDialog.alert({
                                title: 'Warning',
                                textContent: "WebGl is not supported in your browser",
                                ok: 'Close'
                            })).finally(function () {
                                checkBothForm();
                            });
                        } else {
                            checkBothForm();
                        }


                        function checkBothForm() {
                            calculation();
                        }

                        function calculation() {
                            $parent.utils.preload(true);
                            $timeout(function () {


                                var calcVal = {};
                                /*arr.forEach(function (field) {
                                    calcVal[field] = d[field];
                                    if (field == fields[11]) {
                                        calcVal['needInputACD'] = (d[field] && d.ACDneedToBeRequred);
                                    }
                                });*/


                                var onError = function (text, av) {
                                    //document.getElementById('logo_text').focus();
                                    $parent.utils.preload();
                                    $mdDialog.show($mdDialog.alert({
                                        title: 'Error',
                                        textContent: text,
                                        ok: 'Close'
                                    })).finally(function () {
                                        onFinish(av);
                                        //if (webglEl && webglEl[threejs])webglEl[threejs].build.clear();
                                        //$scope.data.input.IOL = _iol = [];
                                        //if (form && form['ACD'])form['ACD'].$touched = true;
                                        //if (av)   d.ACDneedToBeRequred = "yeas";
                                        //
                                        //webglEl.needToResize = false;
                                        //jQuery(_iol).fadeOut();
                                        //resizeThreejsDiv();

                                    });
                                };
                                var onSucceces = function (_r) {

                                    if (_r) {
                                        calcVal._r = _r;
                                        _dat.iolODRef = _r.iolRef;
                                        //$scope.data.input.IOL = _r.yPst.toFixed(2) + " D";
                                        //if (webglEl && webglEl[listOfGraph[1]]) {
                                        //    webglEl[listOfGraph[1]].build.buildEyeTorState();
                                        //    webglEl.needToResize = true;
                                        //}
                                    }
                                    _dat.isInputing = 0;
                                    _dat.summary = 1;
                                    jQuery(_iol).fadeIn();
                                    resizeThreejsDiv();
                                    jQuery('body').css('overflow', 'auto');
                                };
                                request.add(calcVal, 'calculate').then(function (data) {

                                    $scope.submitted = false;
                                    if (data['error']) {
                                        onError(data['error'], data['ACDneedToBeRequred']);
                                    } else {
                                        if (data["success"]) {
                                            var _r = data['calc'];
                                            onSucceces(_r);
                                        } else {
                                            onError("Some bad calculate");
                                        }

                                    }


                                }, function (error) {
                                    onError("Some bad calculate");
                                });


                            }, 100);
                        }

                    }
                }, 100 );


            },
            reset: function (form, threejs, isResetBgn, isEdit) {
                $scope.submitted = true;
                $scope.isEdit = isEdit;
                var _dat =$scope.data;

                if (isResetBgn || isEdit) {
                    if (!isEdit) _dat.ptntName = _dat.ptntID = null;
                    _dat.isInputing = 1;
                    _dat.summary = 0;
                    if ($scope.toggle)this.toggle();
                }

                if (!isEdit) {
                    _dat._outputs={};
                    _dat._outputs[fields[1]] = '1.3375';
                    _dat.patient = {};
                    _dat.eyeActive = null;
                    _dat.doctor = {};
                }

                $timeout(function () {
                    if (!isEdit) {
                        var arr = fields;
                        for (var i = 0; i < arr.length; i++) {
                            var inpName =  arr[i];
                            if (form[inpName]) {
                                form[inpName].$touched = false;
                                form[inpName].$dirty = false;
                                jQuery(document.getElementsByName(inpName)).removeClass(_classes.er);
                            }

                        }
                        updateInputData();
                    }
                    var l = [
                        fields[7],
                        fields[9],
                        fields[14]
                    ];
                    l.forEach(function (o) {
                        $scope.calc.onInput(o);
                    })
                }, 100);

            },
            print: function (threejs) {
                var strMime = "image/png",

                    DOM = '#' + threejs,
                    isRight = threejs == listOfGraph[0],
                    img_ = document.getElementById(("threejsODOutput")),
                    img__ = document.getElementById(("threejsOSOutput")),
                    w = jQuery(DOM).width(),
                    _dat = $scope.data,
                    h = w - w * 0.2,
                    _curThre = webglEl && webglEl[_dat.listOfGraph[2]] ? webglEl[_dat.listOfGraph[2]] : false,
                    _t = this;
                $parent.utils.preload(true);
                $scope.data.eyeActive = isRight ? "Right (OD)" : "Left (OS)";

                jQuery('#pdf-content').css({display: "block"});
                jQuery('.pdf-content').css({top: 0 + 'px', left: 0 + 'px'});
                jQuery('body').css({overflow: 'hidden'});
                var star;
                if (false && _curThre) {
                    jQuery(DOM + ' canvas').css({position: 'absolute', top: w + 'px'});
                    camera.aspect = w / h;
                    renderer.setSize(w, h);
                    camera.setSize(w, h);
                    camera.updateProjectionMatrix();
                    if (webglEl.objState) {
                        star = false;//webglEl.objState.getObjectByName('medic_star');
                        if (star) {
                            star.lineBorder.scale.multiplyScalar(2);
                            star.scale.multiplyScalar(2);
                            jQuery.each(star.material.materials, function (k, o) {
                                o.colorGCopy = o.color.clone();
                                o.color = new THREE.Color(0xffffff);
                            });
                        }
                    }
                }

                setTimeout(function () {
                    if (_curThre) {

                        var arr = [
                                {v: _curThre, im: img_}
                            ],
                            dom = jQuery('.data-data'),
                            el = dom.find('.image-con > img'),
                            table = dom.find('table');
                        for (var i = 0; i < arr.length; i++) {
                            var __c = arr[i].v;
                            if (__c.scene.children.length > 1) {
                                jQuery(el[i]).css('display', 'block');
                                jQuery(table[i]).css('display', 'table');
                                __c.timer.startTimer();
                                __c.render.domElement.getContext("experimental-webgl", {preserveDrawingBuffer: true});
                                arr[i]['im'].src = ( __c.render.domElement.toDataURL(strMime));
                            } else {
                                jQuery(el[i]).css('display', 'none');
                                jQuery(table[i]).css('display', 'none');
                            }
                        }
                    }
                    var pdfHtml = document.getElementsByClassName('pdf-content'),
                        sd = pdfHtml[0];
                    var d_h = jQuery('.data-data').height(),
                        c_h = jQuery(img_).height();
                    // jQuery('.image-con').css({height: d_h});

                    var onfinish = function () {

                        resizeThreejsDiv();
                        jQuery(DOM + '  canvas').css({position: 'static', top: w + 'px'});
                        var _y = jQuery('.pdf-content').height(),
                            _x = jQuery('.pdf-content').width();
                        //console.log(_x._y);
                        jQuery('.pdf-content').css({
                            top: -_y + 'px',
                            left: -_x + 'px'
                        });
                        document.title = "IOLcalc - Ladas Super Formula";
                        jQuery('body').css({overflow: 'auto'});
                        if (star) {
                            star.lineBorder.scale.multiplyScalar(0.5);
                            star.scale.multiplyScalar(0.5);
                            jQuery.each(star.material.materials, function (k, o) {
                                o.color = o.colorGCopy;
                            });
                        }

                        // $scope.data.input = {KIndex: K_index};
                        // $scope.data.iolRef = [];
                        request.add({statistic: {st_printes: 1}}, 'statistic').then(function (data) {
                        });
                    };


                    html2canvas(sd).then(function (canvas) {
                        var imgData = canvas.toDataURL(strMime);
                        var link = document.getElementById('contvd');
                        link.src = imgData;
                        $parent.utils.preload();

                        var title = "LSF IOL Calc" +
                                ($scope.data.ptntID ? " - " + $scope.data.ptntID : "") +
                                ($scope.data.ptntName ? " - " + $scope.data.ptntName : "")
                        // + ( isRight ? "(OD)" : "(OS)")
                            ;
                        document.title = title;

                        setTimeout(function () {
                            if ($parent.isIE) {
                                onfinish();
                                var delta = 18,
                                    _h = 750,
                                    _w = 545;
                                jQuery(link).height((_h + _h * 0.2));
                                _h += delta;
                                var myWindow = window.open('', 'IE PRINT', 'width=' + _w + ',height=' + (_h));
                                myWindow.document.title = title;
                                myWindow.document.body.innerHTML = jQuery('#contvdParent').html();
                                myWindow.document.close(); //missing code
                                myWindow.focus();
                                myWindow.print();

                            } else {
                                window.print();
                            }
                            onfinish();
                        }, 100);
                    }, function (err) {
                        console.log(err, "--------------------");
                    });

                }, 200);

            },
            changePrintView: function (flag) {
                var _t = this;
                if (!_t.isOpenPDF) {
                    jQuery('.pdf-content').css({
                        transform: "scale(0.2)",
                        left: '-980px',
                        top: '-1380px',
                        position: "relative"
                    });
                    _t.isOpenPDF = true;
                }

                _t.backSubmit = true;
                if (flag) {
                    $parent.utils.preload();
                    jQuery('.pdf-content').css({left: '-980px', top: '-1380px', "z-index": 10, position: "relative"});
                    //jQuery('.pdf-content').css({"z-index":10,left:'0px',top:'0px'});
                    jQuery('.cont').css({display: "none"});
                    jQuery('#backSubmit').css({display: "block"});
                    jQuery('#calcCostum').css({display: "none"});
                    jQuery('body').css({overflow: "hidden"});

                } else {
                    var _y = jQuery('.pdf-content').height(),
                        _x = jQuery('.pdf-content').width()
                    jQuery('.pdf-content').css({
                        top: -_y + 'px',
                        left: -_x + 'px'
                    });
                    jQuery('.pdf-content').css({"z-index": -10, position: "relative"});
                    //jQuery('.pdf-content').css({ "z-index":-10});
                    jQuery('.cont').css({display: "block"});
                    jQuery('#calcCostum').css({display: "block"});
                    jQuery('#backSubmit').css({display: "none"});
                    jQuery('#pdf-content').css({display: "none"});
                    jQuery('body').css({overflow: "scroll"});
                    resizeThreejsDiv();
                }


            },
            cutImage: function (im) {
                var c = document.createElement('canvas');
                var img = new Image();
                img.src = im;
                c.width = img.width;
                c.height = img.height;
                var ctx = c.getContext('2d');
                ctx.drawImage(img, 0, 0, c.width, c.height);
                var _pst = getCanvasActivePixels(c);

                var canvas = document.createElement('canvas');
                $(canvas).css('position', 'absolute');
                canvas.width = _pst.x2 - _pst.x1;//endX-firstX ;
                canvas.height = _pst.y2 - _pst.y1;//endY-firstY ;
                var context = canvas.getContext('2d');
                var st = 0;
                context.drawImage(img, _pst.x1 - st, _pst.y1 - st, canvas.width, canvas.height, st, st, canvas.width, canvas.height);
                return canvas.toDataURL("image/png");
            },
            printOld: function () {
                //$('#actions').fadeOut();
                $('#threejsPrint').fadeIn();
                renderer.domElement.getContext("experimental-webgl", {preserveDrawingBuffer: true});
                var strMime = "image/png",
                    img = document.getElementById('threejsPrint').childNodes[1],
                    img_ = document.getElementById('threejsPrint_').childNodes[1]
                    ;
                img.src = renderer.domElement.toDataURL(strMime);
                img_.src = renderer.domElement.toDataURL(strMime);
                var bodyH = window.innerHeight;
                //console.log(bodyH);
                var sd = document.getElementsByClassName('container_main')[0];
                $(sd).css(
                    //'height',(bodyH)+'px',
                    'width', (bodyH * 1.414) + 'px');
                //$('.contvd').css('width',(bodyH*1.4143)+'px');
                html2canvas(sd).then(function (canvas) {
                    var imgData = canvas.toDataURL(strMime);
                    var link = document.getElementById('contvd')//document.createElement('a');
                    link.src = imgData//.replace(strMime, "image/octet-stream");
                    $('#threejsPrint').fadeOut();
                    $(sd).css(
                        'width', '100%');
                    setTimeout(function () {
                        window.print();
                    }, 100);

                });
                //$('#actions').fadeIn();
            },
            isAdmin: function () {
                return $parent.utils.isAdmin();
            },
            createPromo: function () {
                $scope.submitted = true;
                if (!$scope.data.promocode) {
                    return
                }
                request.add({
                    code: $scope.data.promocode,
                    user: $parent.session.user
                }, 'savePromoCode').then(function (data) {
                    if (data['error']) {
                        alert(data['error']);
                    } else {
                        $scope.promoCode = false;
                        alert(data['success']);
                    }
                    $scope.submitted = false;
                });
            },
            upload: function () {
                angular.element(document.querySelector('#fileInput')).click();
            },
            changeFile: function ($file, $message, $flow) {
            },
            toggle: function () {
                $scope.toggle = !$scope.toggle;
                var iyt = setInterval(function () {
                    resizeThreejsDiv();
                }, 10);

                var _h = jQuery(".body-card").height() + 16;
                jQuery(".body-card").css({"min-height": _h, "max-height": _h})
                $(".target").toggle("slow", function () {
                    jQuery(".body-card").css({"min-height": 0, "max-height": 10 * _h})
                    resizeThreejsDiv();
                    clearInterval(iyt);
                });
            },
            edit: function () {

            },
            version: function () {
                $mdDialog.show({
                    clickOutsideToClose: true,
                    scope: $scope,        // use parent scope in template
                    preserveScope: true,  // do not forget this if use parent scope
                    template: '<md-dialog aria-label="LADAS SUPER FORMULA">' +
                    '  <md-dialog-content style="padding:10px ">' +
                    VERSION_APP +
                    '  </md-dialog-content>' +
                    '  <md-dialog-actions>' +
                    '    <md-button ng-click="closeDialog()" class="md-primary">' +
                    '      Close' +
                    '    </md-button>' +
                    '  </md-dialog-actions>' +
                    '</md-dialog>',
                    controller: function DialogController($scope, $mdDialog, $timeout) {
                        $scope.closeDialog = function () {
                            $mdDialog.hide();
                        }
                        $timeout(function () {
                        }, 1500);

                    },
                    onComplete: function () {
                    }
                });
            },
            onInput: function (name) {
                if (!name)return;
                var _dat = $scope.data,
                    input =  _dat['_outputs'],
                    domF = '.tor-form-full';

                switch (name) {
                    case fields[10]:
                    {
                        if ((input[fields[17]] || input[fields[20]])) {
                            input[fields[20]] = true;
                            var curInp = input[fields[10]],
                                isCor = curInp > 30 || curInp < 20 || !curInp;
                            input[fields[17]] = isCor ? "yeas" : null;
                            if (!input[fields[18]])jQuery(domF).find(document.getElementsByName(fields[18])).removeClass(isCor ? '' : _classes.er).addClass(isCor ? _classes.er : '');
                        }
                        break;
                    }
                }
            },
            selectEye: function (name) {
                $scope.data.eyeActive = name;
            }
        }

        var ioles =[];
        for (var i = 6; i <= 36; i += 0.5) {
            ioles.push(i);
        }
        var exam,
            req =['calcsFormSubmitted','none'],
            fields =[
                "IOL",
                "Device Refractive Index",
                "Optional K1",
                "Optional K2",
                "Lens Factor",
                "A-Constant",
                "Flat K",
                "Flat Axis",
                "Steep K",
                "Steep Axis",
                "Axial Length",
                "Optical ACD",
                "Target Refraction",
                "Incision SIA",
                "Incision Location",
                "Lens Thickness",
                "WTW",
                "ACDneedToBeRequred",
                "Cct",
                "AD",
                "ACDResultFromServer",
            ],
            domEl =['#iolODData'],
            _inputes = [
                {t: "text", n: fields[0], s: ioles, o: {n: fields[0]+"s"}},
                {t: "text", n: fields[1], s: ['1.3315', '1.3320', '1.3360', '1.3375', '1.3380'], o: {n: fields[1]+"s"}},
                {t: "text", n: fields[2], min: 35, max: 55, r: req[0]},
                {t: "text", n: fields[3], min: 35, max: 55, r: req[0]},
                {t: "text", n: fields[4], min: 2, max: 5, r: req[1]},
                {t: "text", n: fields[5], min: 100, max: 120, r: req[0]},
                {t: "text", n: fields[6], min: 30, max: 50, r: req[1]},
                {t: "text", n: fields[7], min: 0, max: 360, r: req[1]},
                {t: "text", n: fields[8], min: 30, max: 50, r: req[1]},
                {t: "text", n: fields[9], min: 0, max: 360, r: req[1]},
                {t: "text", n: fields[10], min: 15, max: 35, r: req[0]},
                {t: "text", n: fields[11], min: 0, max: 5, r: req[0]},
                {t: "text", n: fields[12], min: -10, max: 10, r: req[0]},
                {t: "text", n: fields[13], min: 0, max: 2, r: req[1]},
                {t: "text", n: fields[14], min: 0, max: 360, r: req[1]},
                {t: "text", n: fields[15], min: 2, max: 8, r: req[1]},
                {t: "text", n: fields[16], min: 8, max: 14, r: req[1]},
            ],
            _classes = {
                er: 'input-err',
                ok: 'input-succeces'
            };

        $scope.data = {
            name:"ToricCalcCtrl",
            none: false,
            isInputing:true,
            _inputes:_inputes,
            _fields:fields,
            _outputs:{},
            PersConstes: ['Personal Constant', 'Alcoln SN60WF'],
            PersCons: 'Personal Constant',
            calcs: ["1", "2"],
            eyeActive: null,
            userData: {},
            file:{
                old:[
                    {
                        n: 'AxialData',
                        f: [{xml: 'Acd', or: fields[11]}, {xml: 'Al', or: fields[10]},
                            {xml: 'Cct', or: fields[18]}]
                    },
                    {
                        n: 'Keratometry',
                        f: [{xml: 'SteepK', or: fields[8]},
                            {xml: 'FlatK', or: fields[6]},
                            {xml: "RefractiveIndex", or: fields[1]}]
                    }
                ],
                _new:[
                    {
                        f: [
                            {xml: 'K1', or: fields[2]},
                            {xml: 'n', or: fields[1]},
                            {xml: 'K2', or: fields[3]},
                            {xml: 'CCT', or: fields[18]},
                            {xml: 'AD', or: fields[19]},
                            {xml: 'AL', or: fields[10]},
                            {xml: 'WTW', or: fields[16]},
                            {xml: 'K1Axis', or: fields[5]}
                        ]
                    }
                ]},
            label: {
                IOL: "IOL POWER",
                web_header: "Barrett Toric Calculator"
            }
        };
        $scope.data._outputs[fields[1]] = '1.3375';

        if (!ses) {
            $parent.utils.getSession(updateInputData);
        }else{
            updateInputData();
        }
        function updateInputData() {
            try{
                $parent.utils.updateInputData($scope.data,
                    ['_outputs'],
                    {KI: fields[1], AC: fields[5]}, _inputes[0].s);


            }catch(e){
                console.log(e);
            }

            $timeout(function(){
                if(!$scope.data.fuck){
                    jQuery('.Right_OD').click();
                    setColorSvg(0,[{selector:'.tor-calc-btn',col:"#ffffff"},{selector:'.tor-row',col:"#009688"}]);
                    $scope.data.fuck =1;
                }
            },100);

            $parent.utils.preload();
        }

    }]);