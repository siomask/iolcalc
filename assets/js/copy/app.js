var app = angular.module('Main', ['ngMaterial']);
app.controller('AppCtrl', ['$scope', '$location', function ($scope, $location) {
    var preloader = "preloader";
    $scope.calc = {
        plot: function (flag) {
            var min = 10, max = 50,
                d = $scope.data.input;
            //$('.' + preloader).fadeIn();
            if (d.K1 && d.K2 && d.ACD && d.axialLength && d.AConstant) {
                var result = PLOTG(
                    {
                        AConstant: d.AConstant,
                        ACD: d.ACD,
                        K1: d.K1,
                        K2: d.K2,
                        axialLength: d.axialLength,
                        Rfrctn: d.Rfrctn
                    });
                if (result){
                    $scope.data.input.IOL = result.yPst.toFixed(2);
                   if(flag) $scope.$apply();
                }
            } else {
                alert('you forgot to set some input');
            }
            //$('.' + preloader).fadeOut();
        },
        reset: function () {
            webglEl.build.clear();
            $scope.data.input={};
            $scope.data.patient={};
            $scope.data.doctor={};
        },
        print: function () {
            //$('#actions').fadeOut();
            $('#threejsPrint').fadeIn();
            renderer.domElement.getContext("experimental-webgl", {preserveDrawingBuffer: true});
            var strMime = "image/png",
                img = document.getElementById('threejsPrint').childNodes[1];
            img.src = renderer.domElement.toDataURL(strMime);
            html2canvas(document.body).then(function (canvas) {
                var imgData = canvas.toDataURL(strMime);
                var link = document.createElement('a');
                document.body.appendChild(link);
                link.download = "superformulagroup.png";
                link.href = imgData.replace(strMime, "image/octet-stream");
                link.click();
                document.body.removeChild(link);
                $('#threejsPrint').fadeOut();
            });
            //$('#actions').fadeIn();
        },
        denied: function () {
            if (window.parent) {
                window.parent.location.assign("http://iolcalc.com/")
            } else {
                window.location.assign("http://iolcalc.com/")
            }
        },
        access: function () {
            $scope.data.userAccept = true;
            setTimeout(function () {
                resizeThreejsDiv();
                var parent = document.getElementById('THREEJS');
                var w = $(parent).width(),
                    h = $(parent).height();
                camera.aspect = w / h;
                camera.updateProjectionMatrix();
                renderer.setSize(w, h);
            })

        }
    };
    $scope.data = {
        userAccept: false,
        patient: {
            name: 'Patient'
        }, doctor: {
            name: 'Doctor'
        }, input: {
            Rfrctn: 0,
            K1: 44.25,
            K2: 43.98,
            axialLength: 27.45,
            ACD: 3.45,
            AConstant: 119.2/*,
             IOL: */
        }, label: {
            axialLength: "Axial Length",
            ACD: "Measured ACD ",
            A: "A-Constant",
            Rfrctn: "Target-Refraction",
            IOL: "IOL POWER"
        }
    };


    window.addEventListener('keydown', function (ev) {
        if (ev.keyCode == 13) {
            $scope.calc.plot(true);
        }
    }, false);
    //$('.' + preloader).fadeOut();
}]);

function PLOTG(d) {
    //reset variables
    AConstant = d.AConstant ? d.AConstant : 115.3;
    SF = (AConstant * 0.5663) - 65.6;
    ACD = d.ACD;
    //ACD=(SF + 3.595) / 0.9704;//5.665663643858211
    ACDKonstant = (AConstant * 0.62467) - 68.747;
    a0 = ((ACDKonstant / 1000) - (a1 * (MWVKpr / 1000))) - (a2 * (MWALpr / 1000));
    pACD = (0.58357 * AConstant) - 63.896;
    K3 = (((d.K1) + (d.K2)) / 2);
    AL = d.axialLength;
    Ref = d.Rfrctn;
    HofRef = Ref / (1 - (0.012 * Ref));
    VKpr = ACD;

    var yPst = SUPERFORMULA(K3, AL);
    var points = [], faces = [],
        xBgn = 20, yBgn = 37,
        abscissSize =10,
        delta = {x: 0.0, y: abscissSize / K3, z:abscissSize / K3},
        step = 0.25,
        indx = 0, indStep = (1 / step) * abscissSize,
        maxByIt = 10 - step, _yMax = 0,curMaxY=0;
    for (var i = 0; i <= abscissSize; i += step) {
        var curX = xBgn + i,
            curY = yBgn;
        for (var k = 0; k <= abscissSize; k += step) {
            curMaxY = (SUPERFORMULA( curY+ k ,curX)) *delta.y;
            //if(curX >29 && curY + k>29){console.log(curY+ k,curX )}
            if (_yMax < curMaxY)_yMax = curMaxY;
            points.push(new THREE.Vector3(curX , curMaxY, curY+ k));
            if (k <= maxByIt) {
                if (i <= maxByIt) {
                    faces.push(new THREE.Face3(indx + 1, indx, indx + 1 + (indStep)));
                    faces.push(new THREE.Face3(indx+1, indx +1+ indStep, indx+ 2 + indStep));
                }
            } /*else {
                if (indx + 2 > indStep) {
                    //faces.push(new THREE.Face3(indx + 2 - (indStep), indx, indx + 1 - (indStep)));
                }
            }*/
            indx++;
        }
    }

    var param = {
        _zSt: Math.ceil(Math.round(K3) / 10),
        points: points,
        faces: faces,
        _yMax: _yMax,
        yPst: yPst,
        indStep: indStep,
        poinPst: {x: K3, y: yPst * delta.y, z: AL}
    };

    webglEl.build.buildEyeState(param);
    return param;
}

function resizeThreejsDiv() {
    var h = window.innerHeight - ($($('.level-1')[0]).height()  + 52 + 74);
    $('.threejs').css('min-height', h).css('max-height', h);
    $('.toolltip').css('margin-top', -h);
}



