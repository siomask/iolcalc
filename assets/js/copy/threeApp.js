var Threejs = function () {

    var scene,
        controls, objState, isAlreadyAbscissHasTooltip = false,
        parent = document.getElementById('THREEJS');


    function init() {
        var w = $(parent).width(),
            h = $(parent).height();

        scene = new THREE.Scene();
        var orphS = 60;
        //camera = new THREE.PerspectiveCamera(55, w / h, 0.1, 3000);
        //camera =   new THREE.OrthographicCamera(w / - orphS, w / orphS, h / orphS, h / - orphS, 1, 300);
        camera = new THREE.CombinedCamera( w , h , 55, 0.1, 3000, 1, 300 );
        camera.updateProjectionMatrix();
        camera.toOrthographic();
        //camera.toPerspective();
        camera.setZoom(100);
        camera.position.set(16, 7, 16);
        camera.lookAt(new THREE.Vector3(0,3,0));
        //scene.add(new THREE.AxisHelper(10,7,3));

       /* controls = new THREE.OrbitControls(camera);
        controls.minDistance = 0;
        controls.maxDistance = 50;
        controls.target.set(5, 5, 5);*/

        //lights
        var directionalLight = new THREE.DirectionalLight(0xE8EEEF, 0.8);
        directionalLight.position.set(100, 100, 100);
        directionalLight.shadowCameraVisible = true;
        scene.add(directionalLight);

        var hemiLight = new THREE.HemisphereLight(0xFFFFFF, 0xFFFFFF, 0.6);
        hemiLight.groundColor.setHSL(0.1, 1, 0.7);
        hemiLight.position.set(1, 1, 1);
        scene.add(hemiLight);

        scene.fog = new THREE.Fog(0xECE0E0, 2000, 10000);
        renderer = new THREE.WebGLRenderer({
            antialias: true, alpha: true, preserveDrawingBuffer: true
        });
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(w, h);
        renderer.setClearColor(scene.fog.color, 0);
        renderer.autoClear = false;
        renderer.gammaInput = true;
        renderer.gammaOutput = true;
        renderer.shadowMapEnabled = true;

        parent.appendChild(renderer.domElement);
        //build.buildEyeState({});
        window.addEventListener('resize', onWindowResize, false);
        //window.addEventListener('mousedown', onKeyDown, false);
        //window.addEventListener('keydown', onKeyDown, false);

    }


    function animate() {
        requestAnimationFrame(animate);
        renderer.clear();
        //controls.update();
        renderer.render(scene, camera);
    }

    function onWindowResize() {
        resizeThreejsDiv();

    }

    var build = {
        buildEyeState: function (arg) {
            var deltaY = arg._yMin,
                _x = 10,//arg._x?arg._x: 5,
                _y = deltaY < 0 ? arg._yMax + (deltaY * -1) : arg._yMax - (deltaY),//arg._y?arg._y: 5,
                _yMax = 10,//arg._y?arg._y: 5,
                _z = 10,// arg._z?arg._z: 5,
                _zSt = arg._zSt,
                pointsGl = arg.points ? arg.points : [],
                faces = arg.faces ? arg.faces : [],
                dft = {
                    x: 37, y: 20
                },
                indStep = arg.indStep,
                xStep = dft.x, yStep = dft.y, zStep = 0, font = 16;

            this.clear();
            objState = new THREE.Object3D();

            //build back plane
            var mtr = new THREE.MeshBasicMaterial({
                    color: "#fff",
                    side: THREE.DoubleSide
                }),
                xy = new THREE.Mesh(new THREE.PlaneGeometry(_x, _y), mtr),
                xz = new THREE.Mesh(new THREE.PlaneGeometry(_x, _z), mtr),
                zy = new THREE.Mesh(new THREE.PlaneGeometry(_z, _y), mtr);
            xy.position.set(_x / 2, _y / 2 + deltaY, 0);
            objState.add(xy);
            xz.position.set(_x / 2, deltaY, _z / 2);
            xz.rotation.x = -1.57;
            objState.add(xz);
            zy.position.set(0, _y / 2 + deltaY, _z / 2);
            zy.rotation.y = 1.57;
            objState.add(zy);

            var bgnPnt = new THREE.Vector3(0, deltaY, 0),
                points = [
                    {vB: new THREE.Vector3(_x, deltaY, 0), vE: new THREE.Vector3(_x, deltaY, _z), count: 10},
                    {vB: new THREE.Vector3(0, deltaY, _z), vE: new THREE.Vector3(_x, deltaY, _z), count: _y},
                    {vB: new THREE.Vector3(0, deltaY, _z), vE: new THREE.Vector3(0, _y + deltaY, _z), count: 10}
                ],
                dftPnts = [
                    {vB: bgnPnt, vE: new THREE.Vector3(_x, 0, 0), count: _x},
                    {vB: bgnPnt, vE: new THREE.Vector3(0, _y, 0), count: _y},
                    {vB: bgnPnt, vE: new THREE.Vector3(0, 0, _z), count: _z}
                ];

            //build grid
            for (var i = 0; i < points.length; i++) {
                objState.add(this.buildLine(points[i].vB, points[i].vE, false, 0.06));
            }
            for (var i = 0; i < dftPnts.length; i++) {
                var c = dftPnts[i].count;
                for (var k = 0; k <= c; k++) {
                    var sprite, label,
                        v1, v2, v3, v4, pntDst = 1.5, lblEnd = pntDst / 2;
                    if (i == 0 && (yStep++) % 2 == 0) {
                        var x = _x / c * k,
                            v4 = new THREE.Vector3(x, deltaY, _z + lblEnd);
                        v1 = new THREE.Vector3(x, deltaY, 0),
                            v2 = new THREE.Vector3(x, _y + deltaY, 0),
                            v3 = new THREE.Vector3(x, deltaY, _z);
                        sprite = this.drawSprite({
                            fontsize: font,
                            msg: (yStep - 1) + ''
                        })
                        sprite.position.set(x, deltaY, _z + pntDst);
                        label = this.buildLine(v3, v4, false, 0.02);
                    } else if (i == 1) {
                        if (zStep > 40) {
                            break;
                        }
                        var y = _y / c * k;
                        v1 = new THREE.Vector3(0, y, 0),
                            v2 = new THREE.Vector3(_x, y, 0),
                            v3 = new THREE.Vector3(0, y, _z),
                            v4 = new THREE.Vector3(0- lblEnd, y, _z );

                        sprite = this.drawSprite({
                            fontsize: font,
                            msg: (zStep.toString().length == 1 ? zStep + ' ' : zStep + '')
                        });
                        zStep += _zSt;
                        sprite.position.set(0- lblEnd, y, _z+ lblEnd );

                        //}
                    } else if (i == 2 && ((xStep++) % 2 == 0 && k > 0)) {
                        var z = _z / c * k;
                        v1 = new THREE.Vector3(0, deltaY, z),
                            v3 = new THREE.Vector3(_x, deltaY, z),
                            v2 = new THREE.Vector3(0, _y + deltaY, z),
                            v4 = new THREE.Vector3(_x + lblEnd, deltaY, z);
                        if (k > 0) {
                            sprite = this.drawSprite({
                                fontsize: font,
                                msg: (xStep - 1) + ''
                            })
                            sprite.position.set(_x + pntDst, deltaY, z);
                        }

                    }
                    if (sprite) {
                        objState.add(sprite);
                        objState.add(this.buildLine(v1, v2, 'inner', 0.02));
                        objState.add(this.buildLine(v1, v3, 'inner', 0.02));
                        objState.add(this.buildLine(v3, v4, false, 0.02));
                    }


                }
            }

            var geometry = new THREE.Geometry(), delta = 0;
            geometry.vertices = pointsGl;
            geometry.faces = faces;
            geometry.computeFaceNormals();
            //geometry.computeVertexNormals();
            //geometry.computeCentroids();
            geometry.mergeVertices();
            //geometry.computeLineDistances();
            //geometry.computeBoundingSphere();

            //var c=document.createElement("canvas"),del = 0.05;
            //c.width= c.height = 1+del;
            //var ctx=c.getContext("2d");
            ////ctx.lineWidth="2";
            //ctx.rect(del,del,c.width,c.width);
            //ctx.stroke();
            ////document.getElementById('contvd').src=c.toDataURL();
            //var textt = THREE.ImageUtils.loadTexture('assets/img/rama.png'/*c.toDataURL()*/);
            //textt.wrapS = textt.wrapT = THREE.RepeatWrapping;
            //textt.repeat.set( 2, 2 );
            var materials = new THREE.MeshPhongMaterial({
                color: "#416192",
                shading: THREE.SmoothShading,
                //map:textt,
                side: THREE.DoubleSide
            });


            //objState.add(new THREE.Mesh(new THREE.BoxGeometry(5,5,5,10),new THREE.MeshBasicMaterial({map:textt})));

            var modifier = new THREE.SubdivisionModifier( 2),
                smooth = geometry.clone();;
            smooth.mergeVertices();
            smooth.computeFaceNormals();
            smooth.computeVertexNormals();
            modifier.modify( smooth );

            var particles = new THREE.Mesh(smooth, materials);
            particles.position.set(-dft.y, delta, -dft.x);
            objState.add(particles);


                    /* var geometry = new THREE.Geometry(), delta = 0;
             geometry.vertices = pointsGl;
             geometry.computeFaceNormals();
             var materials = new THREE.PointCloudMaterial({
             color:'#ff6192',
             size:0.1
             });
             var particles = new THREE.PointCloud(geometry, materials);
             particles.position.set(-dft.y, delta, -dft.x);
             objState.add(particles);*/

            //build point
            var starPoints = [];
            var y1 = 50, x1 = 40;
            starPoints.push(new THREE.Vector2(0, y1));
            starPoints.push(new THREE.Vector2(10, 10));
            starPoints.push(new THREE.Vector2(x1, 10));
            starPoints.push(new THREE.Vector2(20, -10));
            starPoints.push(new THREE.Vector2(30, -y1));
            starPoints.push(new THREE.Vector2(0, -20));
            starPoints.push(new THREE.Vector2(-30, -y1));
            starPoints.push(new THREE.Vector2(-20, -10));
            starPoints.push(new THREE.Vector2(-x1, 10));
            starPoints.push(new THREE.Vector2(-10, 10));
            var starShape = new THREE.Shape(starPoints);
            var extrusionSettings = {
                size: 0.01, height: 0.01, curveSegments: 0.03,
                bevelThickness: 0.01, bevelSize: 0.02, bevelEnabled: false,
                material: 0, extrudeMaterial: 0.01, amount: 1
            };
            var starGeometry = new THREE.ExtrudeGeometry(starShape, extrusionSettings);

            var materialFront = new THREE.MeshBasicMaterial({color: 0x00D400});
            var materialSide = new THREE.MeshBasicMaterial({color: 0x00D400});
            var materialArray = [materialFront, materialSide];
            var starMaterial = new THREE.MeshFaceMaterial(materialArray);
            var star = /* build.drawSprite({img: $('#star')[0]});//*/new THREE.Mesh(starGeometry, starMaterial);
            var pPstn = arg.poinPst;
            star.position.set(pPstn.z - dft.y + 0.4, pPstn.y, pPstn.x - dft.x + 0.4);
            star.scale.multiplyScalar(0.01);
            star.rotation.set(-0.51, 0.7, 0.35);
            objState.add(star);

            //build grid or surface
            var grid = new THREE.Group(), st = 1 + indStep, step = indStep, flag = true;
            var abscissSize = arg.abscissSize,v1=[],v2=[],
                step = arg.step,
                indx = 0,
                id = 0,
                ih = 0,
                stp = 41;

            for (var i = 0; i <= abscissSize; i += step) {
                var geometry = new THREE.Geometry(),v1=[],v2=[],
                    vG = new THREE.Geometry();
                vG.vertices.push(
                    pointsGl[ih]
                );
                for (var k = 0; k <= abscissSize; k += step) {

                    v1.push( pointsGl[indx]);
                    geometry.vertices.push(
                        pointsGl[indx++]
                    );
                    if (ih + stp < pointsGl.length) {
                        vG.vertices.push(
                            pointsGl[ih += stp]
                        );
                        v2.push( pointsGl[ih]);
                    }
                }
                ih = ++id;
                var  smooth = geometry.clone(),smooth1 =vG.clone();
                smooth.mergeVertices();
                smooth1.mergeVertices();
                smooth.computeFaceNormals();
                smooth1.computeFaceNormals();
                smooth.computeVertexNormals();
                smooth1.computeVertexNormals();
                //modifier.modify( smooth );
                //modifier.modify( smooth1 );
                var spline1 = new THREE.SplineCurve3(v1),
                    spline2=new THREE.SplineCurve3(v2);
                geometry.vertices = spline1.getPoints(5000);
                vG.vertices = spline2.getPoints(5000);
                grid.add(new THREE.Line(geometry, new THREE.LineBasicMaterial({
                    color: "#000000", linewidth: 3
                })));
                grid.add(new THREE.Line(vG, new THREE.LineBasicMaterial({
                    color: "#000000", linewidth: 3
                })));
                //var curve = new THREE.SplineCurve3();
                //var spline1 = new THREE.SplineCurve3(v1),
                //    spline2=new THREE.SplineCurve3(v2);
                //geometry.vertices = spline1.getPoints(1000);
                //vG.vertices = spline2.getPoints(1000);
                //var
                //    gg1 = new THREE.TubeGeometry(spline1, 20, 0.01, 10),
                //    gg2 =new THREE.TubeGeometry(spline2, 20, 0.01, 10);
                //gg1.mergeVertices();
                //gg2.mergeVertices();
                //gg1.computeFaceNormals();
                //gg2.computeFaceNormals();
                //gg1.computeVertexNormals();
                //gg2.computeVertexNormals();
                ////modifier.modify( gg1 );
                ////modifier.modify( gg2 );
                //grid.add(new THREE.Mesh(gg1, new THREE.MeshPhongMaterial({color:'#000000'})));
                //grid.add(new THREE.Mesh(gg2, new THREE.MeshPhongMaterial({color:'#000000'})));


            }

            grid.position.set(-dft.y, delta+0.03, -dft.x);
            objState.add(grid);
            scene.add(objState);

            //add absciss tooltip
            var font = 17, textures = [
                build.drawTexture({msg: "Axial Length (mm)", fontsize: font}),
                build.drawTexture({msg: "Corneal Power (D)", fontsize: font}),
                build.drawTexture({msg: "IOL Power (D)", fontsize: font})
            ];

            //absciss tooltips
            var wP = 6, hP = wP * textures[0].siz.h / textures[0].siz.w,shift =15;
            deltaY -=0.3;
            spr = new THREE.Mesh(new THREE.PlaneGeometry(wP, hP), new THREE.MeshBasicMaterial({
                color: '#000000',
                map: textures[0]
            }));
            spr.rotation.set(-0.4, 0, 0);
            spr.position.set(8, deltaY, shift);
            spr.scale.multiplyScalar(1.4);
            objState.add(spr);

            hP = wP * textures[1].siz.h / textures[1].siz.w;
            spr = new THREE.Mesh(new THREE.PlaneGeometry(wP, hP), new THREE.MeshBasicMaterial({
                color: '#000000',
                map: textures[1]
            }));
            spr.rotation.set(-1.54, 1.09, 1.55);
            spr.position.set(shift, deltaY, 6);
            spr.scale.multiplyScalar(1.4);
            objState.add(spr);

            hP = wP * textures[2].siz.h / textures[2].siz.w;
            var spr = new THREE.Mesh(new THREE.PlaneGeometry(wP, hP), new THREE.MeshBasicMaterial({
                color: '#000000',
                map: textures[2]
            }));
            //spr.rotation.set(1.56, 1.56, 0);
            spr.rotation.set(0, 0.76, 1.57);
            spr.position.set(0, _y / 2, shift-2);
            objState.add(spr);
            //}
        },
        clear: function () {
            if (objState)scene.remove(objState);
        },
        buildSurface: function (args) {
            var _x = {min: 20, max: 30},
                _y = {min: 37, max: 47};
        },
        buildLine: function (vBgn, vEnd, typ, rad) {
            var matr = new THREE.MeshBasicMaterial({color: "#000000"});
            if (typ == 'inner') {
                matr.color = new THREE.Color(0xCAC6C6);
            }
            return new THREE.Mesh(new THREE.TubeGeometry(new THREE.CatmullRomCurve3([vBgn, vEnd]), 1, rad, 10), matr);
        },
        drawSprite: function (arg) {
            var texture = this.drawTexture(arg);
            var spriteMaterial = new THREE.SpriteMaterial({
                map: texture, rotation: 0,
                transparent: true, opacity: 1.0
            });
            var sprite = new THREE.Sprite(spriteMaterial);
            sprite.scale.set(1, texture.siz.h / texture.siz.w, 1);
            return sprite;

        },
        drawTexture: function (arg) {
            var can = document.createElement('canvas'),
                ctx = can.getContext('2d'),
                fd = arg,
                wid = fd.img ? fd.img.width : fd.fontsize * (fd.msg.length );
            can.width = fd.img ? wid : wid - wid * 0.01;
            can.height = fd.img ? fd.img.height : 14 + (fd.fontsize / 2);
            ctx.fillStyle = "rgba(255, 255, 255, 0)";
            ctx.fillRect(0, 0, can.width, can.height);
            ctx.lineJoin = "round";
            if (fd.msg) {
                ctx.font = "bold " + fd.fontsize + "px Open Sans";
                ctx.fillStyle = "rgba(0, 0, 0, 1)";
                ctx.fillText(fd.msg, 5, fd.fontsize);
            } else if (fd.img) {
                ctx.drawImage(fd.img, 0, 0, fd.img.width, fd.img.height);
            }

            var texture = new THREE.Texture(can);
            texture.needsUpdate = true;
            texture.minFilter = THREE.LinearFilter;
            texture.siz = {h: can.height, w: can.width};
            return texture;
        }
    };
    this.build = build;

    init();
    animate();
};
Math.getDistBtwTwoPoints = function (source, destination) {
    var dX = source instanceof Array && destination instanceof Array;
    var deltaX = dX ? destination[0] - source[0] : destination.x - source.x;
    var deltaY = dX ? destination[1] - source[1] : destination.y - source.y;
    var deltaZ = dX ? destination[1] - source[1] : destination.z - source.z;

    return this.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
};

var webglEl;

    $(document).ready(function () {
        setTimeout(function () {
        webglEl = new Threejs();
        resizeThreejsDiv();
        }, 500);
    });








