var   preloader_animate = ".preloader-text-animate",
 listOfGraph =['THREEJSOD','THREEJSOS','THREEJSTOR'],

  webglEl,listofWebgl=[];

function preventSelection(element){
    var preventSelection = false;

    function addHandler(element, event, handler){
        if (element.attachEvent)
            element.attachEvent('on' + event, handler);
        else
        if (element.addEventListener)
            element.addEventListener(event, handler, false);
    }
    function removeSelection(){
        if (window.getSelection) { window.getSelection().removeAllRanges(); }
        else if (document.selection && document.selection.clear)
            document.selection.clear();
    }
    function killCtrlA(event){
        var event = event || window.event;
        var sender = event.target || event.srcElement;

        if (sender.tagName.match(/INPUT|TEXTAREA/i))
            return;

        var key = event.keyCode || event.which;
        if (event.ctrlKey && key == 'A'.charCodeAt(0))  // 'A'.charCodeAt(0) можно заменить на 65
        {
            removeSelection();

            if (event.preventDefault)
                event.preventDefault();
            else
                event.returnValue = false;
        }
    }

    // не даем выделять текст мышкой
    addHandler(element, 'mousemove', function(){
        if(preventSelection)
            removeSelection();
    });
    addHandler(element, 'mousedown', function(event){
        var event = event || window.event;
        var sender = event.target || event.srcElement;
        preventSelection = !sender.tagName.match(/INPUT|TEXTAREA/i);
    });

    // борем dblclick
    // если вешать функцию не на событие dblclick, можно избежать
    // временное выделение текста в некоторых браузерах
    addHandler(element, 'mouseup', function(){
        if (preventSelection)
            removeSelection();
        preventSelection = false;
    });

    // борем ctrl+A
    // скорей всего это и не надо, к тому же есть подозрение
    // что в случае все же такой необходимости функцию нужно
    // вешать один раз и на document, а не на элемент
    addHandler(element, 'keydown', killCtrlA);
    addHandler(element, 'keyup', killCtrlA);
}

//preventSelection(document);
function resizeThreejsDiv() {
    //var blocks = $('.level-1'),
    //    curH = $('.infoLogo').height();
    //var minHeight =  jQuery('#max-height-block').height();
    //var h = window.innerHeight - (curH + 120 );

    if (listofWebgl ) {
        listofWebgl.forEach(function(o){
            if(!o || !o.scene)return;
            //var curEl = $('#' + o);
            /*if(webglEl[o].needToResize != 'no') {
                if (webglEl[o].needToResize) {
                    curEl.fadeIn();
                    var _d = curEl.height();
                    var curH,
                        _widt = curEl.width() * 0.78;
                    if (webglEl && webglEl[o].needToResize) {
                        curH = _widt;
                    } else {
                        curH = Math.abs(_d + (window.innerHeight - (minHeight)));
                    }
                    //if (Math.abs(curH / _widt) > 0.00150)$('#' + o).css({'min-height': curH, 'max-height': curH});
                } else {
                    curEl.fadeOut();
                }
            }else{
                curEl.fadeIn();
            }*/

            var camera = o['camera'],
               renderer = o['render'],
               scene = o['scene'],
               //parent = document.getElementById(o),
               w = $(o.getParent()).width(),
               h = $(o.getParent()).height();
            if(camera && renderer){
                camera.aspect = w / h;
                renderer.setSize(w, h);
                camera.setSize(w, h);
                camera.updateProjectionMatrix();
                renderer.render(scene, camera);
                //timer.startTimer();
            }

        });

    }
    //fadePreload(true);

    postCalcResize()
}

function postCalcResize(flag){
    var curLi =  jQuery('.fs-fields>.fs-current');
    if(flag)return  jQuery(curLi[0]).css('margin-top',0);
    if(curLi[0]){
        var mH = jQuery('.fs-form-wrap').height();
        var top = mH - curLi.height();
        if(top>0){
            jQuery(curLi[0]).css('margin-top',top/2);
        }
    }

}
function fadePreload(flag) {
    if (flag) {
        jQuery(preloader_animate).fadeOut();
     jQuery('body').css('overflow', 'auto');
    } else {
        jQuery(preloader_animate).fadeIn();
      jQuery('body').css('overflow', 'hidden');
                new ProgreesLoad().start();
    }
}

var eachCanvasConext = function (Canvas, callback) {
    var ctx = Canvas.getContext('2d');
    var mWidth = Canvas.width;
    var mHeight = Canvas.height;
    var imgd = ctx.getImageData(0, 0, mWidth, mHeight);
    var PixDATA = imgd.data;
    var cell = 0;
    for (var YY = 0; YY < mHeight; YY++) {
        for (var XX = 0; XX < mWidth; XX++) {
            var _Pixel = {
                r: PixDATA[cell],
                g: PixDATA[cell + 1],
                b: PixDATA[cell + 2],
                a: PixDATA[cell + 3],
                x: XX,
                y: YY
            }
            cell += 4;
            callback(_Pixel);
        }
    }
};

var getCanvasActivePixels = function (Canvas) {
    var L = false;
    var T = false;
    var R = false;
    var B = false;
    eachCanvasConext(Canvas, function (nextPixel) {
        if (nextPixel.a == 0) return;
        if (!T) {
            T = nextPixel;
        }
        if (!L) {
            L = nextPixel;
        }
        if (!R) {
            R = nextPixel;
        }
        if (!B) {
            B = nextPixel;
        }
        if (L.x > nextPixel.x) {
            L = nextPixel;
        }
        if (R.x < nextPixel.x) {
            R = nextPixel;
        }
        if (B.y < nextPixel.y) {
            B = nextPixel;
        }
    });
    return {x1: L.x, y1: T.y, x2: R.x, y2: B.y}
};

function detectIE() {
    var ua = window.navigator.userAgent;

    // Test values; Uncomment to check result …

    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

    // IE 12 / Spartan
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    // Edge (IE 12+)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}
function loadPrintCSS(flag) {
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = flag ? 'assets/css/all-ie-only.css' : 'assets/css/all-ie-exept.css';
    document.body.appendChild(link);
}
function openWin() {
    var myWindow = window.open('', '', 'width=200,height=100');
    myWindow.document.write("<p>This is 'myWindow'</p>");

    myWindow.document.close();
    myWindow.focus();
    myWindow.print();
    myWindow.close();

}
function runOnKeys(func) {
    var codes = [].slice.call(arguments, 1);

    var pressed = {};

    document.onkeydown = function(e) {
        e = e || window.event;

        pressed[e.keyCode] = true;

        for (var i = 0; i < codes.length; i++) { // проверить, все ли клавиши нажаты
            if (!pressed[codes[i]]) {
                return;
            }
        }

        pressed = {};

        func(e);

    };

    document.onkeyup = function(e) {
        e = e || window.event;

        delete pressed[e.keyCode];
    };

}

runOnKeys(
    function(e) {
        e.preventDefault();
    },
    17,
    83
);
function ProgreesLoad(){
    var canvas = document.getElementById('preloaderImg');
    var textOut = document.getElementById('preloaderStatus');
    var context = canvas.getContext('2d');
    var x = canvas.width / 2;
    var y = canvas.height / 2;

    var endPercent = 101;
    var curPerc = 0;
    var counterClockwise = false;
    var circ = Math.PI * 2;
    var quart = Math.PI / 2;
    var frustum, cameraViewProjectionMatrix, events;
    var markers;
    var octree;
    var cloud;
    context.lineWidth = 20;
    context.strokeStyle = '#3498db';
    context.shadowOffsetX = 0;
    context.shadowOffsetY = 0;
    context.shadowBlur = 10;
    context.shadowColor = '#656565';
    var radius = x-2* context.lineWidth;

    function drawPreloader(current) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.beginPath();
        context.arc(x, y, radius, -(quart), ((circ) * current) - quart, false);
        context.stroke();

    }
    this.start = function (){
        var percentComplete = 0/*,
         myInterval = setInterval(function(){

         if(percentComplete<100){
         percentComplete +=1;
         }else{
         clearInterval(myInterval);
         }
         },Math.random()*300)*/;
        var itr =10;
        (function animate(){
            percentComplete+=Math.random()*itr;
            if(percentComplete>50 && percentComplete<92)itr =(100-percentComplete)/10
            drawPreloader(percentComplete / 100);

            if(percentComplete<100){
                textOut.innerText = (percentComplete.toFixed(2));
                requestAnimationFrame(animate);
            }
        })();
    }
}
function supportsWebGl() {

    var support = true;

    try {

        var $canvas = $('<canvas />');
        $('body').append($canvas);
        var canvas = $canvas[0];

        if (canvas.addEventListener) {
            canvas.addEventListener("webglcontextcreationerror", function (event) {
                console.log('webglcontextcreationerror');
                support = false;
            }, false);
        }

        var context = create3DContext(canvas);
        if (!context) {

            console.log('No webgl context');

            if (!window.WebGLRenderingContext) {
                console.log('No WebGLRenderingContext');
            }

            support = false;
        }
    }
    catch (e) {
        console.log(e);
    } finally {
        $canvas.remove();
    }

    return support;
}
function setColorSvg(color,el){
    setTimeout(function(){
        var col = color || "#ffffff";
        if(el  && el instanceof Array){
            el.forEach(function(o){
                var v = jQuery(o.selector).find('svg');
                [].forEach.call(v,function(obj){
                    jQuery(obj).attr("fill", o.col);
                });
            })
        } else{
            var listSvg = document.getElementsByTagName('svg');
            [].forEach.call(listSvg,function(o){
                o.setAttribute("fill",col)
            });
        }

    },100);

}

function create3DContext(canvas) {
    var names = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"];
    var context = null;
    for (var ii = 0; ii < names.length; ++ii) {
        try {
            context = canvas.getContext(names[ii]);
        } catch (e) {
        }
        if (context) {
            break;
        }
    }
    return context;
}

var IS_WEBGL_AVIALABLE = Detector.webgl;//supportsWebGl();


const ADJUSTED_ATLAS_9000="Modified from formulas in Wang L, Booth MA, Koch DD (2004) Comparison of intraocular"+
        "lens power calculation methods in eyes that have undergone laser in-situ keratomileusis."+
        "Ophthalmology 111(10):1825-31. Presented at ASCRS 2007.</br>"+
        "With this method, the Atlas 9000 4-mm zone value is obtained from the Zeiss Humphrey"+
        "Atlas 9000 topographer. This value is then modified according to the amount of refractive"+
        "correction induced by the surgery:</br>"+
        "Atlas 9000 4-mm zone - (RC x 0.2) = Post-LASIK/PRK adjusted corneal power</br>"+
        "where RC = the amount of refractive correction induced by the LASIK/PRK at the corneal"+
        "plane.</br>"+
        "The Shammas-PL"+
        "formula outlined in the following article is used for IOL power calculation:<br>"+
        "<br>"+
        "Shammas HJ and <span style=\"font-family: Times New Roman\">Shammas</span> MC (2007)."+
        "    No-history method of intraocular lens power calculation for cataract surgery after"+
        "myopic laser in situ keratomileusis. J Cataract Refractive Surg. 33:31-36.<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt\">"+
        "    <!--?xml namespace=\"\" ns=\"urn:schemas-microsoft-com:office:office\" prefix=\"o\" ?--><!--?xml namespace=\"\" prefix=\"O\" ?--><o:p></o:p>&nbsp; &nbsp;"+
        "</p>"+
        "<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt\">"+
        "&nbsp;</p>",


    ADJUSTED_ATLAS_RINGS="<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt\">"+
        "Modified from formulas in Wang L, Booth MA, Koch DD (2004) Comparison of intraocular"+
        "lens power calculation methods in eyes that have undergone laser in-situ keratomileusis."+
        "  Ophthalmology 111(10):1825-31. Presented at ASCRS 2007.</p>"+
        "<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt\">"+
        "<span style=\"mso-spacerun: yes\"></span>&nbsp;</p>"+
        "<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt\">"+
        "With this method, the average corneal power is obtained by averaging the"+
        "0 mm, 1 mm, 2 mm, and 3 mm ring values on the Atlas 9000 or the Numerical View from the  "+
        "Atlas 992 - 995 series. This value is then modified according to the amount of refractive"+
        "correction induced by the surgery:"+
        "</p>"+
        "<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt\">"+
        "   <!--?xml namespace=\"\" ns=\"urn:schemas-microsoft-com:office:office\" prefix=\"o\" ?-->"+
        "<o:p>&nbsp;</o:p>"+
        "</p>"+
        "<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt\">"+
        "Atlas Ring Values - (RC x 0.2) = Post-LASIK/PRK adjusted corneal power"+
        "</p>"+
        "<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt\">"+
        "<o:p>&nbsp;</o:p>"+
        "</p>"+
        "<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt\">"+
        "where RC = the amount of refractive correction induced by the LASIK/PRK at the corneal"+
        "plane."+
        "</p>"+
        "<p class=\"MsoNormal\" style=\"margin: 0in 0in 0pt\">"+
        "<o:p>&nbsp;</o:p>"+
        "</p>"+
        "<span style=\"font-size: 12pt; font-family: 'Times New Roman'; mso-fareast-font-family: 'MS Mincho';"+
        "mso-ansi-language: EN-US; mso-fareast-language: JA; mso-bidi-language: AR-SA>The"+
        "double-K"+
        "<!--?xml namespace=\"\" ns=\"urn:schemas-microsoft-com:office:smarttags\" prefix=\"st1\" ?-->"+
        "<st1:place w:st=\"on\">Holladay</st1:place>"+
        "1 IOL calculation formula is used.</span>",

    MASKET_FORMULA="" +
        "<p>Masket S (2006). A simple regression formula for IOL power adjustment in eyes requiring cataract surgery following excimer laser photoablation. J Cataract Refract Surg, 32(3):430-4.</p><br>"+
        "<p>Instead of calculating IOL power with pre-LASIK/PRK data, this method modifies the predicted IOL power obtained using the patient’s post-LASIK/PRK K readings (IOLMaster Ks) by using the following formula:</p><br>"+
        "<p> IOLpost + (RC x 0.326) + 0.101 = IOLadj</p><br>"+
        "<p>where IOLpost = the calculated IOL power following ablative corneal refractive surgery, RC = the refractive change after corneal refractive surgery at the corneal plane, and IOLadj = the adjusted power of the IOL to be implanted.</p><br>",
    MODIFIED_MASKET="" +
        "<p>Hill W. Presented at ASCRS 2006.</p><br>"+
        "<p>Dr. Hill has modified the Masket Formula to increase the accuracy of IOL power prediction as follows:</p><br>"+
        "<p>IOLpost + (RC x 0.4385) + 0.0295 = IOLadj</p><br>"+
        "<p>where IOLpost = the calculated IOL power following ablative corneal refractive surgery, RC = the refractive change after corneal refractive surgery at the corneal plane, and IOLadj = the adjusted power of the IOL to be implanted.</p><br>",
    ADJUSTED_ACCP = "" +
        "<p>Awwad ST, Manasseh C, Bowman RW, Cavanagh HD, Verity S, Mootha V, McCulley JP (2008). Intraocular lens power calculation after myopic laser in situ keratomileusis: Estimating the corneal refractive power. J Cataract Refract Surg. 34:1070-6.</p><br>"+
        "<p>The average central corneal power (ACCP) is the average of the mean powers of the central Placido rings over the central 3.0 mm of the cornea, as displayed by the Tomey Topography Modeling System.  With the adjusted ACCP method, the ACCP is modified according to the amount of refractive correction induced by the surgery:</p><br>"+
        "<p>ACCP - (RC x 0.16) = Post-LASIK/PRK adjusted corneal power</p><br>"+
        "<p>where RC = the amount of refractive correction induced by the LASIK/PRK at the corneal plane.</p><br>"+
        "<p> Magellan ACP and OPD-Scan III APP 3-mm manual value are modified using the same formula as the ACCP based on the amount of refractive correction induced by the surgery (personal communication Stephen D. Klyce, PhD).</p><br>",

    BARRETT_K = "" +
        "<p>Barrett GD. True-K formula: new approach to biometry after LASIK. Presented at ASCRS 2009.</p><br>"+
        "<p>Barrett GD.  Approaches to biometry in patients who have had corneal refractive surgery. Presented at WOC 2008.</p><br>"+
        "<p>Modified from the formula in:</p><br>"+
        "<p>Barrett GD. An improved universal theoretical formula for intraocular lens power prediction. J Cataract Refract Surg, 19 (1993), 713–720</p><br>"+
        "<p>Barrett GD. Intraocular lens calculation formulas for new intraocular lens implants. J Cataract Refract Surg, 13 (1987), 389–396</p><br>"+
        "<p>Change in refraction induced by the corneal refractive surgery (∆MR) is used in the Barrett True K Formula.</p><br>",

    VERSION_APP="" +
        " <div class=\"row\" style=\"margin: 0;\">"+
        "<div class=\"col-sm-12\">"+
        "<h2>Ladas Super Formula (v1.0b)</h2>"+
        "<h3 style=\"text-align: left;\">New Features:</h3>"+
        "<ul class=\"list-group col-sm-12  centerX\">"+
        "<li class=\"list-group-item\">Improved browser compatibility</li>"+
        "<li class=\"list-group-item\">Updates to interface layout and design</li>"+
        "<li class=\"list-group-item\">Updates to printout layout and design</li>"+
        "<li class=\"list-group-item\">Inclusion of an 'Import Patient' button to allow for '.xml' file imports of Lenstar or IOL Master patient files</li>"+
        "<li class=\"list-group-item\">Inclusion of 'Device Refractive Index'</li>"+
        "<li class=\"list-group-item\">Ability to calculate both eyes at once</li>"+
        "<li class=\"list-group-item\">Ability to set default values for 'A-Constant' and 'Device Refractive Index'</li>"+
        "</ul>"+
        "</div>"+
        "</div>";





