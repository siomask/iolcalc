app.config(
    [
        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider',
        function ($stateProvider, $urlRouterProvider, $locationProvider) {
            $urlRouterProvider.otherwise('/home');
            var url = 'partials/',
                calcs = url+"calcs/";

            $stateProvider.state('home_en', {
                url: '/', templateUrl: url + 'updates.html', controller: function ($scope) { $scope.$parent.containerVis = true;  }
            })
                .state('recent_updates', {url: '/press', templateUrl: calcs + 'view.html', controller: function ($scope) { $scope.$parent.containerVis = true;  }})
                .state('learn_more', {url: '/more', templateUrl: calcs + 'more.html', controller: function ($scope) { $scope.$parent.containerVis = true;  }})
                .state('home', {url: '/home', templateUrl: calcs + 'home.html', controller: 'HomeCntrl'})
                //.state('agreement', {url: '/agreement', templateUrl: url + 'agreement.html', controller: 'AgreeCntrl'})
                .state('settings', {url: '/settings', templateUrl: url + 'settings.html', controller: 'SettingsCntrl'})
                //.state('req_ac', {url: '/req_ac', templateUrl: url + 'req_ac.html', controller: 'AuthCtrl'})
                .state('sign_in', {url: '/sign_in', templateUrl: url + 'sign_in.html', controller: 'AuthCtrl'})
                .state('post-lasik-calc', {url: '/post-lasik-calc', templateUrl: calcs + 'post_lasik_calc.html', controller: 'PostCalcCntrl'})
                .state('file', {url: '/file:alias'})
                .state('toric_calc', {url: '/toric_calc', templateUrl: calcs + 'toric_calc.html', controller: 'ToricCalcCtrl'})
                .state('sign_up', {url: '/sign_up', templateUrl: url + 'sign_up.html', controller: 'AuthCtrl'});

            $locationProvider.html5Mode({enabled: true, requireBase: false});
        }]
);