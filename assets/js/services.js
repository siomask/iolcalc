'use strict';
var product = ['http://webgl.unilimes.com/project/mcalc'],
    local = ['mcal'],
    usees = local;

app.factory("handler", function ($q) {
    return {
        success: function (response) {
            if(response && response.data && response.data['session_time_out']){
                location.reload();
            }else{
                return response.data;
            }
        },
        error: function (response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return $q.reject("An unknown error occurred.");
            }

            return $q.reject(response.data.message);
        }
    }
});

app.factory("request", function ($http, handler) {
    var url='api/route.php',
        ajaxG = new XMLHttpRequest(),
        host_mdb = "http://13.58.33.131/",
        uri = host_mdb+"iolcalc_mdb/?method=";

    function loadData(data,method) {
        return $http.post( url,{data:data,method:method}).then(
            handler.success,
            handler.error
        );
    }
    function addData(data,method) {
        return $http.post( url,{data:data,method:method}).then(
            handler.success,
            handler.error
        );
    }
    function session(method) {
        return $http.post( url,{method:method}).then(
            handler.success,
            handler.error
        );
    }
    function mdbpost(data,method){
        return $http.post( uri+method,{table:data}).then(
            handler.success,
            handler.error
        );
    }
    function form(data,method,onFinish, onError){
        var ajax = ajaxG  ;
        ajax.onreadystatechange = function(oEvent){
            if (ajax.readyState === 4) {
                if (ajax.status === 200) {
                    onFinish(ajax.responseText?JSON.parse(ajax.responseText):{error:'Something went wrong'});
                } else {
                    if(onError)onError();
                }

            }

        };
        ajax.addEventListener("progress", updateProgress, false);
        ajax.addEventListener("load", transferComplete, false);
        ajax.addEventListener("error", transferFailed, false);
        ajax.addEventListener("abort", transferCanceled, false);
        ajax.addEventListener("loadend", loadEnd, false);
        function updateProgress (oEvent) {
            if (oEvent.lengthComputable) {
                var percentComplete = oEvent.loaded / oEvent.total;
                console.log(percentComplete);
            } else {
                console.error('can not progress loading');
            }
        }
        function loadEnd(e) {
            console.warn("finish loading, but i'm not sure if it was succeses.",e);
        }
        function transferComplete(evt) {
            console.warn("finish loading",evt);
        }

        function transferFailed(evt) {
            console.error("on load file catch error",evt);
        }

        function transferCanceled(evt) {
            console.error("User cancel loading.");
        }
        ajax.open("POST",url+"?method="+method);
        //ajax.open("POST","http://146.185.138.204:8010/public/iolcalc_mdb/?method="+method);
        ajax.send(data);
    }
    function mdb(data,method,onFinish, onError){
        var ajax = ajaxG  ;
        ajax.onreadystatechange = function(oEvent){
            if (ajax.readyState === 4) {
                if (ajax.status === 200) {
                    //console.log(ajax.responseText)
                    onFinish(ajax.responseText?JSON.parse(ajax.responseText):{error:'Something went wrong'});
                } else {
                    if(onError)onError();
                }

            }

        };
        ajax.addEventListener("progress", updateProgress, false);
        ajax.addEventListener("load", transferComplete, false);
        ajax.addEventListener("error", transferFailed, false);
        ajax.addEventListener("abort", transferCanceled, false);
        ajax.addEventListener("loadend", loadEnd, false);
        function updateProgress (oEvent) {
            if (oEvent.lengthComputable) {
                var percentComplete = oEvent.loaded / oEvent.total;
                console.log(percentComplete);
            } else {
                console.error('can not progress loading');
            }
        }
        function loadEnd(e) {
            console.warn("finish loading, but i'm not sure if it was succeses.",e);
        }
        function transferComplete(evt) {
            console.warn("finish loading",evt);
        }

        function transferFailed(evt) {
            console.error("on load file catch error",evt);
        }

        function transferCanceled(evt) {
            console.error("User cancel loading.");
        }
        ajax.open("POST",uri+method);
        ajax.send(data);
    }

    return {
        form: form,
        get: loadData,
        session: session,
        mdb: mdb,
        mdbpost: mdbpost,
        add: addData
    };
});
app.factory('FeedService', ['$http', function($http){
    var state = {};

    var loadFeed = function(){
        $http.get(usees[0]).then(function(result){
            state.feed = result.items;
        });
    };

    // load the feed on initialisation
    loadFeed();

    return {
        getState: function(){
            return state;
        }
    };
}]);

