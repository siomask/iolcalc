

AConstant=119.2;

SF=(AConstant * 0.5663) - 65.6;

ACD=(SF + 3.595) / 0.9704;

Ref=0;

nc=4 / 3;

R=function(xx,yy) { return (337.5 / xx); };

//HofRef=Ref / (1 - (0.012 * Ref));
HofRef =  function(Ref) {return Ref/(1-0.012*Ref);}


HofAL=function(xx,yy) {
    return (((18.5 * (yy < 18.5)) +
    (31 * (yy > 31))) + (yy * ((yy >= 18.5) && (yy <= 31))));
};

M=function(xx,yy) { return ((1 * (yy <= 23)) + (-1 * (yy > 23))); };

G=function(xx,yy) { return ((28 * (yy <= 23)) + (23.5 * (yy > 23))); };

pACD=(0.58357 * AConstant) - 63.896;

//HofACDPlaceholder=function(xx,yy) { return ((((pACD + (0.3 * (HofAL(xx,yy) - 23.5))) + (Math.pow(Math.tan(Math.radians(xx)),2))) + (((0.1 * M(xx,yy)) * (Math.pow((23.5 - HofAL(xx,yy)),2))) * Math.tan(Math.radians((0.1 * (Math.pow((G(xx,yy) - HofAL(xx,yy)),2))))))) - 0.99166); };
HofACDPlaceholder=function(xx,yy) { return ((((pACD + (0.3 * (HofAL(xx,yy) - 23.5))) + (Math.pow(Math.tan(Math.radians(xx)),2))) + (((0.1 * M(xx,yy)) * (Math.pow((23.5 - HofAL(xx,yy)),2))) * Math.radians((0.1 * (Math.pow((G(xx,yy) - HofAL(xx,yy)),2))))))) - 0.99166; };

HofACD=function(xx,yy) { return (((2.5 * (HofACDPlaceholder(xx,yy) < 2.5)) + (6.5 * (HofACDPlaceholder(xx,yy) > 6.5))) + (HofACDPlaceholder(xx,yy) * ((HofACDPlaceholder(xx,yy) >= 2.5) && (HofACDPlaceholder(xx,yy) <= 6.5)))); };

RT=0.2;

Rag=function(xx,yy) { return ((7 * (R(xx,yy) < 7)) + (R(xx,yy) * (R(xx,yy) >= 7))); };

//AG=function(xx,yy) { return ((13.5 * (((12.5 * yy) / 23.45) > 13.5)) + (((12.5 * yy) / 23.45) * (((12.5 * yy) / 23.45) <= 13.5))); };
AG = function(x,y){return 13.5*((12.5*y/23.45)>13.5)+(12.5*y/23.45)*((12.5*y/23.45)<=13.5);}

HolACD=function(xx,yy) { return ((0.56 + Rag(xx,yy)) - Math.sqrt(((Rag(xx,yy) * Rag(xx,yy)) - ((AG(xx,yy) * AG(xx,yy)) / 4)))); };

KochAL=function(xx,yy) { return ((0.8814 * yy) + 2.8701); };

MWVKpr=3.37;

MWALpr=23.39;

VKpr=MWVKpr;

ALpr=function(xx,yy) { return yy; };

ACDKonstant=(AConstant * 0.62467) - 68.747;

a1=0.4;

a2=0.1;

a0=((ACDKonstant / 1000) - (a1 * (MWVKpr / 1000))) - (a2 * (MWALpr / 1000));

d=function(xx,yy) { return ((a0 + ((a1 * VKpr) / 1000)) + ((a2 * ALpr(xx,yy)) / 1000)); };

nCC=1.3315;

n=1.336;

dBC=12;
V = 12;
na = 1.336;
NC = 1.333;
ncml = 0.333;

RC=function(xx,yy) { return (337.5 / xx); };

DC=function(xx,yy) { return ((nCC - 1) / (RC(xx,yy) / 1000)); };

Zee=function(xx,yy,Ref) { return (DC(xx,yy) + (Ref / (1 - ((Ref * dBC) / 1000)))); };

ModACD = 0.62467*AConstant-68.747;
r = function(x,y){return R(x,y);};
LCOR = function(x,y){return y*(y<=24.2) + (-3.446+1.715*y-0.0237*Math.pow(y,2)*(y>24.2));};
Cw = function(x,y){return -5.41+0.58412*LCOR(x,y)+0.098*x;};
H = function(x,y) {return (((r(x,y)-Math.sqrt(Math.abs(r(x,y)*r(x,y)-((Cw(x,y)*Cw(x,y))/4)))))*((r(x,y)*r(x,y)-((Cw(x,y)*Cw(x,y))/4))>=0) + ((r(x,y)-Math.sqrt(0)))*((r(x,y)*r(x,y)-((Cw(x,y)*Cw(x,y))/4))<0));};
NegTest = function(x,y){return (r(x,y)*r(x,y)-((Cw(x,y)*Cw(x,y))/4));};
Offset = ModACD-3.336;
ACDest = function(x,y){return H(x,y)+Offset;};

RETHICK = function(x,y){return (0.65696 - 0.02029*y);}
LOPT = function(x,y){return (y + RETHICK(x,y));}
REFTGT = Ref;




SRKI=function(xx,yy) { return ((AConstant - (0.9 * xx)) - (2.5 * yy)); };
HOFFERQ = function(x,y,Ref){return (1336/(y-HofACD(x,y)-0.05))-(1.336/((1.336/(x+HofRef(Ref)))-((HofACD(x,y)+0.05)/1000)));};
HOLLADAY = function(x,y,Ref){

    //var res = {};
    //res.a = na*R(x,y);
    //res.ba = (nc-1);
    //res.bb =  (y+RT);
    //res.b = (res.ba)*(res.bb);
    //res.c = na*R(x,y)-(nc-1)*(y+RT);
    //res.d = V*res.c ;
    //res.e = (y+RT)*R(x,y);
    //res.de = res.d +  res.e;
    //res.da = 0.001*Ref*res.de  ;
    //res.db = res.c - res.da ;
    //res.dc = 1000.*na* res.db ;
    //
    //console.log(res);


    return((1000.*na*(na*R(x,y)-(nc-1)*(y+RT)-0.001*Ref*(V*(na*R(x,y)-(nc-1)*(y+RT))+(y+RT)*R(x,y))))/(((y+RT)-HolACD(x,y)-SF)*(na*R(x,y)-(nc-1)*(HolACD(x,y)+SF)- 0.001*Ref*(V*(na*R(x,y)- (nc-1)*(HolACD(x,y)+SF))+(HolACD(x,y)+SF)*R(x,y)))));

};


KOCH = function(x,y,Ref){

    return ((1000.*na*(na*R(x,y)- (nc-1)*(KochAL(x,y)+RT) -0.001*Ref*(V*(na*R(x,y)-(nc-1)*(KochAL(x,y)+RT)) +  (KochAL(x,y)+RT)*R(x,y))))/(((KochAL(x,y)+RT)- HolACD(x,y)-SF)*(na*R(x,y)-(nc-1)*(HolACD(x,y)+SF)- 0.001*Ref*(V*(na*R(x,y)-(nc-1)*(HolACD(x,y)+SF))+(HolACD(x,y)+SF)*R(x,y)))));
};

HAIGIS = function(x,y,Ref) { return(n/(y/1000-d(x,y)))-(n/(n/Zee(x,y,Ref)-d(x,y)));};
SRKTHOLD = function(x,y,REFTGT){return (1000*na*(na*r(x,y)-ncml*LOPT(x,y)-.001*REFTGT*(V*(na*r(x,y)-ncml*LOPT(x,y))+LOPT(x,y)*r(x,y))))/((LOPT(x,y)-ACDest(x,y))*(na*r(x,y)-ncml*ACDest(x,y)-0.001*REFTGT*(V*(na*r(x,y)-ncml*ACDest(x,y))+ACDest(x,y)*r(x,y))));};
//SRKT = function(x,y,Ref) {return SRKTHOLD(x,y,Ref) + rem(0,NegTest(x,y,Ref)>=0);};
SUPERFORMULA = function(x,y,Ref) {return (HOFFERQ(x,y,Ref)*(y<=21.49) + KOCH(x,y,Ref)*((y>25)&&(KOCH(x,y,Ref)>=0)) + HOLLADAY(x,y,Ref)*((y>21.49)&&(y<=25)) + HAIGIS(x,y,Ref)*(KOCH(x,y,Ref) < 0));}


Math.radians = function (degrees) {
    return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function (radians) {
    return radians * 180 / Math.PI;
};

 //console.log("************-------------test---------***********");
 //console.log("AConstant = " + AConstant);
 //console.log("SF = " + SF);
 //console.log("ACD = " + ACD);
 //console.log("Ref = " + Ref);
 //console.log("nc = " + nc);
 //console.log("pACD = " + pACD);
 //console.log("RT = " + RT);
 //console.log("MWVKpr = " + MWVKpr);
 //console.log("MWALpr = " + MWALpr);
 //console.log("ACDKonstant = " + ACDKonstant);
 //console.log("a1 = " + a1);
 //console.log("a2 = " + a2);
 //console.log("a0 = " + a0);
 //console.log("nCC = " + nCC);
 //console.log("n = " + n);
 //console.log("dBC = " + dBC);
 //console.log("V = " + V);
 //console.log("na = " + na);
 //console.log("NC = " + NC);
 //console.log("ncml = " + ncml);
 //console.log("ModACD = " + ModACD);
 //console.log("Offset = " + Offset);
 //console.log("REFTGT = " + REFTGT);
 //console.log("CW = " + Cw(41, 20, 1));
 //console.log("H  = " + H(41, 20, 1));
 //console.log("NegTest  = " + NegTest (41, 20, 1));
 //console.log("ACDest  = " + ACDest (41, 20, 1));
 //console.log("RETHICK   = " + RETHICK  (41, 20, 1));
 //console.log("LOPT    = " + LOPT   (41, 20, 1));
 //console.log("HOFFERQ     = " + HOFFERQ    (41, 20, 1));
 //console.log("HolACD      = " + HolACD     (41, 20, 1));
 //
 //
 //console.log("RT      = " + RT);
 //console.log("na      = " + na);
 //console.log("nc      = " + nc);
 //console.log("V      = " + V);
 //console.log("SF      = " + SF);
 //console.log("KochAL      = " + KochAL(41, 20, 1));
 //console.log("R      = " + R(41, 20, 1));
 //console.log("************-------------error---------***********");
 //console.log("HOLLADAY      = " + HOLLADAY     (41, 20, 1));
 //console.log("KOCH       = " + KOCH      (41, 20, 1));
 //console.log("************-------------error---------***********");
 //console.log("HAIGIS        = " + HAIGIS       (41, 20, 1));
 //console.log("SRKTHOLD         = " + SRKTHOLD        (41, 20, 1));
 //console.log("SUPERFORMULA          = " + SUPERFORMULA        ((44.25+43.98)/2,27.45,0));





function PLOTG(d) {
    //reset variables
    AConstant = d.AConstant ? d.AConstant : 119.2;
    SF = (AConstant * 0.5663) - 65.6;
    //ACD = d.ACD;
    ACD = d.needInputACD ? d.ACD :(d.AD && d.CCT? d.AD + (d.CCT/1000):(SF + 3.595) / 0.9704);
    ACDKonstant = (AConstant * 0.62467) - 68.747;
    a0 = ((ACDKonstant / 1000) - (a1 * (MWVKpr / 1000))) - (a2 * (MWALpr / 1000));
    ModACD = 0.62467 * AConstant - 68.747;
    Offset = ModACD - 3.336;

    pACD = (0.58357 * AConstant) - 63.896;
    K3 = (((d.K1) + (d.K2)) / 2);
    AL = d.axialLength;
    Ref = d.Rfrctn;
    REFTGT = Ref;
    //HofRef = Ref / (1 - (0.012 * Ref));
    VKpr = d.ACD;
    var AdjustedInputK3 = (K3*(1.3375-1))/((d.KIndex == 0?1.3375:d.KIndex)-1);
    var
        refInd = SUPERFORMULA(AdjustedInputK3, AL, Ref),
        yPst = (refInd >= 10)?SUPERFORMULA(K3, AL, Ref):refInd;
    var points = [], faces = [],
        xBgn = 20, yBgn = 37,
        abscissSize = 10,
        delta = {x: 0.0, y: (abscissSize - 1.2 ) / K3, z: abscissSize / K3},
        step = 0.5,//,0.25
        indx = 0, indStep = (1 / step) * abscissSize,
        maxByIt = 10 - step, _yMax = 0, _yMin = 0, curMaxY = 0, strt = 0;

    for (var i = strt; i <= abscissSize; i += step) {
        var curX = xBgn + i,
            curY = yBgn;
        for (var k = strt; k <= abscissSize; k += step) {
            curMaxY = (SUPERFORMULA(curY + k, curX, Ref)) * delta.y;
            if (_yMax < curMaxY)_yMax = curMaxY;
            if (_yMin > curMaxY)_yMin = curMaxY;
            points.push(new THREE.Vector3(curX, curMaxY, curY + k));

            if (k <= maxByIt) {
                if (i <= maxByIt) {
                    faces.push(new THREE.Face3(indx + 1, indx, indx + 1 + (indStep)));
                    faces.push(new THREE.Face3(indx + 1, indx + 1 + indStep, indx + 2 + indStep));
                }
            }
            indx++;
        }
    }
    //points.push(points[points.length-1]);

    var iolRef = iolRefCreate(AdjustedInputK3, AL, Ref,d.needInputACD);
    if (!iolRef)return false;
    var param = {
        _zSt: Math.ceil(Math.round(K3) / 10),
        points: points,
        faces: faces,
        _yMax: _yMax,
        _yMin: _yMin,
        yPst: yPst,
        indStep: indStep,
        abscissSize: abscissSize,
        step: step,
        iolRef: iolRef,
        poinPst: {x: K3, y: yPst * delta.y, z: AL}
    };

    if (webglEl)webglEl.build.buildEyeState(param);
    return param;
}

function iolRefCreate(K3, AL, Ref3,needInputACD) {
    var result = [],
        z = [],
        delta = 0.5;

    //data for ref
    z[3] = parseFloat((SUPERFORMULA(K3, AL, Ref3)).toFixed(2));
    z[2] = parseFloat((((Math.floor(2 * z[3])) / 2)).toFixed(2));
    z[4] = parseFloat((( (Math.ceil(2 * z[3])) / 2)).toFixed(2));
    if (z[3] == z[2] && z[3] == z[4]) {
        z[2] = z[3] - delta;
        z[4] = z[3] + delta;
    } else {
        z[2] = Math.floor((2 * z[3])) / 2;
        z[4] = Math.ceil((2 * z[3])) / 2;
    }

    z[1] = z[2] - delta;
    z[5] = z[4] + delta;
    z[0] = z[1] - delta;
    z[6] = z[5] + delta;

    for (var i = 0; i < z.length; i++) {
        var results;
        if (i == 3) {
            results = (Ref3).toFixed(2);
        } else {
            try {
                results = fzero(function (r) {
                    return ((SUPERFORMULA(K3, AL, r) - z[i]));
                }, 1).toFixed(2);
            } catch (RangeError) {
                results = '0.00';
            }
            if (results == z[i]) results = NaN;
        }
        if (z[i] < 0 && !needInputACD) return false;
        result.push({iol: z[i].toFixed(2), ref: results, middle: (i == 3)});
    }
    return result
}

function fzeroT(func, result) {
    var delta = 0.001,
        res = 0,
        res_ = 0,
        i = result + 0,
        k = result + 0;

    function init(a) {
        return parseFloat((func(a)).toFixed(2))
    };

    do {
        res = init(i);
        res_ = init(k);
        i += delta;
        k -= delta;
    } while ((res) != 0 && res_ != 0);
    if (res == 0) {
        return i;
    } else {
        return k;
    }
}

var fzero = function (func, num) {
    if (typeof func == 'function' && typeof num == 'number') {
        var up = true;
        var Step = num / 2;
        var Numb = num;
        var Result = num;
        var round = 3,
            maxIter = 10000,
            curIter = 0;

        function fNeares(_num) {
            return Math.abs(parseFloat(( func(_num)).toFixed(round)));
        }

        function checkNearest(_nearest, _step) {
            var na = ( _nearest + _step );
            var nb = ( _nearest - _step );
            var res,
                rA = fNeares(na),
                rB = fNeares(nb);
            res = (rA < rB ? na : nb);
            return res;
        }

        if (fNeares(num) != 0) {
            (function _check(n1, sr) {
                var result = checkNearest(n1, sr);
                //console.log(sr);
                if (fNeares(result) != 0) {
                    var down = 1,
                        st = result / 2;
                    if (up && parseFloat(result.toFixed(2)) == 0) {
                        down = -1;
                        //up =false;
                        st = 0.1;
                    }
                    _check(result * down, st);

                } else {
                    Result = result;
                }
            })(Numb, Step);
        }
        return Result;
    }
};