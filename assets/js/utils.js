var API_DIR ="api/";
function showContentDialog(type){
    var link = '';
    var size = {
        width: 0,
        height: 0
    };

    switch (type){
        case 'logup':
            link = API_DIR+'members/logup.php';
            size.width = 380;
            size.height = 450;
            break;
    }

    if(link) {
        $('.white_content_uam').css({
            width: size.width,
            height: size.height,
            display:'block',
            background: type == 'login' ? 'transparent' : 'whitesmoke'
        });

        $('#area_uam').html('<iframe src="' + link + '" frameBorder="0"></iframe>');
        toggleAuthDialog();
    }
}

function toggleAuthDialog(){
    //var dialogFrame = $('#white_content_uam, #black_overlay_uam, #area_uam');
    //dialogFrame.is(':visible') ? dialogFrame.hide() : dialogFrame.show();
}