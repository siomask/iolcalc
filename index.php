<?php
//header("Access-Control-Allow-Origin: *");
$req = preg_split("/\//", $_SERVER['REQUEST_URI']);
$url = "http://{$_SERVER['HTTP_HOST']}/{$req[1]}/{$req[2]}/ ";
$escaped_url = htmlspecialchars($url, ENT_QUOTES, 'UTF-8');
?>
<!DOCTYPE html >
<html data-ng-app="Main">
<head lang="en">
    <meta charset="UTF-8">
    <!--    <meta http-equiv="refresh" content="10; url=/">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <meta http-equiv="pragma" content="no-cache">

    <title> IOLcalc </title>
    <base id="baseHref" href="<?php echo $escaped_url; ?>">


    <link href='assets/css/css.css' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="assets/css/style2.css" rel="stylesheet" type="text/css">
    <link media="print, handheld" rel="stylesheet" href="assets/css/print.css">
    <link rel="stylesheet" type="text/css" href="assets/js/libs/FullscreenForm/css/normalize.css"/>
    <link rel="stylesheet" type="text/css" href="assets/js/libs/FullscreenForm/css/demo.css"/>
    <link rel="stylesheet" type="text/css" href="assets/js/libs/FullscreenForm/css/component.css"/>
    <link rel="stylesheet" type="text/css" href="assets/js/libs/FullscreenForm/css/cs-select.css"/>
    <link rel="stylesheet" type="text/css" href="assets/js/libs/FullscreenForm/css/cs-skin-boxes.css"/>

    <!--material design -->
    <link rel="stylesheet" href="assets/css/material.indigo-pink.min.css">

    <link rel="stylesheet" href="assets/css/icon.css">
    <link rel="stylesheet" href="bower_components/angular-material/angular-material.css">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/angular-material-data-table/dist/md-data-table.min.css"/>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/angular/angular.min.js"></script>
    <script src="bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="bower_components/angular-aria/angular-aria.min.js"></script>
    <script src="bower_components/angular-messages/angular-messages.min.js"></script>
    <script src="bower_components/angular-material/angular-material.js"></script>
    <script src="bower_components/moment/min/moment.min.js"></script>
    <script src="bower_components/moment-timezone/builds/moment-timezone-with-data.min.js"></script>

    <!--    <script src="bower_components/ng-flow/dist/ng-flow-standalone.min.js"></script>-->
    <!--    <script src="bower_components/flow.js/dist/flow.min.js"></script>-->
    <!--    <script src="bower_components/ng-flow/dist/ng-flow.min.js"></script>-->

    <script src="assets/js/libs/html2canvas-0.5.0-alpha1/dist/html2canvas.min.js"></script>
    <script src="assets/js/libs/html2canvas-0.5.0-alpha1/dist/html2canvas.svg.min.js"></script>
    <script src="assets/js/libs/jquery.xml2json.js" type="text/javascript" language="javascript"></script>

    <!--three-->
    <script src="assets/js/libs/three.js"></script>
    <!--    <script src="http://threejs.org/examples/js/controls/OrbitControls.js"></script>-->
    <script src="assets/js/libs/Detector.js"></script>
    <script src="assets/js/libs/CombinedCamera.js"></script>
    <script src="assets/js/libs/FullscreenForm/js/modernizr.custom.js"></script>

    <script src="assets/js/libs/SubdivisionModifier.js"></script>

    <!--custum-->
    <script type="text/javascript" src="bower_components/tinymce-dist/tinymce.js"></script>
    <script type="text/javascript" src="bower_components/angular-ui-tinymce/src/tinymce.js"></script>
    <script type="text/javascript" src="bower_components/excellentexport/excellentexport.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/threeApp.js"></script>

    <script src="assets/js/app.js"></script>

    <script src="assets/js/services.js"></script>
    <script src="assets/js/router.js"></script>

    <script src="assets/js/controller/HomeCntrl.js"></script>
    <!--<script src="assets/js/controller/HomeCntrl1.js"></script>-->

    <script src="assets/js/controller/PostCalcCntrl.js"></script>

    <script src="assets/js/controller/SetCntrl.js"></script>

    <script src="assets/js/controller/ToricCalcCtrl.js"></script>

    <link rel="stylesheet" ; href="assets/css/ng-table.min.css">
    <script src="assets/js/libs/ng-table.min.js"></script>

    <link id="scsDefaultFont" rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Abel">
    <style>
        .navigation-item {
            padding: 15px 0;
        }

        .title-p1 {
            font-size: 42pt;
            font-family: serif;
            display: block;
            padding: 30px;
            line-height: 50px;
        }

        .bold-p1 * {
            font-weight: 900;
        }
        /*.iolcalc a{
            text-shadow: 4px 2px 9px rgba(150, 150, 150, 1);
            font-weight: 900;
            font-family: monospace;
            transition: 0.5s linear;

        }
        .iolcalc a:hover{
            color:blue!important;
        }*/
        #octBlock_516425 .scsMainText{
            color: #000533;
        }
        .my-list li{
            font-weight: 900;
            background-color: transparent;
            /*font-size: 18.6667px;*/
            margin: 10px;
            /*color: rgb(0, 0, 255);*/
            line-height: 20px;
            text-align: left;
        }
        .title-p1{
            font-family: inherit!important;
            color: #000533;
        }
        .overflow-fix{
            overflow-x: visible !important;
            overflow-y: visible !important;
        }
    </style>
</head>

<body ng-cloak="" data-ng-controller="AppCtrl" class="overflow-fix">

<div class="preloader-text">
    <div class="center">
        <div id="inTurnBlurringTextG" class="inTurStaingTextG">Loading...</div>
    </div>
</div>
<div class="preloader-text-animate">
    <div class="center">
        <div id="inTurnBlurringTextG">
            <div id="inTurnBlurringTextG_1" class="inTurnBlurringTextG">L</div>
            <div id="inTurnBlurringTextG_2" class="inTurnBlurringTextG">o</div>
            <div id="inTurnBlurringTextG_3" class="inTurnBlurringTextG">a</div>
            <div id="inTurnBlurringTextG_4" class="inTurnBlurringTextG">d</div>
            <div id="inTurnBlurringTextG_5" class="inTurnBlurringTextG">i</div>
            <div id="inTurnBlurringTextG_6" class="inTurnBlurringTextG">n</div>
            <div id="inTurnBlurringTextG_7" class="inTurnBlurringTextG">g</div>
            <div id="inTurnBlurringTextG_8" class="inTurnBlurringTextG">.</div>
            <div id="inTurnBlurringTextG_9" class="inTurnBlurringTextG">.</div>
            <div id="inTurnBlurringTextG_10" class="inTurnBlurringTextG">.</div>
            <div style="text-align: center;"><span id="preloaderStatus" style="color:#3498db;"></span>%</div>
        </div>
    </div>
    <div id="loadingImg" class="center">
        <canvas id="preloaderImg" width="300" height="300" class="center" style="    width: initial;"></canvas>
    </div>
</div>
<div data-ng-show="containerVis" class="container_main ">
    <div class="cont"><!--data-ng-click="utils.close()"-->
    </div>
    <ui-view autoscroll></ui-view>
</div>
<div id="contvdParent"><img class="contvd" id="contvd" src="#"></div>

</body>
</html>
