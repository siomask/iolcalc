<?php
?>
<style>
    #btn-superformula{
        font-size: 32px;
        padding: 5px;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
    }
    #btn-superformula:active {
        margin-top: 2px;
        margin-bottom: 13px;
    }
    .button {
        display: inline-block;
    //    height: 50px;
    //    line-height: 50px;
        padding: 20px 20px 15px 20px;
    //    padding-right: 30px;
    //    padding-left: 70px;
    //    position: relative;
        background-color: #6E8BBA;
        color: rgb(255,255,255)!important;
        text-decoration: none!important;
        font-family: Arial;
        font-weight: 600;
        font-size: 18px;
        text-transform: uppercase;
    //    letter-spacing: 1px;
    //    margin-bottom: 15px;
        border-radius: 8px;
        -moz-border-radius: 8px;
        -webkit-border-radius: 8px;
        text-shadow: 0px 1px 0px rgba(0,0,0,0.5);
        -ms-filter: \"progid:DXImageTransform.Microsoft.dropshadow(OffX=0,OffY=1,Color=#ff123852,Positive=true)\";
        zoom: 1;
        filter: progid:DXImageTransform.Microsoft.dropshadow(OffX=0,OffY=1,Color=#ff123852,Positive=true);
        -moz-box-shadow: 0px 5px 5px rgba(0,0,0,0.3);
        -webkit-box-shadow: 0px 5px 5px rgba(0,0,0,0.3);
        box-shadow: 0px 5px 5px rgba(0,0,0,0.3);
        -ms-filter: \"progid:DXImageTransform.Microsoft.dropshadow(OffX=0,OffY=2,Color=#33000000,Positive=true)\";
        filter: progid:DXImageTransform.Microsoft.dropshadow(OffX=0,OffY=2,Color=#33000000,Positive=true);
    }

    //.button:hover span, .button.active span {
      //    background-color:rgb(0,102,26);
      //  border-right: 1px solid  rgba(0,0,0,0.3);
      //  text-decoration: underline;
      //}

    .button:active {
    //    margin-top: 2px;
    //  margin-bottom: 13px;


        -moz-box-shadow:0px 0px 0px rgba(255,255,255,0.5);
        -webkit-box-shadow:0px 0px 0px rgba(255,255,255,0.5);
        box-shadow:0px 0px 0px rgba(255,255,255,0.5);
        -ms-filter:\"progid:DXImageTransform.Microsoft.dropshadow(OffX=0,OffY=1,Color=#ccffffff,Positive=true)\";
        filter:progid:DXImageTransform.Microsoft.dropshadow(OffX=0,OffY=1,Color=#ccffffff,Positive=true);
    }
    .button:hover {
        background-color: #84a3d6;
    }
    .button:focus{
        outline: none!important;
        outline-offset: 0!important;;
    }



</style>
<script>
    var _listOfATags = document.getElementsByTagName('a');
    for(var i=0;i<_listOfATags.length;i++){
        var _tag = _listOfATags[i];
        if(_tag.href == location.origin+'/play/agreement'){
            _tag.className = 'button';
            break;
        }
    }

</script>;