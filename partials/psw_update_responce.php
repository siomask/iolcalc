<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 27.04.2016
 * Time: 17:50
 */
?>
<!DOCTYPE html >
<html>
<head lang="en">
<link rel="stylesheet" href="../bower_components/angular-material/angular-material.css">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/angular/angular.min.js"></script>
    <script src="../bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="../bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="../bower_components/angular-aria/angular-aria.min.js"></script>
    <script src="../bower_components/angular-messages/angular-messages.min.js"></script>
    <script src="../bower_components/angular-material/angular-material.js"></script>

    </head>
<body>
<script type="text/javascript">



(function(angular, undefined){
    "use strict";

    angular
        .module('demoApp', ['ngMaterial'])
        .controller('EmployeeController', EmployeeEditor) ;


    function EmployeeEditor($scope, $mdDialog) {
        var alert;
        function showAlert() {
            alert = $mdDialog.alert()
                .title('<?php if(empty($pswResUpda["error"]) ){echo "Password Reset";}else{echo"Error";}?>')
                .content('<?php if(empty($pswResUpda["error"]) ){echo $pswResUpda["success"];}else{echo $pswResUpda["error"];}?>')
                .ok('Close');

            $mdDialog
                .show( alert )
                .finally(function() {
                    location.href = '<?php echo   $pswResUpda["url"]?>';
                });
        }
        showAlert();

    }


})(angular);

</script>
<div ng-app='demoApp'  ng-controller='EmployeeController'></div>
</body>
</html>